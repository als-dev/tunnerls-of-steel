package fr.bretzel.region;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class RegionManager
{

    private HashMap<String, Region> regions = new LinkedHashMap<>();

    private File SAVE_DIRECTORY;

    private JavaPlugin javaPlugin;

    public RegionManager(JavaPlugin javaPlugin)
    {
        this.javaPlugin = javaPlugin;

        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Loads region.");
        load();

        Bukkit.getPluginManager().registerEvents(new RegionListener(this), javaPlugin);

        Bukkit.getPluginManager().registerEvents(new Listener()
        {
            @EventHandler
            public void onPluginDisable(PluginDisableEvent event)
            {
                if (event.getPlugin().getName().equals(javaPlugin.getName()))
                {
                    Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Save region");
                    save();

                }
            }
        }, javaPlugin);
    }

    public <V extends Region> void removeRegion(V region)
    {
        Objects.requireNonNull(region);

        if (regionExist(region.getName()))
        {
            File file = new File(SAVE_DIRECTORY, region.getName() + ".yml");
            if (file.exists() && file.isFile() && file.delete())
                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Successfully delete file " + region.getName() + ".yml");

            regions.remove(region.getName());
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Successfully removed region " + region.getName());
        } else
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Error, region " + region.getName() + " don't exit !");

    }

    public void removeRegion(String region)
    {
        removeRegion(getRegion(region));
    }

    /**
     * Register a new region if the region name doest exist.
     *
     * @param region Your region.
     */
    public <V extends Region> V registerNewRegion(V region)
    {
        Objects.requireNonNull(region);

        if (!regionExist(region.getName()))
            regions.put(region.getName(), region);
        else
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Error, region " + region.getName() + " already exit !");

        return region;
    }

    /**
     * @param name the name of region
     * @return true if the region is already exist
     */
    public boolean regionExist(String name)
    {
        return regions.containsKey(name);
    }

    public Region getRegion(String name)
    {
        return regionExist(name) ? regions.get(name) : null;
    }

    /**
     * Exemple: List<SimpleRegion> list = (SimpleRegion) this.getRegion(SimpleRegion.class)
     *
     * @param type SQLType of class if you want to get
     * @return List of your custom region.
     */
    public <C extends Region> List<C> getRegions(Class<C> type)
    {
        List<C> list = new ArrayList<>();

        for (Region region : getRegions())
        {
            //instanceof
            if (region.isTypeOf(type))
                list.add((C) region);
        }

        return list;
    }

    /**
     * @return to all region
     */
    public Collection<Region> getRegions()
    {
        return regions.values();
    }

    public Collection<Region> getRegion(Location location)
    {
        ArrayList<Region> regions = new ArrayList<>();

        for (Region region : getRegions())
            if (region.contains(location))
                regions.add(region);

        return regions;
    }

    /**
     * Simple method for if x, y, z is in a region
     *
     * @param x X location
     * @param y Y location
     * @param z Z location
     * @return true if a region contains the coordinate
     */
    public boolean contains(int x, int y, int z)
    {
        for (Region region : getRegions())
        {
            if (region.contains(x, y, z))
                return true;
        }

        return false;
    }

    /**
     * @param location
     * @return if a region contains this coordinate
     */
    public boolean contains(Location location)
    {
        for (Region region : getRegions())
        {
            if (region.contains(location))
                return true;
        }

        return false;
    }

    /**
     * Basics utility to save location
     *
     * @param location
     * @return a simple save string separate with ':'
     */
    private String toStringLocation(Location location)
    {
        return location.getWorld() == null ? "world" : location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ();
    }

    /**
     * Basics utility to load location
     *
     * @param location simple string formatted by toStringLocation(Location location)
     * @return a bukkit location
     */
    private Location toLocationString(String location)
    {
        if (location != null && !location.isEmpty())
        {
            String[] strings = location.split(":");
            return Bukkit.getWorld(strings[0]) == null ? null : new Location(Bukkit.getWorld(strings[0]), Double.valueOf(strings[1]), Double.valueOf(strings[2]), Double.valueOf(strings[3]));
        } else
        {
            return null;
        }
    }

    /**
     * Save all region in a file.
     */
    private <V extends Region> void load()
    {
        JavaPlugin plugin = this.javaPlugin;

        SAVE_DIRECTORY = new File(plugin.getDataFolder() + File.separator + "region" + File.separator);

        boolean mkdir = true;

        regions.clear();

        if (!SAVE_DIRECTORY.exists())
            mkdir = SAVE_DIRECTORY.mkdir();

        if (mkdir && SAVE_DIRECTORY.isDirectory() && Objects.requireNonNull(SAVE_DIRECTORY.listFiles()).length > 0)
        {
            for (File file : Objects.requireNonNull(SAVE_DIRECTORY.listFiles()))
            {
                YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

                Class<? extends Region> clazz = null;

                V region = null;

                if (configuration.contains("class"))
                {
                    try
                    {
                        clazz = Class.forName(configuration.getString("class")).asSubclass(Region.class);
                    } catch (Exception e)
                    {
                        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Error, cannot load " + file.getName() + " class: " + configuration.getString("class") + " not found.");
                        continue;
                    }
                }

                if (clazz != null)
                {
                    try
                    {
                        Constructor constructor = getSimpleConstructor(clazz);

                        if (constructor == null)
                        {
                            throw new RuntimeException("Each subclass of Region has to implement a constructor with no parameters");
                        }

                        region = (V) constructor.newInstance();

                        Method loadMethod = getLoadMethod(region);

                        if (loadMethod != null)
                        {
                            loadMethod.invoke(region, configuration);
                        } else
                        {
                            throw new RuntimeException("Cannot get loading method for: " + region.getClass() + " : " + file.getName());
                        }
                    } catch (InvocationTargetException | IllegalAccessException e)
                    {
                        e.printStackTrace();
                    } catch (InstantiationException e)
                    {
                        System.out.println("RegionLoader: Each subclass of Region has to implement a constructor with no parameters");
                        e.printStackTrace();
                    }
                } else
                {
                    Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] Error, cannot load " + file.getName() + " class: " + configuration.getString("class") + " is null.");
                }

                if (region != null)
                {
                    regions.put(region.getName(), region);
                }
            }
        }

        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + ChatColor.WHITE + javaPlugin.getName() + ChatColor.RED + "] " + getRegions().size() + " region loaded.");
    }

    /**
     * Save all region in a file.
     */
    private <V extends Region> void save()
    {
        for (Region region : getRegions())
        {
            File file = new File(SAVE_DIRECTORY, region.getName() + ".yml");
            YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

            String className = region.getClass().getPackage().getName() + "." + region.getClass().getSimpleName();

            configuration.set("class", className);
            configuration.set("name", region.getName());
            configuration.set("maxLocation", toStringLocation(region.getMaxLocation()));
            configuration.set("minLocation", toStringLocation(region.getMinLocation()));

            Method saveMethod = getSaveMethod(region);

            if (saveMethod != null)
            {
                try
                {
                    saveMethod.invoke(region, configuration);
                } catch (IllegalAccessException | InvocationTargetException e)
                {
                    e.printStackTrace();
                }
            }

            try
            {
                configuration.save(file);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param region
     * @return get the save method
     */
    private <V extends Region> Method getSaveMethod(V region)
    {
        for (Method m : region.getClass().getMethods())
        {
            if (m.getName().equals("save")) {
                if (m.getParameterCount() > 1)
                    return null;
                if (m.getParameterTypes()[0].equals(YamlConfiguration.class))
                    return m;
            }
        }

        return null;
    }

    /**
     * @param region
     * @return get the load method
     */
    private <V extends Region> Method getLoadMethod(V region)
    {
        for (Method m : region.getClass().getMethods())
        {
            if (m.getName().equals("load")) {
                if (m.getParameterCount() > 1)
                    return null;
                if (m.getParameterTypes()[0].equals(YamlConfiguration.class))
                    return m;
            }
        }

        return null;
    }

    /**
     * Simple method to get a simple constructor
     *
     * @param clazz Region clazz
     * @return a constructor with no parameter
     */
    private Constructor getSimpleConstructor(Class clazz)
    {
        if (clazz != Object.class)
        {
            //Check a 'public' constructor
            for (Constructor constructor : clazz.getConstructors())
            {
                if (constructor.getParameterCount() == 0)
                    return constructor;
            }

            //Check a 'private' constructor and set accessible
            for (Constructor constructor : clazz.getDeclaredConstructors())
            {
                if (constructor.getParameterCount() == 0)
                {
                    constructor.setAccessible(true);
                    return constructor;
                }
            }
        }

        return null;
    }
}
