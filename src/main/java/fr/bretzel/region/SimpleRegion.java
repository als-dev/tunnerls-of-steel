package fr.bretzel.region;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public class SimpleRegion extends Region
{
    private String name;
    private Location maxLocation;
    private Location minLocation;

    public SimpleRegion()
    {
    }

    public SimpleRegion(String name, Location locationOne, Location locationTwo)
    {
        this.name = name;

        World world = locationOne.getWorld();

        int maxX = (Math.max(locationOne.getBlockX(), locationTwo.getBlockX()));
        int minX = (Math.min(locationOne.getBlockX(), locationTwo.getBlockX()));

        int maxY = (Math.max(locationOne.getBlockY(), locationTwo.getBlockY()));
        int minY = (Math.min(locationOne.getBlockY(), locationTwo.getBlockY()));

        int maxZ = (Math.max(locationOne.getBlockZ(), locationTwo.getBlockZ()));
        int minZ = (Math.min(locationOne.getBlockZ(), locationTwo.getBlockZ()));

        maxLocation = new Location(world, maxX, maxY, maxZ);
        minLocation = new Location(world, minX, minY, minZ);
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public World getOwnerWorld()
    {
        return getMaxLocation().getWorld();
    }

    @Override
    public Location getMaxLocation()
    {
        return maxLocation;
    }

    @Override
    public Location getMinLocation()
    {
        return minLocation;
    }

    @Override
    public boolean contains(Location location)
    {
        return contains(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    @Override
    public boolean contains(int x, int y, int z)
    {
        return contains(getOwnerWorld(), x, y, z);
    }

    @Override
    public boolean contains(World world, int x, int y, int z)
    {
        return world.getName().equals(getOwnerWorld().getName()) && x >= getMinLocation().getBlockX() && x <= getMaxLocation().getBlockX() && y >= getMinLocation().getBlockY() && y <= getMaxLocation().getBlockY() &&
                z >= getMinLocation().getBlockZ() && z <= getMaxLocation().getBlockZ();
    }

    @Override
    public void load(YamlConfiguration configuration) {
        this.maxLocation = toLocationString(configuration.getString("maxLocation"));
        this.minLocation = toLocationString(configuration.getString("minLocation"));
        this.name = configuration.getString("name");
    }

    @Override
    public YamlConfiguration save(YamlConfiguration configuration) {
        return configuration;
    }
}
