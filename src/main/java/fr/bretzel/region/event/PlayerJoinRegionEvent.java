package fr.bretzel.region.event;

import fr.bretzel.region.Region;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerJoinRegionEvent extends RegionEvent
{
    private static final long serialVersionUID = 4803701525466747L;

    private Player player;
    private Location from;
    private Location to;

    public PlayerJoinRegionEvent(Region region, Player player, Location from, Location to)
    {
        super(region);
        this.player = player;
        this.from = from;
        this.to = to;
    }

    public Location getFrom()
    {
        return from;
    }

    public Location getTo()
    {
        return to;
    }

    public Player getPlayer()
    {
        return player;
    }
}
