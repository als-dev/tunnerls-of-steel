package fr.bretzel.region.event;

import fr.bretzel.region.Region;
import org.bukkit.block.Block;

import java.util.List;

public class BlockExplodeInRegionEvent extends RegionEvent {
    private List<Block> block;

    public BlockExplodeInRegionEvent(Region region, List<Block> blocks) {
        super(region);
        this.block = blocks;
    }

    public List<Block> getBlock() {
        return block;
    }
}
