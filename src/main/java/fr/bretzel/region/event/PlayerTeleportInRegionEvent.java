package fr.bretzel.region.event;

import fr.bretzel.region.Region;
import org.bukkit.entity.Player;

public class PlayerTeleportInRegionEvent extends RegionEvent {
    private Player player;

    public PlayerTeleportInRegionEvent(Region region, Player player) {
        super(region);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
