package fr.bretzel.region.event;

import fr.bretzel.region.Region;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public abstract class RegionEvent<V extends Region> extends  Event implements Cancellable
{
    private static final long serialVersionUID = 4803701525466747L;
    private static final HandlerList handlers = new HandlerList();

    private boolean cancelled;
    private V region;

    public RegionEvent(V region)
    {
        this.region = region;
    }


    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        this.cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

    public V getRegion()
    {
        return region;
    }

    public void setRegion(V region)
    {
        this.region = region;
    }
}
