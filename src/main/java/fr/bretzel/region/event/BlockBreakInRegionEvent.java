package fr.bretzel.region.event;


import fr.bretzel.region.Region;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class BlockBreakInRegionEvent extends RegionEvent
{
    private Player player;
    private Block block;

    public BlockBreakInRegionEvent(Region region, Player player, Block block)
    {
        super(region);
        this.player = player;
        this.block = block;
    }

    public Block getBlock()
    {
        return block;
    }

    public Player getPlayer()
    {
        return player;
    }
}
