package fr.bretzel.region.event;

import fr.bretzel.region.Region;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerMoveOnRegion extends RegionEvent
{
    private static final long serialVersionUID = 8743701525466747L;

    private Player player;
    private Location to, from;

    public PlayerMoveOnRegion(Region region, Player player, Location from, Location to)
    {
        super(region);
        this.player = player;
        this.from = from;
        this.to = to;
    }

    public Player getPlayer()
    {
        return player;
    }

    public Location getTo()
    {
        return to;
    }

    public Location getFrom()
    {
        return from;
    }
}
