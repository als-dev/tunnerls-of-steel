package fr.bretzel.region;

import fr.bretzel.region.event.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class RegionListener implements Listener
{
    private RegionManager manager;

    public RegionListener(RegionManager manager)
    {
        this.manager = manager;
    }


    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Location getTo = event.getTo();
        Location getFrom = event.getFrom();

        Collection<? extends Region> fromList = manager.getRegion(getFrom);
        Collection<? extends Region> toList = manager.getRegion(getTo);

        fromList.forEach(fromRegion ->
        {
            if (!toList.contains(fromRegion)) {
                PlayerLeaveRegionEvent cEvent = new PlayerLeaveRegionEvent(fromRegion, event.getPlayer(), getFrom, getTo);
                Bukkit.getPluginManager().callEvent(cEvent);

                if (cEvent.isCancelled())
                    event.setCancelled(true);
            }

            if (toList.contains(fromRegion)) {
                PlayerMoveOnRegion cEvent = new PlayerMoveOnRegion(fromRegion, event.getPlayer(), getFrom, getTo);
                Bukkit.getPluginManager().callEvent(cEvent);

                if (cEvent.isCancelled())
                    event.setCancelled(true);
            }
        });

        toList.forEach(toRegion ->
        {
            if (!fromList.contains(toRegion)) {
                PlayerJoinRegionEvent cEvent = new PlayerJoinRegionEvent(toRegion, event.getPlayer(), getFrom, getTo);
                Bukkit.getPluginManager().callEvent(cEvent);

                if (cEvent.isCancelled())
                    event.setCancelled(true);
            }
        });
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBreakBlockEvent(BlockBreakEvent e)
    {
        manager.getRegion(e.getBlock().getLocation()).forEach(region ->
        {
            BlockBreakInRegionEvent event = new BlockBreakInRegionEvent(region, e.getPlayer(), e.getBlock());
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled())
                e.setCancelled(true);
        });
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlaceBlockEvent(BlockPlaceEvent e) {
        manager.getRegion(e.getBlock().getLocation()).forEach(region ->
        {
            BlockPlaceInRegionEvent event = new BlockPlaceInRegionEvent(region, e.getPlayer(), e.getBlock());
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled())
                e.setCancelled(true);
        });
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerTeleportEvent(PlayerTeleportEvent e) {
        manager.getRegion(e.getTo()).forEach(region ->
        {
            PlayerTeleportInRegionEvent event = new PlayerTeleportInRegionEvent(region, e.getPlayer());
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled())
                e.setCancelled(true);
        });
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockBreakByExplosion(BlockExplodeEvent e) {
        HashMap<Region, ArrayList<Block>> blockList = new HashMap<>();

        e.blockList().forEach(block ->
        {
            if (manager.contains(block.getLocation())) {
                manager.getRegion(block.getLocation()).forEach(region ->
                {
                    if (blockList.containsKey(region)) {
                        blockList.get(region).add(block);
                    } else {
                        ArrayList<Block> blcks = new ArrayList<>();
                        blcks.add(block);
                        blockList.put(region, blcks);
                    }
                });
            }
        });

        blockList.forEach((region, blocks) ->
        {
            BlockExplodeInRegionEvent event = new BlockExplodeInRegionEvent(region, blocks);
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled()) {
                e.blockList().removeAll(event.getBlock());
            }
        });
    }
}
