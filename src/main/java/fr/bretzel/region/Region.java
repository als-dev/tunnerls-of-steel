package fr.bretzel.region;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class Region
{
    public abstract String getName();

    public abstract World getOwnerWorld();

    public abstract Location getMaxLocation();

    public abstract Location getMinLocation();

    public abstract boolean contains(Location location);

    public abstract boolean contains(int x, int y, int z);

    public abstract boolean contains(World world, int x, int y, int z);

    public abstract YamlConfiguration save(YamlConfiguration yamlConfiguration);

    public abstract void load(YamlConfiguration yamlConfiguration);

    public boolean isTypeOf(Class type) {
        return type.isInstance(this);
    }

    public String toStringLocation(Location location) {
        return location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ();
    }

    public Location toLocationString(String location)
    {
        if (location != null && !location.isEmpty())
        {
            String[] strings = location.split(":");
            return Bukkit.getWorld(strings[0]) == null ? null : new Location(Bukkit.getWorld(strings[0]), Double.valueOf(strings[1]), Double.valueOf(strings[2]), Double.valueOf(strings[3]));
        } else
        {
            return null;
        }
    }

}
