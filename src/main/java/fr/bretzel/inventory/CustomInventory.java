package fr.bretzel.inventory;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

public class CustomInventory implements Listener {
    private Inventory inventory;

    private LinkedHashMap<Integer, ArrayList<ActionCallback>> actionOnSlot = new LinkedHashMap<>();

    private boolean onlyTopInventory;

    private boolean cancelClick = true;

    public CustomInventory(Plugin plugin, Inventory inventory, boolean onlyTopInventory) {
        this.inventory = inventory;
        this.onlyTopInventory = onlyTopInventory;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerClickInventory(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player && event.getInventory().equals(inventory)) {
            if (event.getRawSlot() < event.getInventory().getSize() && isOnlyTopInventory()) {
                if (hasAction(event.getSlot())) {
                    for (ActionCallback actionCallback : getActionCallback(event.getSlot())) {
                        actionCallback.setEvent(event);
                        actionCallback.onPlayerClickInventory((Player) event.getWhoClicked(), event.getInventory(), event.getClick(), event.getAction(), event.getSlotType(), event.getCurrentItem(), event.getCursor());
                    }
                } else {
                    if (isCancelClick())
                        event.setCancelled(true);
                }
            } else if (!isOnlyTopInventory()) {
                if (hasAction(event.getSlot())) {
                    for (ActionCallback actionCallback : getActionCallback(event.getSlot())) {
                        actionCallback.setEvent(event);
                        actionCallback.onPlayerClickInventory((Player) event.getWhoClicked(), event.getInventory(), event.getClick(), event.getAction(), event.getSlotType(), event.getCurrentItem(), event.getCursor());
                    }
                } else {
                    if (isCancelClick())
                        event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerCloseInventory(InventoryCloseEvent event) {
        if (event.getInventory().equals(getInventory())) {
            event.getHandlers().unregister(this);
            InventoryClickEvent.getHandlerList().unregister(this);
        }
    }

    public void addAction(int slot, ActionCallback actionCallback) {
        if (!actionOnSlot.containsKey(slot)) {
            ArrayList<ActionCallback> list = new ArrayList<>();
            list.add(actionCallback);
            actionOnSlot.put(slot, list);
            if (actionCallback.getItemStack() != null)
                getInventory().setItem(slot, actionCallback.getItemStack());
        } else {
            actionOnSlot.get(slot).add(actionCallback);
            if (actionCallback.getItemStack() != null)
                getInventory().setItem(slot, actionCallback.getItemStack());
        }
    }

    public void removeAction(ActionCallback actionCallback) {
        for (int slot : actionOnSlot.keySet()) {
            ArrayList<ActionCallback> copyCurrent = new ArrayList<>();
            Collections.copy(copyCurrent, actionOnSlot.get(slot));

            if (hasAction(slot, actionCallback)) {
                actionOnSlot.get(slot).remove(actionCallback);
            }
        }
    }

    public boolean hasAction(int slot, ActionCallback actionCallback) {

        for (ActionCallback callback : getActionCallback(slot)) {
            if (callback.equals(actionCallback))
                return true;
        }
        return false;
    }

    public boolean hasAction(ActionCallback actionCallback) {
        for (int slot : actionOnSlot.keySet()) {
            if (hasAction(slot, actionCallback))
                return true;
        }
        return false;
    }

    public boolean hasAction(int slot) {
        return actionOnSlot.containsKey(slot);
    }

    public List<ActionCallback> getActionCallback(int slot) {
        if (!actionOnSlot.containsKey(slot))
            return null;

        return actionOnSlot.get(slot);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public boolean isOnlyTopInventory() {
        return onlyTopInventory;
    }

    public boolean isCancelClick() {
        return cancelClick;
    }

    public void setCancelClick(boolean cancelClick) {
        this.cancelClick = cancelClick;
    }
}
