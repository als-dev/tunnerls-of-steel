package fr.bretzel.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class ActionCallback {
    private InventoryClickEvent event;

    public abstract void onPlayerClickInventory(Player player, Inventory inventory, ClickType clickType, InventoryAction inventoryAction, InventoryType.SlotType slotType, ItemStack current, ItemStack cursor);

    public abstract ItemStack getItemStack();

    public InventoryClickEvent getEvent() {
        return event;
    }

    public void setEvent(InventoryClickEvent event) {
        this.event = event;
    }
}
