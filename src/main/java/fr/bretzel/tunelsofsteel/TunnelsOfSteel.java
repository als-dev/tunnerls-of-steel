package fr.bretzel.tunelsofsteel;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import fr.bretzel.config.Config;
import fr.bretzel.config.table.ColumnType;
import fr.bretzel.config.table.Table;
import fr.bretzel.hologram.HologramManager;
import fr.bretzel.region.Region;
import fr.bretzel.region.RegionManager;
import fr.bretzel.tunelsofsteel.abilities.*;
import fr.bretzel.tunelsofsteel.abilities.dirtbomb.DirtBomb;
import fr.bretzel.tunelsofsteel.abilities.manager.AbilityManager;
import fr.bretzel.tunelsofsteel.command.FixfallingCommand;
import fr.bretzel.tunelsofsteel.command.TOSCommand;
import fr.bretzel.tunelsofsteel.game.Statue;
import fr.bretzel.tunelsofsteel.generator.VoidWorldGenerator;
import fr.bretzel.tunelsofsteel.listener.BlockEvent;
import fr.bretzel.tunelsofsteel.listener.PlayerEvent;
import fr.bretzel.tunelsofsteel.listener.RegionManagerListener;
import fr.bretzel.tunelsofsteel.listener.SelectionEvent;
import fr.bretzel.tunelsofsteel.listener.armor.ArmorListener;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.refineries.Refineries;
import fr.bretzel.tunelsofsteel.refineries.RefineriesManager;
import fr.bretzel.tunelsofsteel.refineries.Upgrade;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.region.GameRegion;
import fr.bretzel.tunelsofsteel.region.TeamRegion;
import fr.bretzel.tunelsofsteel.scoreboard.GameScoreboard;
import fr.bretzel.tunelsofsteel.scoreboard.LobbyScoreboard;
import fr.bretzel.tunelsofsteel.task.AreaAbsorbingTask;
import fr.bretzel.tunelsofsteel.task.ClearGame;
import fr.bretzel.tunelsofsteel.task.CustomSpawn;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import net.minecraft.server.v1_14_R1.Block;
import net.minecraft.server.v1_14_R1.IRegistry;
import net.minecraft.server.v1_14_R1.MinecraftKey;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public final class TunnelsOfSteel extends JavaPlugin
{
    public static final Table PLAYERS_TABLE = new Table("Players",
            ColumnType.INT("GameID"),
            ColumnType.VARCHAR("UUID", "40"),
            ColumnType.VARCHAR("Name", "32"),
            ColumnType.VARCHAR("Team", "24"),
            ColumnType.INT("PlayerKill"),
            ColumnType.INT("Death"),
            ColumnType.INT("oreMined"),
            ColumnType.INT("talentiumDeposed"),
            ColumnType.INT("potentialTalentiumMined"),
            ColumnType.DOUBLE("playerDamaged"),
            ColumnType.DOUBLE("playerHealRegen")
    );

    public static final String GAME_REGION_NAME = "TOS_GAME_REGION";
    public static GameScoreboard GAME_SCOREBOARD;
    public static TunnelsOfSteel INSTANCE;
    public static Statue STATUE = Statue.WAITING;
    public static int GAME_ID = -1;
    public static GameRegion GAME_REGION;
    private static String TOS_WORLD_NAME = "TunnelsOfSteel";
    private static Config CONFIG;
    private static HashMap<Material, Integer> ITEMS_TO_BURNS = new HashMap<>();
    private static FileConfiguration TOS_CONFIG;
    private static FileConfiguration REFINERIES_CONFIG;
    public static RegionManager REGION_MANAGER;
    public static HologramManager HOLOGRAM_MANAGER;
    private static FileConfiguration TEAMS_CONFIG;
    public static AbilityManager ABILITIES_MANAGER;
    private static File TOS_FILE;
    private static File REFINERIES_FILE;
    private static File TEAM_FILE;

    /**
     * @return the output volume
     */
    public static int getOutputForItem(ItemStack stack)
    {
        if (isWhiteListedBurnItems(stack))
            for (Material st : ITEMS_TO_BURNS.keySet())
                if (st == stack.getType())
                    return ITEMS_TO_BURNS.get(st);


        return 0;
    }

    /**
     * @return the output volume
     */
    public static int getOutputForItem(Material material) {
        return getOutputForItem(new ItemStack(material));
    }

    /**
     * @param material your item to verify
     * @return true if the stack if a burn item
     */
    public static boolean isWhiteListedBurnItems(Material material)
    {
        return ITEMS_TO_BURNS.containsKey(material);
    }

    /**
     * @param stack your item to verify
     * @return true if the stack if a burn item
     */
    public static boolean isWhiteListedBurnItems(ItemStack stack)
    {
        for (Material whiteListStack : ITEMS_TO_BURNS.keySet())
            if (whiteListStack == stack.getType())
                return true;

        return false;
    }

    /**
     * @return a random id if the game is not started
     */
    private static int getRandomGameId() {
        return new Random().nextInt(999);
    }

    public static World getTOSWorld() {
        World world;
        world = Bukkit.getWorld(TOS_WORLD_NAME);

        if (world != null)
            return world;

        return generateNewTOSWorld();
    }

    private static World generateNewTOSWorld() {
        World world = Bukkit.getWorld(TOS_WORLD_NAME);

        if (world == null) {
            Logger.log("Create new world: " + ChatColor.AQUA + TOS_WORLD_NAME);
            WorldCreator creator = new WorldCreator(TOS_WORLD_NAME);
            creator.generateStructures(false);
            creator.generator(new VoidWorldGenerator());
            world = creator.createWorld();
        }

        Objects.requireNonNull(world);

        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);

        world.setDifficulty(Difficulty.NORMAL);

        return world;
    }


    public static boolean pushTOSPlayerInDB(TOSPlayer... players)
    {
        String updateSQL = "UPDATE Players SET GameID = ?, Name = ?, Team = ?, PlayerKill = ?, Death = ?, oreMined = ?, talentiumDeposed = ?, potentialTalentiumMined = ?, playerDamaged = ?, playerHealRegen ? WHERE UUID = ?";
        String insertSQL = "INSERT INTO Players (GameID, UUID, Name, Team, PlayerKill, Death, oreMined, talentiumDeposed, potentialTalentiumMined, playerDamaged, playerHealRegen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            PreparedStatement updateStatement = CONFIG.openConnection().prepareStatement(updateSQL);
            PreparedStatement insertStatement = CONFIG.openConnection().prepareStatement(insertSQL);

            for (TOSPlayer player : players)
            {
                if (CONFIG.ifStringExistForColon(player.getPlayerID().toString(), "UUID", PLAYERS_TABLE))
                {
                    updateStatement.setInt(1, GAME_ID);
                    updateStatement.setString(2, player.getName());
                    updateStatement.setString(3, player.getTeam().getName());
                    updateStatement.setInt(4, player.getKill());
                    updateStatement.setInt(5, player.getDeath());
                    updateStatement.setInt(6, player.getOreMined());
                    updateStatement.setInt(7, player.getTalentiumDeposed());
                    updateStatement.setInt(8, player.getPotentialTalentiumMined());
                    updateStatement.setDouble(9, player.getPlayerDamaged());
                    updateStatement.setDouble(10, player.getPlayerHealRegen());
                    updateStatement.setString(11, player.getPlayerID().toString());

                    updateStatement.addBatch();

                    System.out.println("Adding new update batch for: " + player.getName());
                } else
                {
                    insertStatement.setInt(1, GAME_ID);
                    insertStatement.setString(2, player.getPlayerID().toString());
                    insertStatement.setString(3, player.getName());
                    insertStatement.setString(4, player.getTeam().getName());
                    insertStatement.setInt(5, player.getKill());
                    insertStatement.setInt(6, player.getDeath());
                    insertStatement.setInt(7, player.getOreMined());
                    insertStatement.setInt(8, player.getTalentiumDeposed());
                    insertStatement.setInt(9, player.getPotentialTalentiumMined());
                    insertStatement.setDouble(10, player.getPlayerDamaged());
                    insertStatement.setDouble(11, player.getPlayerHealRegen());

                    insertStatement.addBatch();

                    System.out.println("Adding new insert batch for: " + player.getName());
                }
            }

            insertStatement.executeBatch();
            updateStatement.executeBatch();

        } catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onLoad()
    {
        Logger.log("Start load.");

        INSTANCE = this;

        List<String> nerfedDurability = Arrays.asList("stone", "diorite", "polished_diorite", "lime_concrete",
                "orange_concrete", "purple_concrete", "light_blue_concrete", "dirt", "coarse_dirt", "oak_log", "andesite");

        IRegistry.BLOCK.forEach(block ->
        {
            MinecraftKey key = IRegistry.BLOCK.getKey(block);
            if (nerfedDurability.contains(key.getKey().toLowerCase()))
            {
                Logger.log("Nerfed durability for block " + key.toString());
                try
                {
                    Field durabilityField = getDurabilityField(block);
                    if (durabilityField != null)
                    {
                        durabilityField.setAccessible(true);
                        durabilityField.set(block, 0.5F);
                        durabilityField.setAccessible(false);
                    }
                } catch (NoSuchFieldException | IllegalAccessException e)
                {
                    e.printStackTrace();
                }
            }
        });

        Logger.log("End load.");
    }

    private Field getDurabilityField(Block block) throws NoSuchFieldException {
        Class clazz = block.getClass();

        while (clazz != Object.class) {
            if (clazz.equals(Block.class)) {
                return clazz.getDeclaredField("durability");
            } else if (clazz.getSuperclass() != null) {
                clazz = clazz.getSuperclass();
            }
        }

        return null;
    }

    @Override
    public void onEnable()
    {
        Logger.log("Start enable.");
        //Generate World
        World world = Bukkit.getWorld(TOS_WORLD_NAME);

        if (world == null)
        {
            Logger.log("Create new world: " + ChatColor.AQUA + TOS_WORLD_NAME);
            WorldCreator creator = new WorldCreator(TOS_WORLD_NAME);
            creator.generateStructures(false);
            creator.generator(new VoidWorldGenerator());
            world = creator.createWorld();
        }

        Logger.log("Actualise gamerule.");

        Objects.requireNonNull(world);

        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);

        world.setDifficulty(Difficulty.NORMAL);

        Logger.log("Enable Region");
        //Region Load
        REGION_MANAGER = new RegionManager(this);

        Logger.log("Start register Listener");

        //TODO PROUT
        new CustomSpawn();

        //Register Listener
        getServer().getPluginManager().registerEvents(new BlockEvent(), INSTANCE);
        getServer().getPluginManager().registerEvents(new PlayerEvent(), INSTANCE);
        getServer().getPluginManager().registerEvents(new ArmorListener(), INSTANCE);
        getServer().getPluginManager().registerEvents(new RegionManagerListener(), INSTANCE);
        getServer().getPluginManager().registerEvents(new SelectionEvent(this), this);

        Logger.log("End register Listener");

        Logger.log("Start register Ability");

        //Register Ability
        ABILITIES_MANAGER = new AbilityManager(this);

        ABILITIES_MANAGER.registerAbility(new FlashBang());
        ABILITIES_MANAGER.registerAbility(new Teleporter());
        ABILITIES_MANAGER.registerAbility(new GlowingBeacon());
        ABILITIES_MANAGER.registerAbility(new C4());
        ABILITIES_MANAGER.registerAbility(new Motivation());
        ABILITIES_MANAGER.registerAbility(new Reset());
        ABILITIES_MANAGER.registerAbility(new TunnelVision());
        ABILITIES_MANAGER.registerAbility(new Flemme());
        ABILITIES_MANAGER.registerAbility(new SeismicScan());
        ABILITIES_MANAGER.registerAbility(new Escrime());
        ABILITIES_MANAGER.registerAbility(new DagueDeVoleur());
        ABILITIES_MANAGER.registerAbility(new DirtBomb());
        ABILITIES_MANAGER.registerAbility(new SkeletonArmor());

        Logger.log("End register Ability");

        //Prout
        new AreaAbsorbingTask();

        if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
            ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.LOGIN) {
                @Override
                public void onPacketSending(PacketEvent event) {
                    if (event.getPacketType().equals(PacketType.Play.Server.LOGIN))
                        event.getPacket().getBooleans().write(0, true);
                }
            });
        }

        HOLOGRAM_MANAGER = new HologramManager(this);

        //Add scoreboard !
        GAME_SCOREBOARD = new GameScoreboard();

        //Init command
        PluginCommand command = getCommand("tos");
        if (command != null) {
            command.setExecutor(new TOSCommand());
            command.setTabCompleter(new TOSCommand());
        } else {
            Logger.log(ChatColor.DARK_RED + "Waring, the command \"tos\" cannot is not registered !");
        }

        getCommand("fixfalling").setExecutor(new FixfallingCommand());
        getCommand("fixfalling").setTabCompleter(new FixfallingCommand());

        Logger.log("Enable Config");
        //Load config
        loadConfig();

        Logger.log("Start loading Ability");
        //Load Abilities !
        ABILITIES_MANAGER.loadAbilities();
        Logger.log("End loading Ability");

        //Clear TOSPlayer
        if (!TOSPlayer.getPlayers().isEmpty())
            TOSPlayer.saveAllAndClear();

        //Init GameID for DB
        GAME_ID = getRandomGameId();

        //init game region
        GAME_REGION = (GameRegion) REGION_MANAGER.getRegion(GAME_REGION_NAME);

        //Init the Scoreboard if the game statue is waiting
        if (STATUE == Statue.WAITING)
            for (Player player : Bukkit.getOnlinePlayers())
                LobbyScoreboard.actualiseLobbyScoreboard(player);
    }

    @Override
    public void onDisable()
    {
        //Unload world on reload and stop
        Logger.log("Unload world: " + ChatColor.AQUA + TOS_WORLD_NAME);

        if (Bukkit.getWorld(TOS_WORLD_NAME) != null)
            Bukkit.getServer().unloadWorld(TOS_WORLD_NAME, true);

        ABILITIES_MANAGER.saveAbilities();

        Logger.log("Save configuration");
        //Save config
        saveConfig();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (player.isOp() && label.equalsIgnoreCase("test")) {
                /*if (pushTOSPlayerInDB(getTosPlayers(Bukkit.getOfflinePlayers())))
                    player.sendMessage("push player to db");
                else player.sendMessage("push player to db ????????????????????????????");*/

                //Bukkit.broadcastMessage("GameID = " + getRandomGameId());

                ItemStack stack = new ItemStack(Material.BARRIER);
                ItemMeta meta = stack.getItemMeta();
                meta.setCustomModelData(1006);
                stack.setItemMeta(meta);
                player.getInventory().addItem(stack);

                return true;
            } else if (label.equalsIgnoreCase("timerreset") && player.isOp())
            {
                GAME_SCOREBOARD.setMaxSecond((30 * 60) + ((60) * 60));

                player.sendMessage(ChatColor.GREEN + "OKAY !");
                return true;
            } else if (label.equalsIgnoreCase("tosstart") && player.isOp())
            {
                /*for (Player p : Bukkit.getOnlinePlayers())
                {
                    TOSTeam team = TeamManager.getTeamOfPlayer(p);

                    if (team != null)
                    {
                        ItemStack potatoStack = new ItemStack(Material.BAKED_POTATO, 64);
                        ItemMeta potatoMeta = potatoStack.getItemMeta();
                        potatoMeta.setDisplayName(ChatColor.GREEN + "Infinite Potato");
                        potatoStack.setItemMeta(potatoMeta);

                        ItemStack torchStack = new ItemStack(Material.TORCH, 64);
                        ItemMeta torchMeta = torchStack.getItemMeta();
                        torchMeta.setDisplayName(ChatColor.GREEN + "Infinite Torch");
                        torchStack.setItemMeta(torchMeta);

                        ItemStack saddleStack = new ItemStack(Material.IRON_SHOVEL, 1);
                        ItemMeta saddleMeta = saddleStack.getItemMeta();
                        saddleMeta.setDisplayName(ChatColor.GREEN + "Pelle de l'Aventurier");
                        saddleMeta.setLore(Arrays.asList(
                                ChatColor.RESET + "Efficace pour creuser mais également pour se défendre",
                                ChatColor.RESET + "Elle possède les mêmes stats de combat qu'une épée en Pierre."));
                        saddleStack.setItemMeta(saddleMeta);

                        String[] names = new String[]{"generic.attackDamage", "generic.attackSpeed"};
                        String[] hand = new String[]{"mainhand", "mainhand"};
                        double[] amout = new double[]{4, -2.4};
                        double[] operation = new double[]{0, 0};

                        saddleStack = NBTItemStack.addAttribute(saddleStack, names, hand, amout, operation, new int[]{17892, 53526}, new int[]{155197, 122973});

                        p.teleport(team.getSpawnLocation());
                        p.setGameMode(GameMode.SURVIVAL);

                        p.getInventory().clear();

                        p.getInventory().addItem(torchStack, potatoStack, saddleStack);
                    }
                }*/

                STATUE = Statue.START;
                return true;
            } else if (label.equalsIgnoreCase("tosviewend") && player.isOp())
            {
                new ClearGame();
                sender.sendMessage("Clear game !");
                return true;
            } else
            {
                player.sendMessage(ChatColor.RED + "Sorry you dont have the permission for this command.");
                return true;
            }
        } else
        {
            return false;
        }
    }

    public void checkConfigFile()
    {
        TEAM_FILE = new File(getDataFolder(), "teams.yml");

        if (!TEAM_FILE.exists() && TEAM_FILE.getParentFile().mkdir())
            this.saveResource("teams.yml", false);

        TOS_FILE = new File(getDataFolder(), "tunnelsofsteel.yml");

        if (!TOS_FILE.exists() && TOS_FILE.getParentFile().mkdir())
            this.saveResource("tunnelsofsteel.yml", false);

        REFINERIES_FILE = new File(getDataFolder(), "refineries.yml");

        if (!REFINERIES_FILE.exists() && REFINERIES_FILE.getParentFile().mkdir())
            this.saveResource("refineries.yml", false);
    }

    public void loadConfig()
    {
        //Create config file if dont exist
        checkConfigFile();

        //TunnelsOfSteel Configuration
        TOS_CONFIG = YamlConfiguration.loadConfiguration(TOS_FILE);

        ITEMS_TO_BURNS.clear();

        for (String s : TOS_CONFIG.getStringList("ITEM-TO-BURNS"))
        {
            String[] args = s.trim().split(":");
            try
            {
                Material material = Material.matchMaterial(args[0].toUpperCase());
                int result = Integer.parseInt(args[1]);

                if (result <= 0)
                {
                    Logger.log("The craft: " + ChatColor.AQUA + args[0] + ChatColor.GOLD + " -> " + ChatColor.AQUA + args[1] + ChatColor.GOLD + " is not possible, the result can be inferior or equals 0");
                    continue;
                }


                ITEMS_TO_BURNS.put(material, result);
                Logger.log("Adding new refineries craft: " + ChatColor.AQUA + args[0] + ChatColor.GOLD + " -> " + ChatColor.AQUA + args[1]);
            } catch (Exception e)
            {
                Logger.log("The item: " + args[0] + " doesn't exist !");
            }
        }

        if (TOS_CONFIG.contains("DEBUG"))
            Logger.DEBUG = TOS_CONFIG.getBoolean("DEBUG");

        //Check in tunnelsofsteel.yml is MySQL is present

        CONFIG = null;
        //If true, use MySQL configuration
        //Else use a local database
        if (TOS_CONFIG.contains("MySQL.Enabled") && TOS_CONFIG.getBoolean("MySQL.Enabled"))
        {
            Logger.logDebug("New connection on MySQL");

            CONFIG = new Config(
                    TOS_CONFIG.getString("MySQL.Hotsname"),
                    TOS_CONFIG.getString("MySQL.Username"),
                    TOS_CONFIG.getString("MySQL.Password"),
                    TOS_CONFIG.getString("MySQL.Port"),
                    TOS_CONFIG.getString("MySQL.Database"),
                    Config.SQLType.MY_SQL,
                    //Add all table
                    PLAYERS_TABLE
            );
        } else
        {
            System.out.println("Open SQLite file.");
            File databaseFile = new File(getDataFolder(), "TunnelOfSteel.db");

            if (!databaseFile.exists())
            {
                try
                {
                    //noinspection ResultOfMethodCallIgnored
                    databaseFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            CONFIG = new Config(databaseFile, Config.SQLType.SQLITE, PLAYERS_TABLE);
        }

        //INSTANCE TunnelsOfSteelConfig
        if (TOS_CONFIG.contains("Statue"))
            STATUE = Statue.valueOf(TOS_CONFIG.getString("Statue"));

        if (TOS_CONFIG.contains("Timer"))
            GAME_SCOREBOARD.setMaxSecond(TOS_CONFIG.getInt("Timer"));


        //Team config
        TEAMS_CONFIG = YamlConfiguration.loadConfiguration(TEAM_FILE);

        TeamManager.removeAll();

        ConfigurationSection teamsSection = TEAMS_CONFIG.getConfigurationSection("teams");

        if (teamsSection != null)
        {
            for (String name : teamsSection.getKeys(false))
            {
                String path = "teams." + name;

                Color color = Color.match(name);

                TOSTeam team = TeamManager.registerTeam(color);

                if (team == null)
                    team = TeamManager.getTeam(color);

                team.addTalentium(TEAMS_CONFIG.getInt(path + ".score"));

                if (TEAMS_CONFIG.contains(path + ".spawnLocation"))
                    team.setSpawnLocation(toLocationString(TEAMS_CONFIG.getString(path + ".spawnLocation")));

                if (TEAMS_CONFIG.contains(path + ".region"))
                    team.setTeamRegion((TeamRegion) REGION_MANAGER.getRegion(TEAMS_CONFIG.getString(path + ".region")));

                if (TEAMS_CONFIG.contains(path + ".absorbingregion"))
                    team.setAbsorbingRegion((AbsorbingRegion) REGION_MANAGER.getRegion(TEAMS_CONFIG.getString(path + ".absorbingregion")));

                if (TEAMS_CONFIG.contains(path + ".players"))
                    for (String uuid : TEAMS_CONFIG.getStringList(path + ".players")) {
                        OfflinePlayer offlinePlayer = TOSUtils.getOfflinePlayer(UUID.fromString(uuid));
                        if (offlinePlayer != null) {
                            System.out.println("loadConfig: uuid of " + offlinePlayer.getName() + " for join team: " + team.getName() + " is " + uuid);
                            team.join(offlinePlayer);
                        }
                    }

                if (TEAMS_CONFIG.contains(path + ".enderchest"))
                    for (String location : TEAMS_CONFIG.getStringList(path + ".enderchest"))
                        team.addEncherChest(toLocationString(location));

                if (TEAMS_CONFIG.contains(path + ".depositMaterial"))
                    team.setDepositBotomMaterial(Material.valueOf(TEAMS_CONFIG.getString(path + ".depositMaterial")));
            }
        }

        //Refineries Configuration
        REFINERIES_CONFIG = YamlConfiguration.loadConfiguration(REFINERIES_FILE);

        ConfigurationSection refineriesSection = REFINERIES_CONFIG.getConfigurationSection("refineries");

        RefineriesManager.clear();

        if (refineriesSection != null)
        {
            for (String name : refineriesSection.getKeys(false))
            {
                String path = "refineries." + name;
                Location refineriesLoc = toLocationString(REFINERIES_CONFIG.getString(path + ".location"));

                if (refineriesLoc != null)
                {
                    Refineries refineries = RefineriesManager.registerRefineries(refineriesLoc, name);
                    if (refineries != null)
                    {
                        Upgrade upgrade = Upgrade.valueOf(refineriesSection.getString(path + ".upgrade.value"));
                        refineries.setUpgrade(upgrade);

                        if (upgrade != Upgrade.DEFAULT && upgrade != Upgrade.PRIVATE && REFINERIES_CONFIG.contains(path + ".upgrade.item"))
                        {
                            refineries.getInventory().setItem(22, REFINERIES_CONFIG.getItemStack(path + ".upgrade.item"));
                        } else if (upgrade == Upgrade.PRIVATE)
                        {
                            ItemStack stack = REFINERIES_CONFIG.getItemStack(path + ".upgrade.item");
                            Item item = refineries.getFurnaceLoc().getWorld().dropItemNaturally(refineries.getFurnaceLoc().clone().add(refineries.getFacing().getModX() + 0.5D, 0.5, refineries.getFacing().getModZ() + +0.5D), stack);
                            item.setVelocity(new Vector(refineries.getFacing().getModX(), 0.2, refineries.getFacing().getModZ()).multiply(0.4D));
                            refineries.getInventory().setItem(22, new ItemStack(Material.AIR));
                        }

                        if (REFINERIES_CONFIG.contains(path + ".region"))
                        {
                            Region region = REGION_MANAGER.getRegion(REFINERIES_CONFIG.getString(path + ".region"));

                            if (region != null)
                                refineries.setRegion(region);
                        }

                        if (REFINERIES_CONFIG.contains(path + ".input"))
                            refineries.setInputStack(REFINERIES_CONFIG.getItemStack(path + ".input"));

                        if (REFINERIES_CONFIG.contains(path + ".output"))
                            refineries.setOutputStack(REFINERIES_CONFIG.getItemStack(path + ".output"));

                        if (REFINERIES_CONFIG.contains(path + ".visualBurning"))
                            for (String stringLoc : REFINERIES_CONFIG.getStringList(path + ".visualBurning"))
                                refineries.addBurningBlock(toLocationString(stringLoc));
                    }
                } else
                {
                    REFINERIES_CONFIG.set(path, null);
                }
            }
        }
    }

    public void saveConfig()
    {
        checkConfigFile();

        //Save team
        if (TEAMS_CONFIG != null)
        {
            TEAMS_CONFIG.set("teams", null);
            for (TOSTeam team : TeamManager.getRegisteredTeam())
            {
                String path = "teams." + team.getName();

                TEAMS_CONFIG.set(path + ".score", team.getTalentium());

                if (team.getSpawnLocation() != null)
                    TEAMS_CONFIG.set(path + ".spawnLocation", toStringLocation(team.getSpawnLocation()));

                if (team.getTeamRegion() != null)
                    TEAMS_CONFIG.set(path + ".region", team.getTeamRegion().getName());


                if (team.getAbsorbingRegion() != null)
                    TEAMS_CONFIG.set(path + ".absorbingregion", team.getAbsorbingRegion().getName());

                if (team.getPlayers().size() > 0)
                {
                    ArrayList<String> uuidsPlayer = new ArrayList<>();

                    team.getTeam().getEntries().forEach(entry ->
                    {
                        if (entry.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}")) {
                            Entity entity = Bukkit.getEntity(UUID.fromString(entry));
                            if (entity instanceof OfflinePlayer)
                                uuidsPlayer.add(entry);
                        }
                    });

                    TEAMS_CONFIG.set(path + ".players", uuidsPlayer);
                }

                if (team.getEnderChests().size() > 0)
                {
                    ArrayList<String> enderChestLoc = new ArrayList<>();

                    for (Location location : team.getEnderChests())
                        enderChestLoc.add(toStringLocation(location));

                    TEAMS_CONFIG.set(path + ".enderchest", enderChestLoc);
                }

                if (team.getDepositBotomMaterial() != null)
                    TEAMS_CONFIG.set(path + ".depositMaterial", team.getDepositBotomMaterial().name());
            }

            try
            {
                TEAMS_CONFIG.save(TEAM_FILE);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        //save refineries
        if (REFINERIES_CONFIG != null)
        {
            REFINERIES_CONFIG.set("refineries", null);

            for (Refineries refineries : RefineriesManager.getRefineries())
            {
                String path = "refineries." + refineries.getRefineriesName();

                REFINERIES_CONFIG.set(path + ".location", toStringLocation(refineries.getFurnaceLoc()));
                REFINERIES_CONFIG.set(path + ".upgrade.value", refineries.getUpgrade().name());

                if (refineries.getUpgrade() != Upgrade.DEFAULT)
                    REFINERIES_CONFIG.set(path + ".upgrade.item", refineries.getInventory().getItem(22));

                if (refineries.getRegion() != null)
                    REFINERIES_CONFIG.set(path + ".region", refineries.getRegion().getName());

                if (refineries.getBurningBlocks().size() > 0)
                {
                    List<String> burnings = new ArrayList<>();

                    for (Location location : refineries.getBurningBlocks())
                        burnings.add(toStringLocation(location));

                    REFINERIES_CONFIG.set(path + ".visualBurning", burnings);
                }

                if (refineries.getInputStack() != null)
                    REFINERIES_CONFIG.set(path + ".input", refineries.getInputStack());

                if (refineries.getOutputStack() != null)
                    REFINERIES_CONFIG.set(path + ".output", refineries.getOutputStack());
            }

            try {
                REFINERIES_CONFIG.save(REFINERIES_FILE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TOS_CONFIG.set("Statue", STATUE.name());
        TOS_CONFIG.set("Timer", GAME_SCOREBOARD.getCurrentMaxSecond());
    }

    public static String toStringLocation(Location location)
    {
        World world = location.getWorld();
        return world == null ? Bukkit.getWorlds().get(0).getName() : world.getName() + ":" +
                location.getX() + ":" +
                location.getY() + ":" +
                location.getZ();
    }

    public static Location toLocationString(String location)
    {
        if (location != null && !location.isEmpty())
        {
            String[] strings = location.split(":");
            return Bukkit.getWorld(strings[0]) == null ? null :
                    new Location(Bukkit.getWorld(strings[0]), Double.parseDouble(strings[1]), Double.parseDouble(strings[2]), Double.parseDouble(strings[3]));
        } else
            return null;
    }

    public static ItemStack getTalentiumItem()
    {
        ItemStack itemStack = new ItemStack(Material.MAGMA_CREAM, 1);
        ItemMeta meta = itemStack.getItemMeta();

        if (meta == null)
            return null;

        meta.setDisplayName(RefineriesManager.TALENTIUM_NAME);
        meta.addItemFlags(ItemFlag.values());
        itemStack.setItemMeta(meta);
        return itemStack;
    }
}
