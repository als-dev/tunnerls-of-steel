package fr.bretzel.tunelsofsteel.region;

import fr.bretzel.region.SimpleRegion;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.util.Selection;

public class AbsorbingRegion extends SimpleRegion
{
    public AbsorbingRegion()
    {
        super();
    }

    public AbsorbingRegion(Selection selection, TOSTeam owner)
    {
        super("TOS_A_" + owner.getName(), selection.getLocationOne(), selection.getLocationTwo());
    }
}
