package fr.bretzel.tunelsofsteel.region;

import fr.bretzel.region.SimpleRegion;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.util.Selection;
import org.bukkit.Location;

public class GameRegion extends SimpleRegion
{

    public GameRegion()
    {
        super();
    }

    public GameRegion(Selection selection)
    {
        super(TunnelsOfSteel.GAME_REGION_NAME, selection.getLocationOne(), selection.getLocationTwo());
    }

}
