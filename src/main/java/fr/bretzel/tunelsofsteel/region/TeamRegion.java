package fr.bretzel.tunelsofsteel.region;

import fr.bretzel.region.SimpleRegion;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Selection;
import org.bukkit.configuration.file.YamlConfiguration;

public class TeamRegion extends SimpleRegion
{
    private Color team;

    public TeamRegion()
    {
        super();
    }

    public TeamRegion(Selection selection, TOSTeam owner)
    {
        super("TOS_" + owner.getColor().name(), selection.getLocationOne(), selection.getLocationTwo());

        this.team = owner.getColor();
    }

    public TOSTeam getOwner()
    {
        return TeamManager.getTeam(team);
    }

    @Override
    public void load(YamlConfiguration configuration) {
        super.load(configuration);
        this.team = Color.match(configuration.getString("Team"));
    }

    @Override
    public YamlConfiguration save(YamlConfiguration configuration) {
        super.save(configuration);
        configuration.set("Team", team.name());
        return configuration;
    }
}
