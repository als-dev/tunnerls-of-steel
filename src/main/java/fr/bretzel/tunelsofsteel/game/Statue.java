package fr.bretzel.tunelsofsteel.game;

public enum Statue
{
    WAITING, START, PAUSED, END
}
