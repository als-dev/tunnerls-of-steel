package fr.bretzel.tunelsofsteel.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.logging.Level;

public class Logger
{

    public static boolean DEBUG = false;

    public static void log(String message)
    {
        if (Bukkit.getConsoleSender() != null)
        {
            Bukkit.getConsoleSender().sendMessage("[" + ChatColor.AQUA + "TunnelsOfSteel" + ChatColor.WHITE + "] " + ChatColor.GOLD + message);
        } else
        {
            Bukkit.getLogger().log(Level.INFO, message);
        }
    }

    public static void logDebug(String message)
    {
        log(ChatColor.RED + "Debug: " + ChatColor.RESET + message);
    }

}
