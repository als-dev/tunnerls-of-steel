package fr.bretzel.tunelsofsteel.util;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.*;

import java.util.List;
import java.util.Random;

public class PlaceBlockRunnable implements Runnable {
    private List<Location> locationList;
    private World world;
    private Material material;
    private Sound sound;
    private boolean random;
    private boolean spawnParticle;
    private int blockToPlaceInTick;

    private int index;

    private int taskId;

    public PlaceBlockRunnable(World world, Material material, Sound sound, boolean random, boolean spawnParticle, List<Location> locations, long everyTick, int blockToPlaceInTick) {
        if (locations.isEmpty())
            return;

        if (blockToPlaceInTick < 1)
            blockToPlaceInTick = 1;

        if (everyTick < 1)
            everyTick = 1;

        taskId = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, everyTick).getTaskId();

        this.world = world;
        this.material = material;
        this.sound = sound;
        this.spawnParticle = spawnParticle;
        this.random = random;
        this.blockToPlaceInTick = blockToPlaceInTick;
        this.locationList = locations;
        this.index = this.locationList.size() - 1;
    }

    @Override
    public void run() {
        for (int i = 0; i <= blockToPlaceInTick; i++)
            if (index >= 0 && index < locationList.size()) {
                Location location;

                if (random) {
                    int rdmI = new Random().nextInt(locationList.size()) - 1;

                    if (rdmI < 0)
                        rdmI = 0;

                    location = locationList.get(rdmI);
                    locationList.remove(rdmI);
                } else {
                    location = locationList.get(index);
                }

                if (spawnParticle)
                    world.spawnParticle(Particle.BLOCK_CRACK, location, 8 + new Random().nextInt(8), 1D, 1D, 1D, material.createBlockData());
                if (sound != null)
                    world.playSound(location, sound, SoundCategory.BLOCKS, 1, 1);

                world.getBlockAt(location).setType(material);

                index--;
            } else {
                locationList.clear();
                Bukkit.getScheduler().cancelTask(taskId);
                return;
            }
    }

    public Material getMaterial() {
        return material;
    }

    public World getWorld() {
        return world;
    }
}
