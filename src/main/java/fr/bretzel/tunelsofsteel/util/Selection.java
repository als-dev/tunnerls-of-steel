package fr.bretzel.tunelsofsteel.util;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class Selection
{
    private static HashMap<UUID, Selection> selections = new HashMap<>();

    public static Selection getSelection(Player player)
    {
        if (selections.containsKey(player.getUniqueId()))
        {
            return selections.get(player.getUniqueId());
        } else
        {
            Selection selection = new Selection();
            selections.put(player.getUniqueId(), selection);
            return selection;
        }
    }

    public static Selection removeSelection(Player player)
    {
        if (selections.containsKey(player.getUniqueId()))
        {
            return selections.remove(player.getUniqueId());
        }
        return null;
    }

    private org.bukkit.Location locationOne;
    private org.bukkit.Location locationTwo;

    public Selection()
    {}

    public Selection(Location locationOne, Location locationTwo)
    {
        setLocationOne(locationOne);
        setLocationTwo(locationTwo);
    }

    public org.bukkit.Location getLocationTwo()
    {
        return locationTwo;
    }

    public void setLocationTwo(org.bukkit.Location locationTwo)
    {
        this.locationTwo = locationTwo;
    }

    public org.bukkit.Location getLocationOne()
    {
        return locationOne;
    }

    public void setLocationOne(org.bukkit.Location locationOne)
    {
        this.locationOne = locationOne;
    }

    public boolean is(org.bukkit.Location location) {
        return isLocationTwo(location) || isLocationOne(location);
    }

    public boolean isLocationTwo(org.bukkit.Location location)
    {
        return getLocationTwo().getBlockX() == location.getBlockX() &&
                getLocationTwo().getBlockY() == location.getBlockY() &&
                getLocationTwo().getBlockZ() == location.getBlockZ() &&
                getLocationTwo().getWorld().getName().equals(location.getWorld().getName());
    }

    public boolean isLocationOne(Location location) {
        return getLocationOne().getBlockX() == location.getBlockX() &&
                getLocationOne().getBlockY() == location.getBlockY() &&
                getLocationOne().getBlockZ() == location.getBlockZ() &&
                getLocationOne().getWorld().getName().equals(location.getWorld().getName());
    }

    public double getMinX() {
        return getMin(getLocationOne().getX(), getLocationTwo().getX());
    }

    public double getMaxX() {
        return getMax(getLocationOne().getX(), getLocationTwo().getX());
    }

    public double getMinY() {
        return getMin(getLocationOne().getY(), getLocationTwo().getY());
    }

    public double getMaxY() {
        return getMax(getLocationOne().getY(), getLocationTwo().getY());
    }

    public double getMinZ() {
        return getMin(getLocationOne().getZ(), getLocationTwo().getZ());
    }

    public double getMaxZ() {

        return getMax(getLocationOne().getZ(), getLocationTwo().getZ());
    }

    private double getMax(double pos, double pos1) {
        return pos > pos1 ? pos : pos1;
    }

    private double getMin(double pos, double pos1) {
        return pos < pos1 ? pos : pos1;
    }
}
