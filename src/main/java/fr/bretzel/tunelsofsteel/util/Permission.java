package fr.bretzel.tunelsofsteel.util;

public class Permission
{
    public static final String COMMANDS_TOS_ALSGAME = "tunelsofsteel.commands.alsgame";
    public static final String COMMANDS_TOS_RELOADLOBBY = "tunelsofsteel.commands.reloadlobby";


    public static final String EVENT_TOS_LOBBY_PLACE = "tunelsofsteel.event.lobby.place";
    public static final String EVENT_TOS_LOBBY_BREAK = "tunelsofsteel.event.lobby.break";
    public static final String EVENT_TOS_BYPASS_JOIN = "tunelsofsteel.event.bypass.join";
}
