package fr.bretzel.tunelsofsteel.util;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NBTItemStack
{

    private static String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
    private static Class<?> craftitemstackclass, itemstackclass, nbttagcompoundclass, nbttaglistclass;
    private static Class<?> nbttagstringclass, nbttagintclass, nbtbaseclass, nbtdoubleclass;
    private static Constructor<?> nbttagstringconstructor, nbttagintconstructor, nbttagdoubleconstructor;
    private static Method nbttagcompoundsetmethod,nbttagcompoundgetmethod, nbttaglistaddmethod, itemstacksettagmethod;

    static
    {
        try
        {
            craftitemstackclass = getObcClass("inventory.CraftItemStack");
            itemstackclass = getNmsClass("ItemStack");
            nbttagcompoundclass = getNmsClass("NBTTagCompound");
            nbttaglistclass = getNmsClass("NBTTagList");
            nbttagstringclass = getNmsClass("NBTTagString");
            nbttagintclass = getNmsClass("NBTTagInt");
            nbtdoubleclass = getNmsClass("NBTTagDouble");
            nbtbaseclass = getNmsClass("NBTBase");
            nbttagintconstructor = nbttagintclass.getConstructor(int.class);
            nbttagdoubleconstructor = nbtdoubleclass.getConstructor(double.class);
            nbttagstringconstructor = nbttagstringclass.getConstructor(String.class);
            nbttagcompoundsetmethod = nbttagcompoundclass.getMethod("set", String.class, nbtbaseclass);
            nbttagcompoundgetmethod = nbttagcompoundclass.getMethod("get", String.class);
            nbttaglistaddmethod = nbttaglistclass.getMethod("add", nbtbaseclass);
            itemstacksettagmethod = itemstackclass.getMethod("setTag", nbttagcompoundclass);
        } catch (Exception error)
        {
            error.printStackTrace();
        }
    }

    public static ItemStack addAttribute(ItemStack item, String[] name, String[] slot, double[] amount, double operation[], int[] uuidMost, int[] uuidLeast)
    {
        try
        {
            Object nmsitem = craftitemstackclass.getMethod("asNMSCopy", ItemStack.class).invoke(craftitemstackclass, item);
            Object compound = itemstackclass.getMethod("getTag").invoke(itemstackclass.cast(nmsitem));

            if (compound == null)
            {
                compound = nbttagcompoundclass.newInstance();
                itemstacksettagmethod.invoke(nmsitem, nbttagcompoundclass.cast(compound));
            }

            Object modifiers = nbttaglistclass.newInstance();

            for (int i = name.length - 1; i >= 0; i--)
            {
                Object details = nbttagcompoundclass.newInstance();

                nbttagcompoundsetmethod.invoke(details, "AttributeName", nbttagstringconstructor.newInstance(name[i]));
                nbttagcompoundsetmethod.invoke(details, "Name", nbttagstringconstructor.newInstance(name[i]));
                nbttagcompoundsetmethod.invoke(details, "Slot", nbttagstringconstructor.newInstance(slot[i]));
                nbttagcompoundsetmethod.invoke(details, "Amount", nbttagdoubleconstructor.newInstance(amount[i]));
                nbttagcompoundsetmethod.invoke(details, "Operation", nbttagdoubleconstructor.newInstance(operation[i]));
                nbttagcompoundsetmethod.invoke(details, "UUIDLeast", nbttagintconstructor.newInstance(uuidLeast[i]));
                nbttagcompoundsetmethod.invoke(details, "UUIDMost", nbttagintconstructor.newInstance(uuidMost[i]));

                nbttaglistaddmethod.invoke(modifiers, details);
            }

            nbttagcompoundsetmethod.invoke(compound, "AttributeModifiers", nbttaglistclass.cast(modifiers));
            itemstacksettagmethod.invoke(nmsitem, compound);

            item = (ItemStack) craftitemstackclass.getMethod("asBukkitCopy", itemstackclass).invoke(craftitemstackclass, itemstackclass.cast(nmsitem));

        } catch (Exception error)
        {
            error.printStackTrace();
        }
        return item;
    }

    public static String getAttributes(ItemStack item)
    {
        try
        {
            Object nmsitem = craftitemstackclass.getMethod("asNMSCopy", ItemStack.class).invoke(craftitemstackclass, item);
            Object compound = itemstackclass.getMethod("getTag").invoke(itemstackclass.cast(nmsitem));
            if (compound == null)
            {
                compound = nbttagcompoundclass.newInstance();
                itemstacksettagmethod.invoke(nmsitem, nbttagcompoundclass.cast(compound));
            }
            Object details = nbttagcompoundclass.newInstance();
            return details.toString();
        } catch (Exception error)
        {
            error.printStackTrace();
        }
        return null;
    }

    public static ItemStack addStringNbt(ItemStack stack, String name, String value)
    {
        try
        {
            Object nmsitem = craftitemstackclass.getMethod("asNMSCopy", ItemStack.class).invoke(craftitemstackclass, stack);
            Object compound = itemstackclass.getMethod("getTag").invoke(itemstackclass.cast(nmsitem));

            if (compound == null)
            {
                compound = nbttagcompoundclass.newInstance();
                itemstacksettagmethod.invoke(nmsitem, nbttagcompoundclass.cast(compound));
            }


            nbttagcompoundsetmethod.invoke(compound, name, nbttagstringclass.cast(nbttagstringconstructor.newInstance(value)));

            itemstacksettagmethod.invoke(nmsitem, compound);

            stack = (ItemStack) craftitemstackclass.getMethod("asBukkitCopy", itemstackclass).invoke(craftitemstackclass, itemstackclass.cast(nmsitem));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e)
        {
            e.printStackTrace();
        }

        return stack;
    }

    public static String getNbtString(ItemStack stack, String name)
    {
        try
        {
            Object nmsitem = craftitemstackclass.getMethod("asNMSCopy", ItemStack.class).invoke(craftitemstackclass, stack);
            Object compound = itemstackclass.getMethod("getTag").invoke(itemstackclass.cast(nmsitem));

            if (compound == null)
            {
                return "";
            }

            Object o = nbttagcompoundgetmethod.invoke(compound, name);

            String str = (String) o.getClass().getMethod("toString").invoke(o);

            return str.substring(1, str.length() - 1);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean asNbtKey(ItemStack stack, String name)
    {
        try
        {
            Object nmsitem = craftitemstackclass.getMethod("asNMSCopy", ItemStack.class).invoke(craftitemstackclass, stack);
            Object compound = itemstackclass.getMethod("getTag").invoke(itemstackclass.cast(nmsitem));

            if (compound == null)
            {
                return false;
            }

            Object o = nbttagcompoundgetmethod.invoke(compound, name);

            return o != null;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private static Class<?> getNmsClass(String classname)
    {
        String fullname = "net.minecraft.server." + version + classname;
        Class<?> realclass = null;
        try
        {
            realclass = Class.forName(fullname);
        } catch (Exception error)
        {
            error.printStackTrace();
        }
        return realclass;
    }

    private static Class<?> getObcClass(String classname)
    {
        String fullname = "org.bukkit.craftbukkit." + version + classname;
        Class<?> realclass = null;
        try
        {
            realclass = Class.forName(fullname);
        } catch (Exception error)
        {
            error.printStackTrace();
        }
        return realclass;
    }

}
