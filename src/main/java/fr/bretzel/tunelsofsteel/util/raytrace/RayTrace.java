package fr.bretzel.tunelsofsteel.util.raytrace;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.function.Predicate;

public class RayTrace {

    public static RayTraceResult rayTraceBlock(Location origin, Location target, double blocksAway, double accuracy, Predicate<Block> predicate) {
        Location orp = origin.clone();
        World world = origin.getWorld();
        Validate.notNull(world, "World cannot be null");

        target = target.clone();
        target.setY(target.getBlockY() + 0.5);
        target.setX(target.getBlockX() + 0.5);
        target.setZ(target.getBlockZ() + 0.5);

        Vector progress = target.clone().toVector().subtract(origin.toVector()).normalize().multiply(new Vector(accuracy, accuracy, accuracy));

        for (double d = 0; d <= blocksAway; d += accuracy) {
            orp.add(progress);

            if (predicate.test(orp.getBlock())) {
                if (!orp.getBlock().getType().isAir()) {
                    BoundingBox boundingBox = new BoundingBox(orp.getBlock());

                    if (boundingBox.isInside(orp.toVector())) {
                        //world.spawnParticle(Particle.REDSTONE, orp, 1, new Particle.DustOptions(Color.RED, 1));
                        return new RayTraceResult(orp.toVector(), orp.getBlock(), null);
                    }
                } else {
                    //world.spawnParticle(Particle.REDSTONE, orp, 1, new Particle.DustOptions(Color.RED, 1));
                    return new RayTraceResult(orp.toVector(), orp.getBlock(), null);
                }
            }

            if (target.getBlockX() == orp.getBlockX() && target.getBlockY() == orp.getBlockY() && target.getBlockZ() == orp.getBlockZ()) {
                return new RayTraceResult(new Vector(0, 0, 0));
            }
        }

        return new RayTraceResult(new Vector(0, 0, 0));
    }
}
