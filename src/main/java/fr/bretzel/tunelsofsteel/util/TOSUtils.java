package fr.bretzel.tunelsofsteel.util;

import com.google.common.collect.Sets;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.refineries.RefineriesManager;
import jdk.internal.jline.internal.Nullable;
import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.Packet;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.lang.reflect.Field;
import java.util.*;

public class TOSUtils
{
    public static void removeItem(Inventory inventory, int slot, int remove)
    {
        if (inventory.getSize() < slot)
            return;

        ItemStack itemStack = inventory.getItem(slot);

        if (itemStack == null)
            return;

        int removeItem = itemStack.getAmount() - remove;

        if (removeItem <= 0)
        {
            inventory.setItem(slot, new ItemStack(Material.AIR));
            return;
        }

        itemStack.setAmount(itemStack.getAmount() - remove);

        inventory.setItem(slot, itemStack);
    }

    public static void addInInventoryItem(Inventory inventory, int slot, int add)
    {
        ItemStack stack = inventory.getItem(slot);
        if (stack != null)
            addInInventoryItem(inventory, slot, add, stack);
    }

    public static void addInInventoryItem(Inventory inventory, int slot, int add, ItemStack stack)
    {
        ItemStack itemStack = inventory.getItem(slot);

        add = add + (itemStack == null ? 0 : itemStack.getAmount());

        stack.setAmount(add);

        inventory.setItem(slot, stack);
    }

    public static ItemStack hideEnchantments(ItemStack stack)
    {
        ItemStack itemStack = stack.clone();

        ItemMeta meta = stack.getItemMeta();
        if (meta != null)
        {
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            itemStack.setItemMeta(meta);
        }

        return itemStack;
    }

    public static ItemStack getFirstTalentium(Player player)
    {
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack != null && stack.getType() == Material.MAGMA_CREAM)
            {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && meta.hasDisplayName() && !meta.getDisplayName().isEmpty() && meta.getDisplayName().equals(RefineriesManager.TALENTIUM_NAME))
                {
                    return stack;
                }
            }
        }

        return null;
    }

    public static ItemStack getMaxTalentiumPur(Player player)
    {
        ItemStack talentium = null;
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack != null && stack.getType() == Material.SKELETON_SKULL)
            {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && meta.hasDisplayName() && !meta.getDisplayName().isEmpty() && meta.getDisplayName().equals(ChatColor.YELLOW + "" + ChatColor.BOLD + "TALENT PUR"))
                {
                    if (talentium == null)
                        talentium = stack;
                    else if (stack.getAmount() > talentium.getAmount())
                        talentium = stack;
                }
            }
        }

        return talentium;
    }

    public static ItemStack getMaxTalentium(Player player)
    {
        ItemStack talentium = null;
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack != null && stack.getType() == Material.MAGMA_CREAM)
            {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && meta.hasDisplayName() && !meta.getDisplayName().isEmpty() && meta.getDisplayName().equals(RefineriesManager.TALENTIUM_NAME))
                {
                    if (talentium == null)
                        talentium = stack;
                    else if (stack.getAmount() > talentium.getAmount())
                        talentium = stack;
                }
            }
        }

        return talentium;
    }

    public static ItemStack getRandomStack(Player player)
    {
        ArrayList<ItemStack> arrayList = new ArrayList<>();
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack != null && stack.getType() != Material.TORCH && stack.getType() != Material.IRON_SHOVEL && stack.getType() != Material.BAKED_POTATO)
            {
                arrayList.add(stack);
            }
        }

        if (arrayList.size() > 0)
            return arrayList.get(new Random().nextInt(arrayList.size()));
        else return null;
    }

    public static boolean hasPlaceInInventory(Player player)
    {
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            if (player.getInventory().getItem(i) == null)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean hasTalentiumInInventory(Player player)
    {
        for (int i = 0; i <= player.getInventory().getSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack != null && stack.getType() == Material.MAGMA_CREAM)
            {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && meta.hasDisplayName() && !meta.getDisplayName().isEmpty() && meta.getDisplayName().equals(RefineriesManager.TALENTIUM_NAME))
                {
                    return true;

                }
            }
        }

        return false;
    }

    public static ItemStack generateStack(Material material, int amount, int damage, String[] lore, String display, boolean unbreakable, ItemFlag... flag)
    {
        ItemStack stack = new ItemStack(material, amount, (short) damage);
        ItemMeta meta = stack.getItemMeta();

        assert meta != null;

        meta.setUnbreakable(unbreakable);

        if (display != null && !display.isEmpty())
            meta.setDisplayName(display);

        if (lore != null && lore.length > 0)
            meta.setLore(Arrays.asList(lore));

        if (flag.length > 0)
            meta.addItemFlags(flag);

        stack.setItemMeta(meta);

        return stack;
    }

    public static ItemStack setMarkedAsTOS(ItemStack stack)
    {
        ItemMeta meta = stack.getItemMeta();

        if (meta == null)
            return stack;

        CustomItemTagContainer tagContainer = meta.getCustomTagContainer();

        NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "marked");

        if (tagContainer.hasCustomTag(namespacedKey, ItemTagType.INTEGER))
            return stack;
        else
            tagContainer.setCustomTag(namespacedKey, ItemTagType.INTEGER, 1);

        stack.setItemMeta(meta);
        return stack;
    }

    public static boolean isMarkedAsTOS(ItemStack stack)
    {
        ItemMeta meta = stack.getItemMeta();

        if (meta == null)
            return false;

        NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "marked");

        CustomItemTagContainer tagContainer = meta.getCustomTagContainer();

        if (tagContainer.hasCustomTag(namespacedKey, ItemTagType.INTEGER))
        {
            int bool = tagContainer.getCustomTag(namespacedKey, ItemTagType.INTEGER);

            return bool == 1;

        } else
        {
            return false;
        }
    }

    public static Block getTargetBlock(Player player, int range)
    {
        Set<Material> materials = Sets.newHashSet(Material.AIR);
        return player.getTargetBlock(materials, range);
    }

    public static ItemStack getSkullTexture(UUID owner, String texture)
    {
        try
        {
            GameProfile profile = new GameProfile(owner, null);

            PropertyMap propertyMap = profile.getProperties();

            propertyMap.put("textures", new Property("textures", texture));

            ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1);

            ItemMeta headMeta = head.getItemMeta();

            Class headMetaClass = headMeta.getClass();

            Field profileField = headMetaClass.getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);

            head.setItemMeta(headMeta);

            return head;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static Block getBlockUnderPlayer(Player player)
    {
        Location pLoc = player.getLocation();

        for (int y = pLoc.getBlockY(); y <= 0; y--) {
            pLoc.setY(y);

            if (pLoc.getBlock().getType() != Material.AIR || pLoc.getBlock().getType() != Material.CAVE_AIR || pLoc.getBlock().getType() != Material.VOID_AIR)
                return pLoc.getBlock();
        }

        return null;
    }

    public static void sendPacket(@Nullable Player player, Packet packet) {
        if (player != null) {
            CraftPlayer craftPlayer = ((CraftPlayer) player.getPlayer());
            if (craftPlayer != null) {
                EntityPlayer handle = craftPlayer.getHandle();
                handle.playerConnection.sendPacket(packet);
            }
        }
    }

    public static OfflinePlayer getOfflinePlayer(UUID uuid) {
        return Arrays.stream(Bukkit.getOfflinePlayers()).filter(offlinePlayer -> offlinePlayer.getUniqueId().equals(uuid)).findFirst().orElse(null);
    }
}
