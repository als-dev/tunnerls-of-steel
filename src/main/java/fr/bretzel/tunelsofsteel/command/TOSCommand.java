package fr.bretzel.tunelsofsteel.command;

import com.google.common.collect.ImmutableList;
import fr.bretzel.region.Region;
import fr.bretzel.region.SimpleRegion;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.refineries.Refineries;
import fr.bretzel.tunelsofsteel.refineries.RefineriesManager;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.region.GameRegion;
import fr.bretzel.tunelsofsteel.region.TeamRegion;
import fr.bretzel.tunelsofsteel.scoreboard.LobbyScoreboard;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Selection;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class TOSCommand implements CommandExecutor, TabCompleter
{
    private List<String> MAIN_CHOICES = ImmutableList.of("gtw", "refineries", "game", "reload", "giveall", "team");

    private List<String> TEAM_CHOICES = ImmutableList.of("list", "add", "remove", "setspawn", "setregion", "removeregion", "join", "leave",
            "addender", "removeender", "setdeposit", "setdepositregion", "removedepositregion");

    private List<String> GAME_CHOICES = ImmutableList.of("setregion", "removeregion", "timer");

    /**
     * INSTANCE Command
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args)
    {
        // if sender is a player
        if (sender instanceof Player)
        {
            Player playerSender = (Player) sender;

            if (!playerSender.isOp())
            {
                playerSender.sendMessage(ChatColor.RED + "Nop.");
                return false;
            }

            if (args.length > 0)
            {
                if (args[0].equalsIgnoreCase("game"))
                {
                    if (args.length > 1)
                    {
                        if (args[1].equalsIgnoreCase("setregion"))
                        {
                            Selection selection = Selection.getSelection(playerSender);

                            if (selection == null)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry, please choose a selection with the golden sword");
                                return false;
                            } else if (selection.getLocationOne() == null)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry, please select the first position with the golden sword");
                                return false;
                            } else if (selection.getLocationTwo() == null)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry, please select the second position with the golden sword");
                                return false;
                            }

                            if (TunnelsOfSteel.GAME_REGION != null)
                            {
                                TunnelsOfSteel.REGION_MANAGER.removeRegion(TunnelsOfSteel.GAME_REGION);
                            }

                            GameRegion gameRegion = new GameRegion(selection);
                            TunnelsOfSteel.GAME_REGION = TunnelsOfSteel.REGION_MANAGER.registerNewRegion(gameRegion);

                            playerSender.sendMessage(ChatColor.AQUA + "Successfully set the game region");

                            Selection.removeSelection(playerSender);
                            return true;
                        } else if (args[1].equalsIgnoreCase("removeregion"))
                        {
                            if (TunnelsOfSteel.GAME_REGION == null)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry, the game region is not set");
                                return false;
                            } else {
                                TunnelsOfSteel.REGION_MANAGER.removeRegion(TunnelsOfSteel.GAME_REGION);
                                TunnelsOfSteel.GAME_REGION = null;
                            }

                            playerSender.sendMessage(ChatColor.AQUA + "Successfully removed the game region");
                            return true;
                        } else if (args[1].equalsIgnoreCase("timer"))
                        {
                            if (args.length > 2)
                            {
                                String[] timer = args[2].split(":");

                                int hour;
                                int minute;
                                int second;

                                if (timer.length >= 3)
                                {
                                    hour = Integer.parseInt(timer[0]);
                                    minute = Integer.parseInt(timer[1]);
                                    second = Integer.parseInt(timer[2]);
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Usage /tos game timer hour:minute:second");
                                    return false;
                                }

                                TunnelsOfSteel.GAME_SCOREBOARD.setMaxSecond(second + (minute * 60) + ((hour * 60) * 60));

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully set timer");
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage /tos game timer hour:minute:second");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("sdprogfhpçqdur"))
                        {
                            return true;
                        } else
                        {
                            playerSender.sendMessage(ChatColor.RED + "Usage: /tos game <setregion|removeregion|timer>");
                            return false;
                        }
                    } else
                    {
                        playerSender.sendMessage(ChatColor.RED + "Usage: /tos game <setregion|removeregion|timer>");
                        return false;
                    }
                } else if (args[0].equalsIgnoreCase("gtw"))
                {
                    if (playerSender.getWorld().equals(TunnelsOfSteel.getTOSWorld())) {
                        playerSender.sendMessage(ChatColor.AQUA + "You have teleported to the default world !");
                        playerSender.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                        return true;
                    } else {
                        playerSender.sendMessage(ChatColor.AQUA + "You have teleported to the TunnelsOfSteel world !");
                        playerSender.teleport(new Location(TunnelsOfSteel.getTOSWorld(), 0, 254, 0));
                        return true;
                    }

                } else if (args[0].equalsIgnoreCase("refineries"))
                {
                    if (args.length > 1)
                    {
                        Block playerLookBlock = TOSUtils.getTargetBlock(playerSender, 5);

                        if (args[1].equalsIgnoreCase("remove") && args.length < 3)
                        {
                            if (playerLookBlock.getType() != Material.FURNACE)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not a furnace.");
                                return false;
                            } else if (RefineriesManager.getRefinery(playerLookBlock.getLocation()) == null)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not a refineries.");
                                return false;
                            }

                            //remove refineries
                            RefineriesManager.removeRefineries(playerLookBlock.getLocation());
                            playerSender.sendMessage(ChatColor.AQUA + "Successfully removed the refineries.");
                            return true;
                        }

                        if (getCurrentRefineries().contains(args[1]))
                        {
                            if (args[1].equalsIgnoreCase("add"))
                            {
                                if (args.length > 2)
                                {
                                    String name = args[2];
                                    //check looked block is not null and is a furnace and is not a refineries
                                    if (playerLookBlock.getType() != Material.FURNACE)
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not a furnace.");
                                        return false;
                                    } else if (RefineriesManager.getRefinery(playerLookBlock.getLocation()) != null)
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is already a refineries.");
                                        return false;
                                    } else if (RefineriesManager.getRefinery(name) != null)
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Sorry the name %name% is already used.".replace("%name%", name));
                                        return false;
                                    }

                                    //Register new refineries
                                    RefineriesManager.registerRefineries(playerLookBlock.getLocation(), name);
                                    playerSender.sendMessage(ChatColor.AQUA + "Successfully registered new refineries.");
                                    return true;
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries add <name>");
                                    return false;
                                }
                            } else if (args[1].equalsIgnoreCase("remove"))
                            {
                                if (args.length > 2)
                                {
                                    String name = args[2];
                                    //check looked block is not null and is a furnace and is not a refineries
                                    if (RefineriesManager.getRefinery(name) == null)
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Cannot found refineries %name%".replace("%name%", name));
                                        return false;
                                    }

                                    //Remove new refineries
                                    RefineriesManager.removeRefineries(name);
                                    playerSender.sendMessage(ChatColor.AQUA + "Successfully removed refineries.");
                                    return true;
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries remove <name>");
                                    return false;
                                }
                            } else if (RefineriesManager.getRefinery(args[1]) != null)
                            {
                                String name = args[1];
                                Refineries refineries = RefineriesManager.getRefinery(name);

                                if (args.length > 2 && refineries != null)
                                {
                                    if (args[2].equalsIgnoreCase("addBurning"))
                                    {
                                        if (playerLookBlock.getLocation().getBlock().getType() == Material.AIR)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the looked cannot be a AIR");
                                            return false;
                                        } else if (playerLookBlock.getLocation().clone().add(0, 1, 0).getBlock().getType() != Material.AIR)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the top looked block is not a AIR");
                                            return false;
                                        } else if (refineries.containsBurningBlock(playerLookBlock.getLocation().clone().add(0, 1, 0)))
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the refineries %name% already contains this block.".replace("%name%", name));
                                            return false;
                                        }

                                        refineries.addBurningBlock(playerLookBlock.getLocation().clone().add(0, 1, 0));
                                        playerSender.sendMessage(ChatColor.AQUA + "Successfully added new visual burning effect");
                                        return true;
                                    } else if (args[2].equalsIgnoreCase("removeBurning"))
                                    {
                                        if (playerLookBlock.getLocation().getBlock().getType() == Material.AIR)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the looked cannot be a AIR");
                                            return false;
                                        } else if (playerLookBlock.getLocation().clone().add(0, 1, 0).getBlock().getType() != Material.AIR)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the top looked block is not a AIR");
                                            return false;
                                        } else if (!refineries.containsBurningBlock(playerLookBlock.getLocation().add(0, 1, 0)))
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the refineries %name% doesn't contains the block");
                                            return false;
                                        }

                                        refineries.removeBurningBlock(playerLookBlock.getLocation().clone().add(0, 1, 0));
                                        playerSender.sendMessage(ChatColor.AQUA + "Successfully removed visual burning effect");
                                        return true;
                                    } else if (args[2].equalsIgnoreCase("setregion"))
                                    {
                                        Selection selection = Selection.getSelection(playerSender);

                                        if (selection == null)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry, please choose a selection with the golden sword");
                                            return false;
                                        } else if (selection.getLocationOne() == null)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry, please select the first position with the golden sword");
                                            return false;
                                        } else if (selection.getLocationTwo() == null)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry, please select the second position with the golden sword");
                                            return false;
                                        }

                                        Region region = new SimpleRegion("TOS_R_" + refineries.getRefineriesName().toUpperCase(), selection.getLocationOne(), selection.getLocationTwo());

                                        TunnelsOfSteel.REGION_MANAGER.registerNewRegion(region);

                                        refineries.setRegion(region);

                                        Selection.removeSelection(playerSender);

                                        playerSender.sendMessage(ChatColor.AQUA + "Successfully set the region for refineries " + refineries.getRefineriesName());

                                        return true;
                                    } else if (args[2].equalsIgnoreCase("removeregion"))
                                    {
                                        if (refineries.getRegion() != null)
                                        {
                                            TunnelsOfSteel.REGION_MANAGER.removeRegion(refineries.getRegion());
                                        }

                                        refineries.setRegion(null);
                                        playerSender.sendMessage(ChatColor.AQUA + "Successfully removed region");
                                        return true;
                                    } else
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries %name% <addBurning|removeBurning|setregion|removeregion>".replace("%name%", name));
                                        return false;
                                    }
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries %name% <addBurning|removeBurning>".replace("%name%", name));
                                    return false;
                                }
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries <add|remove|<refineries name>>");
                                return false;
                            }
                        } else
                        {
                            playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries <add|remove|refineries name>");
                            return false;
                        }
                    } else
                    {
                        playerSender.sendMessage(ChatColor.RED + "Usage: /tos refineries <add|remove|<refineries name>>");
                        return false;
                    }
                } else if (args[0].equalsIgnoreCase("giveall")) {
                    PlayerInventory playerInventory = playerSender.getInventory();
                    playerInventory.addItem(TunnelsOfSteel.ABILITIES_MANAGER.getAllItemAbilities().toArray(new ItemStack[]{}));
                    playerInventory.addItem(TunnelsOfSteel.getTalentiumItem());

                    playerSender.sendMessage(ChatColor.AQUA + "Successfully gives all abilities");
                    return true;
                } else if (args[0].equalsIgnoreCase("team"))
                {
                    if (args.length > 1)
                    {
                        if (args[1].equalsIgnoreCase("list"))
                        {
                            if (TeamManager.getRegisteredTeam().size() <= 0)
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry no team are registered");
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.AQUA + "Team list:");
                                for (TOSTeam tosTeam : TeamManager.getRegisteredTeam())
                                    playerSender.sendMessage(ChatColor.RESET + "         " + tosTeam.getColor() + tosTeam.getName());
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("add"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[2] + " is not a correct color");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                if (team == null)
                                {
                                    TeamManager.registerTeam(teamColor);
                                    playerSender.sendMessage(ChatColor.AQUA + "Successfully added team " + teamColor.getChatColor() + teamColor.name());
                                    return true;
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[2] + " is already added");
                                    return false;
                                }
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team add <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("remove"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[2] + " is not a correct color");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                if (team == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[2] + " is not added");
                                    return false;
                                } else
                                {
                                    TeamManager.unregisterTeam(teamColor);
                                    playerSender.sendMessage(ChatColor.AQUA + "Successfully removed " + teamColor.getChatColor() + teamColor.name());
                                    return true;
                                }
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team add <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("setspawn"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                    return false;
                                }

                                if (!TeamManager.isRegistered(teamColor))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not added");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                team.setSpawnLocation(playerSender.getLocation());

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully set spawn for the team " + teamColor.getChatColor() + teamColor.name());
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team setspawn <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("setregion"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                    return false;
                                }

                                if (!TeamManager.isRegistered(teamColor))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not added");
                                    return false;
                                }

                                Selection selection = Selection.getSelection(playerSender);

                                if (selection == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please choose a selection with the golden sword");
                                    return false;
                                } else if (selection.getLocationOne() == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please select the first position with the golden sword");
                                    return false;
                                } else if (selection.getLocationTwo() == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please select the second position with the golden sword");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                TeamRegion region = new TeamRegion(selection, team);

                                TunnelsOfSteel.REGION_MANAGER.registerNewRegion(region);

                                team.setTeamRegion(region);

                                Selection.removeSelection(playerSender);

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully set the region for the team " + teamColor.getChatColor() + team.getName());
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team setregion <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("removeregion"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                    return false;
                                }

                                if (!TeamManager.isRegistered(teamColor))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not added");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                if (team.getTeamRegion() != null)
                                    TunnelsOfSteel.REGION_MANAGER.removeRegion(team.getTeamRegion());

                                team.setTeamRegion(null);

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully removed the region for the team " + teamColor + team.getName());
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team removeregion <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("join"))
                        {
                            if (args.length > 2)
                            {
                                Player player = Bukkit.getPlayer(args[2]);

                                if (player != null)
                                {
                                    if (args.length > 3)
                                    {
                                        Color color = Color.match(args[3]);

                                        if (color == null)
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                            return false;
                                        }

                                        if (!TeamManager.isRegistered(color))
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not registered");
                                            return false;
                                        }

                                        TOSTeam team = TeamManager.getTeam(color);

                                        if (team.containsPlayer(player))
                                        {
                                            playerSender.sendMessage(ChatColor.RED + "Sorry the player is already in team");
                                            return false;
                                        }

                                        TOSTeam teamPlayer = TeamManager.getTeamOfPlayer(player);

                                        if (teamPlayer != null)
                                            teamPlayer.leave(player);

                                        team.join(player);

                                        player.setScoreboard(team.getScoreboard());

                                        LobbyScoreboard.actualiseLobbyScoreboard(player);
                                        playerSender.sendMessage(ChatColor.AQUA + "Successfully add player " + player.getName() + " to the team " + color.getChatColor() + team.getName());
                                        return true;
                                    } else
                                    {
                                        playerSender.sendMessage(ChatColor.RED + "Usage: /tos team join <player> <color>");
                                        return false;
                                    }
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "The player " + args[2] + " is not connected or don't exist");
                                    return false;
                                }
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team join <player> <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("leave"))
                        {
                            if (args.length > 2)
                            {
                                OfflinePlayer offPlayer = Bukkit.getOfflinePlayer(args[2]);

                                if (offPlayer != null) {
                                    TOSTeam team = TeamManager.getTeamOfPlayer(offPlayer);

                                    if (team == null) {
                                        playerSender.sendMessage(ChatColor.RED + "Sorry the player " + offPlayer.getName() + " is not in a team");
                                        return false;
                                    }

                                    team.leave(offPlayer);

                                    if (offPlayer.isOnline())
                                        LobbyScoreboard.actualiseLobbyScoreboard(offPlayer.getPlayer());
                                    playerSender.sendMessage(ChatColor.AQUA + "Successfully removed player " + offPlayer.getName() + " to the team " + team.getColor() + team.getColor().name());
                                    return true;
                                } else
                                {
                                    playerSender.sendMessage(ChatColor.RED + "The player " + args[2] + " is not connected or don't exist");
                                    return false;
                                }
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team leave <player> <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("addender"))
                        {
                            if (args.length > 2)
                            {
                                Block playerLookBlock = TOSUtils.getTargetBlock(playerSender, 5);

                                Color color = Color.match(args[2]);

                                if (!TeamManager.isRegistered(color))
                                {
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(color);

                                if (playerLookBlock.getType() != Material.ENDER_CHEST)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not correct");
                                    return false;
                                }

                                if (TeamManager.isEnderChest(playerLookBlock.getLocation()))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is already a ender chest for team");
                                    return false;
                                }

                                team.addEncherChest(playerLookBlock.getLocation());
                                playerSender.sendMessage(ChatColor.AQUA + "Successfully added new ender check");
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team addender <color> <looked>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("removeender"))
                        {
                            if (args.length > 2)
                            {
                                Block playerLookBlock = TOSUtils.getTargetBlock(playerSender, 5);

                                Color color = Color.match(args[2]);

                                if (!TeamManager.isRegistered(color))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team is not registered");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(color);

                                if (playerLookBlock.getType() != Material.ENDER_CHEST)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not correct");
                                    return false;
                                }

                                if (!team.isEnderChest(playerLookBlock.getLocation()))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the looked block is not a correct ender chest for team " + team.getName());
                                    return false;
                                }

                                team.removeEncherChest(playerLookBlock.getLocation());

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully removed ender check");
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team removeender <color> <looked>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("setdeposit"))
                        {
                            Material material = playerSender.getInventory().getItemInMainHand().getType();

                            if (!material.isBlock())
                            {
                                playerSender.sendMessage(ChatColor.RED + "Sorry the item in your hand is not a block");
                                return false;
                            }

                            if (args.length > 2)
                            {
                                Color color = Color.match(args[2]);

                                if (!TeamManager.isRegistered(color))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team is not registered");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(color);

                                team.setDepositBotomMaterial(material);

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully set the deposit block");
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team setdeposit <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("setdepositregion"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                    return false;
                                }

                                if (!TeamManager.isRegistered(teamColor))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not added");
                                    return false;
                                }

                                Selection selection = Selection.getSelection(playerSender);

                                if (selection == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please choose a selection with the golden sword");
                                    return false;
                                } else if (selection.getLocationOne() == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please select the first position with the golden sword");
                                    return false;
                                } else if (selection.getLocationTwo() == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, please select the second position with the golden sword");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                if (team.getAbsorbingRegion() != null)
                                {
                                    TunnelsOfSteel.REGION_MANAGER.removeRegion(team.getAbsorbingRegion());
                                    team.setAbsorbingRegion(null);
                                }

                                team.setAbsorbingRegion(TunnelsOfSteel.REGION_MANAGER.registerNewRegion(new AbsorbingRegion(selection, team)));

                                Selection.removeSelection(playerSender);

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully set the deposit region for the team " + teamColor.getChatColor() + team.getColor().name());
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team setdepositregion <color>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("removedepositregion"))
                        {
                            if (args.length > 2)
                            {
                                Color teamColor = Color.match(args[2]);

                                if (teamColor == null)
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry, the color " + args[3] + " is not a correct color");
                                    return false;
                                }

                                if (!TeamManager.isRegistered(teamColor))
                                {
                                    playerSender.sendMessage(ChatColor.RED + "Sorry the team " + args[3] + " is not added");
                                    return false;
                                }

                                TOSTeam team = TeamManager.getTeam(teamColor);

                                if (team.getAbsorbingRegion() != null)
                                    TunnelsOfSteel.REGION_MANAGER.removeRegion(team.getAbsorbingRegion());

                                team.setAbsorbingRegion(null);

                                playerSender.sendMessage(ChatColor.AQUA + "Successfully removed the deposit region for the team " + teamColor + team.getColor().name());
                                return true;
                            } else
                            {
                                playerSender.sendMessage(ChatColor.RED + "Usage: /tos team removedepositregion <color>");
                                return false;
                            }
                        } else
                        {
                            playerSender.sendMessage(ChatColor.RED + "Usage: /tos team <list|add|remove|setspawn|setregion|removeregion|join|leave" +
                                    "|addender|removeender|setdeposit|setdepositregion|removedepositregion>");
                            return false;
                        }
                    } else
                    {
                        playerSender.sendMessage(ChatColor.RED + "Usage: /tos team <list|add|remove|setspawn|setregion|join|leave" +
                                "|addender|removeender|setdeposit|setdepositregion|removedepositregion>");
                        return false;
                    }
                } else if (args[0].equalsIgnoreCase("reload"))
                {
                    try
                    {
                        TunnelsOfSteel.INSTANCE.saveConfig();
                        TunnelsOfSteel.INSTANCE.loadConfig();
                        TOSPlayer.saveAllAndClear();
                    } catch (Exception e)
                    {
                        playerSender.sendMessage(ChatColor.RED + "An a error has been detect on reload.");
                        e.printStackTrace();
                        return false;
                    }
                    playerSender.sendMessage(ChatColor.AQUA + "Successfully reloaded config.");
                    return true;
                } else
                {
                    playerSender.sendMessage(ChatColor.RED + "Usage: /tos <game|refineries|giveall|reload|team>");
                    return true;
                }
            } else
            {
                playerSender.sendMessage(ChatColor.RED + "Usage: /tos <game|refineries|giveall|reload|team>");
                return true;
            }

        } else
        {
            sender.sendMessage(ChatColor.RED + "Sorry you do not a player.");
            return false;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
    {
        if (args.length == 1)
        {
            return StringUtil.copyPartialMatches(args[0], MAIN_CHOICES, new ArrayList<>());
        }

        if (args.length > 1)
        {
            if (args[0].equalsIgnoreCase("refineries"))
            {
                if (args.length == 2)
                {
                    return StringUtil.copyPartialMatches(args[1], getCurrentRefineries(), new ArrayList<>());
                }

                if (RefineriesManager.getRefinery(args[1]) != null)
                {
                    if (args.length == 3)
                        return StringUtil.copyPartialMatches(args[2], ImmutableList.of("addBurning", "removeBurning", "setregion", "removeregion"), new ArrayList<>());
                }
            } else if (args[0].equalsIgnoreCase("team"))
            {
                if (args.length == 2)
                    return StringUtil.copyPartialMatches(args[1], TEAM_CHOICES, new ArrayList<>());

                if (args[1].equalsIgnoreCase("add"))
                    return StringUtil.copyPartialMatches(args[2], getTeamColorChoise(), new ArrayList<>());

                if (args[1].equalsIgnoreCase("remove"))
                    return StringUtil.copyPartialMatches(args[2], getRegisteredTeam(), new ArrayList<>());

                if (args[1].equalsIgnoreCase("addender") || args[1].equalsIgnoreCase("removeender"))
                    return StringUtil.copyPartialMatches(args[2], getRegisteredTeam(), new ArrayList<>());

                if (args[1].equalsIgnoreCase("join") || args[1].equalsIgnoreCase("leave"))
                {
                    if (args.length == 3)
                        return StringUtil.copyPartialMatches(args[2], getPlayerInTOSWorld(), new ArrayList<>());

                    if (args.length == 4)
                        return StringUtil.copyPartialMatches(args[3], getRegisteredTeam(), new ArrayList<>());
                }

                if (args[1].equalsIgnoreCase("setspawn") || args[1].equalsIgnoreCase("setregion") ||
                        args[1].equalsIgnoreCase("removeregion") || args[1].equalsIgnoreCase("setdepositregion") ||
                        args[1].equalsIgnoreCase("removedepositregion") || args[1].equalsIgnoreCase("setdeposit"))
                {
                    return StringUtil.copyPartialMatches(args[2], getRegisteredTeam(), new ArrayList<>());
                }
            } else if (args[0].equalsIgnoreCase("game"))
            {
                if (args.length == 2)
                    return StringUtil.copyPartialMatches(args[1], GAME_CHOICES, new ArrayList<>());
            }
        }

        return ImmutableList.of();
    }

    private List<String> getTeamColorChoise()
    {
        List<String> list = new ArrayList<>();

        for (Color color : Color.values())
            list.add(color.name().toLowerCase());

        return list;
    }

    private List<String> getRegisteredTeam()
    {
        if (TeamManager.getRegisteredTeam().size() > 0)
        {
            List<String> list = new ArrayList<>();

            for (TOSTeam team : TeamManager.getRegisteredTeam())
                list.add(team.getColor().name().toLowerCase());

            return list;
        } else
        {
            return ImmutableList.of("none");
        }
    }

    private List<String> getCurrentRefineries()
    {
        List<String> list = new ArrayList<>();
        list.add("add");
        list.add("remove");

        for (Refineries refineries : RefineriesManager.getRefineries())
        {
            if (refineries != null && refineries.getRefineriesName() != null)
                list.add(refineries.getRefineriesName());
        }

        return list;
    }

    private List<String> getPlayerInTOSWorld()
    {
        List<String> list = new ArrayList<>();

        for (Player player : TunnelsOfSteel.getTOSWorld().getPlayers()) {
            list.add(player.getName());
        }

        return list;
    }
}
