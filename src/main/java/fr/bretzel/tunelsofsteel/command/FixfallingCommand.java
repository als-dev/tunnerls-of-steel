package fr.bretzel.tunelsofsteel.command;

import fr.bretzel.tunelsofsteel.util.Selection;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class FixfallingCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Selection selection = Selection.getSelection(player);
            if (selection.getLocationOne() == null) {
                sender.sendMessage(ChatColor.RED + "Please select the first location");
                return false;
            }

            if (selection.getLocationTwo() == null) {
                sender.sendMessage(ChatColor.RED + "Please select the second location");
                return false;
            }

            if (args.length <= 0) {
                sender.sendMessage(ChatColor.RED + "Usage: /fixfalling <block>");
                return false;
            }

            Material material = Material.matchMaterial(args[0], false);

            if (material == null) {
                sender.sendMessage(ChatColor.RED + "The block: " + args[0] + " d'ont exist !");
                return false;
            }

            for (int z = (int) selection.getMinZ(); z <= selection.getMaxZ(); z++) {
                for (int x = (int) selection.getMinX(); x <= selection.getMaxX(); x++) {
                    for (int y = (int) selection.getMaxY(); y >= selection.getMinY(); y--) {
                        Location location = new Location(selection.getLocationOne().getWorld(), x, y, z);
                        if (location.getBlock().getType() != material) {
                            location.getBlock().setType(material, false);
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            List<String> strings = new ArrayList<>();
            for (Material m : Material.values())
                if (m.isBlock() && m.hasGravity() && !m.isLegacy())
                    strings.add(m.name().toLowerCase());
            return StringUtil.copyPartialMatches(args[0], strings, new ArrayList<>());
        }
        return null;
    }
}
