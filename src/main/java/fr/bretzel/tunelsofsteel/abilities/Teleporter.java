package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.event.TeamUpdateEvent;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Material;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.Team;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Teleporter extends Ability {

    public static String item_name = ChatColor.AQUA + "" + ChatColor.BOLD + "TELEPORTER";
    private static HashMap<TOSTeam, List<TeleporterInfo>> teleporterInfosMap = new HashMap<>();
    private double seconde_to_tp = 1.5D;
    private Sound sound_start_tp = Sound.BLOCK_FENCE_GATE_OPEN;
    private float volume_start_tp = 0.5F;
    private float pitch_start_tp = 0.1F;
    private Sound sound_leave_tp = Sound.BLOCK_FIRE_EXTINGUISH;
    private float volume_leave_tp = 0.3F;
    private float pitch_leave_tp = 2F;
    private ArrayList<String> lore = new ArrayList<>();
    private static HashMap<UUID, TeleporterTask> playerToMove = new HashMap<>();
    private static List<UUID> teleportedPlayer = new ArrayList<>();
    private HashMap<UUID, Location> firstLocation = new HashMap<>();

    public Teleporter() {
        super("Teleporter");
    }

    public static List<TeleporterInfo> getTeleporters() {
        ArrayList<TeleporterInfo> list = new ArrayList<>();
        teleporterInfosMap.values().forEach(list::addAll);
        return list;
    }

    public static void addTeleporters(TeleporterInfo teleporterInfo) {
        if (teleporterInfo.getLocationOne().getBlock().getType() == Material.WHITE_STAINED_GLASS && teleporterInfo.getLocationTwo().getBlock().getType() == Material.WHITE_STAINED_GLASS) {
            if (teleporterInfosMap.containsKey(teleporterInfo.getOwner())) {
                teleporterInfosMap.get(teleporterInfo.getOwner()).add(teleporterInfo);
            } else {
                ArrayList<TeleporterInfo> list = new ArrayList<>();
                list.add(teleporterInfo);
                teleporterInfosMap.put(teleporterInfo.getOwner(), list);
            }
        }
    }

    @EventHandler
    public void onPlayerPlaceBlock(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        ItemStack stack = event.getItemInHand();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

        if (stack.getType() == Material.WHITE_STAINED_GLASS && tosPlayer.getTeam() != null) {
            ItemMeta meta = stack.getItemMeta();

            if (meta != null && !meta.getDisplayName().isEmpty()) {
                if (meta.getDisplayName().equals(item_name) && !firstLocation.containsKey(player.getUniqueId())) {
                    player.sendMessage(ChatColor.AQUA + "First teleportation point has been set");
                    firstLocation.put(player.getUniqueId(), block.getLocation());
                } else if (meta.getDisplayName().equals(item_name) && firstLocation.containsKey(player.getUniqueId())) {
                    player.sendMessage(ChatColor.AQUA + "Second teleportation point has been set");
                    addTeleporters(new TeleporterInfo(firstLocation.get(player.getUniqueId()), block.getLocation(), tosPlayer.getTeam()));
                    firstLocation.remove(player.getUniqueId());
                }
            }
        }
    }

    @EventHandler
    public void onPlayerBreakBlock(BlockBreakEvent event)
    {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (isTeleporterBlock(block.getLocation())) {
            TeleporterInfo teleporterInfo = getTeleporterInfo(block.getLocation());

            teleporterInfo.getLocationOne().getBlock().setType(Material.AIR);
            teleporterInfo.getLocationTwo().getBlock().setType(Material.AIR);

            teleporterInfo.removeAllGlowingTeleporter();

            player.sendMessage(ChatColor.RED + "Removed Teleporter");

            event.setCancelled(true);

            teleporterInfosMap.get(teleporterInfo.getOwner()).remove(teleporterInfo);
        } else {
            List<UUID> toRemove = new ArrayList<>();

            firstLocation.forEach((plUUD, location) ->
            {
                Player ownerPlayer = Bukkit.getPlayer(plUUD);

                if (location.getWorld() != null && block.getLocation().getWorld() != null &&
                        block.getLocation().getBlockX() == location.getBlockX() &&
                        block.getLocation().getBlockY() == location.getBlockY() &&
                        block.getLocation().getBlockZ() == location.getBlockZ() &&
                        block.getLocation().getWorld().getName().equals(location.getWorld().getName())) {
                    toRemove.add(plUUD);
                    if (player.getUniqueId().equals(plUUD)) {
                        player.sendMessage(ChatColor.RED + "Remove first location");

                    } else {
                        if (ownerPlayer != null && ownerPlayer.isOnline()) {
                            ownerPlayer.sendMessage(ChatColor.RED + "A another player has removed your first location of your teleporter !");
                        }
                        player.sendMessage(ChatColor.RED + "Remove first location of a teleporter !");
                    }
                    location.getWorld().dropItemNaturally(location.clone().add(0.5D, 0.2D, 0.5D), getAbilityItem().get(0));
                    location.getBlock().setType(Material.AIR);
                    event.setCancelled(true);
                }
            });

            toRemove.forEach(uuid -> firstLocation.remove(uuid));
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Location base = null;

        if (event.getTo() != null)
            base = event.getTo().clone().subtract(0, 1, 0);

        Player player = event.getPlayer();

        if (base != null && isTeleporterBlock(base) && !playerToMove.containsKey(player.getUniqueId()) && !teleportedPlayer.contains(player.getUniqueId())) {
            player.playSound(base.clone().add(0, 2, 0), sound_start_tp, volume_start_tp, pitch_start_tp);
            playerToMove.put(player.getUniqueId(), new TeleporterTask(event.getPlayer(), getTeleporterInfo(base), base, seconde_to_tp));

        } else if (base != null && !isTeleporterBlock(base) && playerToMove.containsKey(player.getUniqueId())) {

            playerToMove.get(player.getUniqueId()).cancel();
            playerToMove.remove(player.getUniqueId());

            player.playSound(base.clone().add(0, 2, 0), sound_leave_tp, volume_leave_tp, pitch_leave_tp);

        } else if (base != null && !isTeleporterBlock(base)) {
            teleportedPlayer.remove(player.getUniqueId());
        }
    }

    @EventHandler
    public void onTeamUpdate(TeamUpdateEvent event) {
        if (event.getTeam() != null && teleporterInfosMap.containsKey(event.getTeam())) {
            for (TeleporterInfo teleporterInfo : teleporterInfosMap.get(event.getTeam())) {
                teleporterInfo.actualiseViewer();
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        TOSTeam team = TOSPlayer.getPlayer(player).getTeam();

        if (team != null && teleporterInfosMap.containsKey(team)) {
            for (TeleporterInfo teleporterInfo : teleporterInfosMap.get(team)) {
                teleporterInfo.playerViewer.remove(player);
            }
        }
    }

    @EventHandler
    public void onPlayerConnect(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        TOSTeam team = TOSPlayer.getPlayer(player).getTeam();

        if (team != null && teleporterInfosMap.containsKey(team)) {
            for (TeleporterInfo teleporterInfo : teleporterInfosMap.get(team)) {
                teleporterInfo.showGlowingTeleporter(player);
            }
        }
    }

    public TeleporterInfo getTeleporterInfo(Location location) {
        return getTeleporters().stream().filter(teleporterInfo -> teleporterInfo.is(location)).findFirst().orElse(null);
    }

    public boolean isTeleporterBlock(Location location) {
        return getTeleporters().stream().anyMatch(teleporterInfo -> teleporterInfo.is(location));
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", "&b&lTELEPORTER");

        if (!config.has("seconde_to_tp"))
            config.addProperty("seconde_to_tp", seconde_to_tp);

        if (!config.has("sound_start_tp_comment"))
            config.addProperty("sound_start_tp_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound_start_tp"))
            config.addProperty("sound_start_tp", sound_start_tp.name());

        if (!config.has("volume_start_tp_comment"))
            config.addProperty("volume_start_tp_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume_start_tp"))
            config.addProperty("volume_start_tp", volume_start_tp);

        if (!config.has("pitch_start_tp_comment"))
            config.addProperty("pitch_start_tp_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch_start_tp"))
            config.addProperty("pitch_start_tp", pitch_start_tp);

        if (!config.has("sound_leave_tp_comment"))
            config.addProperty("sound_leave_tp_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound_leave_tp"))
            config.addProperty("sound_leave_tp", sound_leave_tp.name());

        if (!config.has("volume_leave_tp_comment"))
            config.addProperty("volume_leave_tp_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume_leave_tp"))
            config.addProperty("volume_leave_tp", volume_leave_tp);

        if (!config.has("pitch_leave_tp_comment"))
            config.addProperty("pitch_leave_tp_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch_leave_tp"))
            config.addProperty("pitch_leave_tp", pitch_leave_tp);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rFonctionne par paire, Lorsque vous en posez un premier",
                    "&rle prochain que vous posez sera",
                    "&rautomatiquement connecté.",
                    "&rPour l'utiliser marchez sur bloc et",
                    "&rvous serez téléporté sur le second.",
                    "&rfonctionne dans les 2 sens.",
                    "&rSi l'un des deux est cassé",
                    "&rl'autre se casse aussi")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);


        JsonArray teleporterArray = new JsonArray();

        getTeleporters().forEach(teleporterInfo ->
        {
            JsonObject object = new JsonObject();
            object.addProperty("owner", teleporterInfo.getOwner().getName());
            object.addProperty("loc1", TunnelsOfSteel.toStringLocation(teleporterInfo.getLocationOne()));
            object.addProperty("loc2", TunnelsOfSteel.toStringLocation(teleporterInfo.getLocationTwo()));
            teleporterInfo.removeAllGlowingTeleporter();
            teleporterArray.add(object);
        });

        playerToMove.forEach((uuid, teleporterTask) -> teleporterTask.cancel());
        playerToMove.clear();
        teleporterInfosMap.clear();
        lore.clear();
        firstLocation.clear();

        jsonObject.add("teleporters", teleporterArray);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("seconde_to_tp"))
            seconde_to_tp = config.get("seconde_to_tp").getAsDouble();

        //Load sound for start tp
        try {
            if (config.has("sound_start_tp"))
                sound_start_tp = Sound.valueOf(config.get("sound_start_tp").getAsString());
        } catch (Exception e) {
            sound_start_tp = Sound.BLOCK_FENCE_GATE_OPEN;
            Logger.log("The sound is null, please check your config file !");
        } finally {
            if (sound_start_tp == null) {
                sound_start_tp = Sound.BLOCK_FENCE_GATE_OPEN;
                Logger.log("The sound is null, please check your config file !");
            }
        }

        //load sound for stop tp
        try {
            if (config.has("sound_leave_tp"))
                sound_leave_tp = Sound.valueOf(config.get("sound_leave_tp").getAsString());
        } catch (Exception e) {
            sound_leave_tp = Sound.BLOCK_FIRE_EXTINGUISH;
            Logger.log("The input sound is null, please check your config file !");
        } finally {
            if (sound_leave_tp == null) {
                sound_leave_tp = Sound.BLOCK_FIRE_EXTINGUISH;
                Logger.log("The input sound is null, please check your config file !");
            }
        }

        if (config.has("volume_start_tp"))
            volume_start_tp = config.get("volume_start_tp").getAsFloat();

        if (config.has("pitch_start_tp"))
            pitch_start_tp = config.get("pitch_start_tp").getAsFloat();

        if (config.has("volume_leave_tp"))
            volume_leave_tp = config.get("volume_leave_tp").getAsFloat();

        if (config.has("pitch_leave_tp"))
            pitch_leave_tp = config.get("pitch_leave_tp").getAsFloat();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(ChatColor.translateAlternateColorCodes('&', jsonElement.getAsString())));

        //load all teleporter
        if (mainObject.has("teleporters")) {
            JsonArray jsonArray = mainObject.getAsJsonArray("teleporters");
            jsonArray.forEach(jsonElement ->
            {
                if (jsonElement.isJsonObject()) {
                    JsonObject json = jsonElement.getAsJsonObject();
                    addTeleporters(new TeleporterInfo(TunnelsOfSteel.toLocationString(json.get("loc1").getAsString()),
                            TunnelsOfSteel.toLocationString(json.get("loc2").getAsString()),
                            TeamManager.getTeam(Color.match(json.get("owner").getAsString()))));
                }
            });
        }
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack stack = new ItemStack(Material.WHITE_STAINED_GLASS);
        ItemMeta meta = stack.getItemMeta();

        if (meta != null) {
            meta.setDisplayName(Teleporter.item_name);
            meta.setLore(lore);
            stack.setItemMeta(meta);
        }
        return Collections.singletonList(stack);
    }

    private static class TeleporterInfo {
        private Location locationOne;
        private Location locationTwo;

        private List<Player> playerViewer = new ArrayList<>();

        private EntityShulker shulker_loc1;
        private EntityShulker shulker_loc2;

        private TOSTeam owner;

        TeleporterInfo(Location locationOne, Location locationTwo, TOSTeam owner) {
            if (locationOne.getWorld() == null)
                return;

            if (locationTwo.getWorld() == null)
                return;

            setLocationOne(locationOne);
            setLocationTwo(locationTwo);
            this.owner = owner;

            this.shulker_loc1 = new EntityShulker(EntityTypes.SHULKER, ((CraftWorld) locationOne.getWorld()).getHandle());
            shulker_loc1.setPosition(locationOne.getBlockX() + 0.5, locationOne.getBlockY(), locationOne.getBlockZ() + 0.5);
            shulker_loc1.setInvisible(true);
            shulker_loc1.setNoGravity(true);
            shulker_loc1.setFlag(6, true);
            shulker_loc1.glowing = true;
            shulker_loc1.setNoAI(true);

            this.shulker_loc2 = new EntityShulker(EntityTypes.SHULKER, ((CraftWorld) locationTwo.getWorld()).getHandle());
            shulker_loc2.setPosition(locationTwo.getBlockX() + 0.5, locationTwo.getBlockY(), locationTwo.getBlockZ() + 0.5);
            shulker_loc2.setInvisible(true);
            shulker_loc2.setNoGravity(true);
            shulker_loc2.setFlag(6, true);
            shulker_loc2.glowing = true;
            shulker_loc2.setNoAI(true);

            Team teleporterTeam = getOwner().getScoreboard().getTeam("Teleporter");

            if (teleporterTeam == null) {
                teleporterTeam = getOwner().getScoreboard().registerNewTeam("Teleporter");
                teleporterTeam.setColor(ChatColor.GRAY);
            }

            teleporterTeam.addEntry(shulker_loc1.getUniqueID().toString());
            teleporterTeam.addEntry(shulker_loc2.getUniqueID().toString());

            actualiseViewer();
        }

        public Location getLocationTwo()
        {
            return locationTwo;
        }

        public void setLocationTwo(Location locationTwo)
        {
            this.locationTwo = locationTwo;
        }

        public Location getLocationOne()
        {
            return locationOne;
        }

        public void setLocationOne(Location locationOne)
        {
            this.locationOne = locationOne;
        }

        public boolean is(Location location) {
            return isLocOne(location) || isLocTwo(location);
        }

        public boolean isLocTwo(Location location) {
            return getLocationTwo().getBlockX() == location.getBlockX() &&
                    getLocationTwo().getBlockY() == location.getBlockY() &&
                    getLocationTwo().getBlockZ() == location.getBlockZ() &&
                    getLocationTwo().getWorld().getName().equals(location.getWorld().getName());
        }

        public boolean isLocOne(Location location) {
            return getLocationOne().getBlockX() == location.getBlockX() &&
                    getLocationOne().getBlockY() == location.getBlockY() &&
                    getLocationOne().getBlockZ() == location.getBlockZ() &&
                    getLocationOne().getWorld().getName().equals(location.getWorld().getName());
        }

        public TOSTeam getOwner() {
            return owner;
        }

        public List<Player> getPlayerViewer() {
            return playerViewer;
        }

        private void removeGlowingTeleporter(OfflinePlayer offlinePlayer) {
            if (offlinePlayer.isOnline()) {
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), new PacketPlayOutEntityDestroy(shulker_loc1.getId()));
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), new PacketPlayOutEntityDestroy(shulker_loc2.getId()));
                Player player = offlinePlayer.getPlayer();
                if (player != null && !getPlayerViewer().contains(player)) {
                    getPlayerViewer().remove(player);
                }
            }
        }

        private void showGlowingTeleporter(OfflinePlayer offlinePlayer) {
            if (offlinePlayer.isOnline()) {
                PacketPlayOutSpawnEntityLiving spawnPacket = new PacketPlayOutSpawnEntityLiving(shulker_loc1);
                PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata(shulker_loc1.getId(), shulker_loc1.getDataWatcher(), true);

                TOSUtils.sendPacket(offlinePlayer.getPlayer(), spawnPacket);
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), metaPacket);

                spawnPacket = new PacketPlayOutSpawnEntityLiving(shulker_loc2);
                metaPacket = new PacketPlayOutEntityMetadata(shulker_loc2.getId(), shulker_loc2.getDataWatcher(), true);

                TOSUtils.sendPacket(offlinePlayer.getPlayer(), spawnPacket);
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), metaPacket);

                Player player = offlinePlayer.getPlayer();
                if (player != null && !getPlayerViewer().contains(player)) {
                    getPlayerViewer().add(player);
                }
            }
        }

        public void removeAllGlowingTeleporter() {
            for (Player player : getPlayerViewer()) {
                if (player.isOnline()) {
                    removeGlowingTeleporter(player);
                }
            }

            getPlayerViewer().clear();
        }

        public void showAllGlowingTeleporter() {
            for (OfflinePlayer offlinePlayer : getOwner().getPlayers()) {
                if (offlinePlayer.isOnline()) {
                    showGlowingTeleporter(offlinePlayer);
                }
            }
        }

        public void actualiseViewer() {
            List<Player> toRemoveViewer = new ArrayList<>();
            for (Player player : getPlayerViewer()) {
                if (!player.isOnline()) {
                    toRemoveViewer.add(player);
                    continue;
                }

                TOSTeam playerTeam = TOSPlayer.getPlayer(player).getTeam();
                if (player.isOnline() && playerTeam != null && !playerTeam.getName().equalsIgnoreCase(getOwner().getName())) {
                    toRemoveViewer.add(player);
                    removeGlowingTeleporter(player);
                }
            }

            getPlayerViewer().removeAll(toRemoveViewer);

            for (OfflinePlayer offlinePlayer : getOwner().getPlayers()) {
                if (offlinePlayer.isOnline()) {
                    Player player = offlinePlayer.getPlayer();
                    if (player != null) {
                        if (!getPlayerViewer().contains(player)) {
                            getPlayerViewer().add(player);
                            showGlowingTeleporter(player);
                        }
                    }
                }
            }
        }
    }

    private static class TeleporterTask implements Runnable {

        private final String TITLE = ChatColor.AQUA + "" + ChatColor.BOLD + "Teleportation in: %value%s";
        private Player player;
        private TeleporterInfo teleporterInfo;
        private Location teleportBase;
        private int taskID;
        private double tick;
        private double maxTick;
        private BossBar bossBar;
        private NumberFormat format = new DecimalFormat("0.0");

        TeleporterTask(Player player, TeleporterInfo teleporterInfo, Location base, double seconde) {
            setTeleporterInfo(teleporterInfo);
            setPlayer(player);
            setTeleportBase(base);

            this.bossBar = Bukkit.createBossBar(TITLE.replace("%value%", format.format(seconde)), BarColor.BLUE, BarStyle.SOLID);
            this.bossBar.setProgress(0);
            this.bossBar.addPlayer(player);

            this.maxTick = seconde * 20;

            this.taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
        }

        @Override
        public void run()
        {
            tick++;

            if (tick >= maxTick)
            {
                if (playerToMove.containsKey(player.getUniqueId()))
                {
                    Location toTeleport = teleporterInfo.isLocOne(getTeleportBase()) ? teleporterInfo.getLocationTwo() : teleporterInfo.getLocationOne();

                    toTeleport.setPitch(player.getEyeLocation().getPitch());
                    toTeleport.setYaw(player.getEyeLocation().getYaw());

                    player.teleport(toTeleport.clone().add(0.5D, 1.3D, 0.5D));
                    player.playSound(player.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 0.1F, 2);
                    TOSPlayer.getPlayer(player).sendActionBar(ChatColor.AQUA + "" + ChatColor.BOLD + "Teleported !", 1.5);

                    teleportedPlayer.add(player.getUniqueId());
                    playerToMove.remove(player.getUniqueId());
                }

                cancel();
            } else
            {
                bossBar.setProgress((tick / maxTick));
                bossBar.setTitle(TITLE.replace("%value%", format.format((maxTick - tick) / 20)));
            }
        }

        public void cancel()
        {
            bossBar.removeAll();
            Bukkit.getScheduler().cancelTask(taskID);
        }

        public Location getTeleportBase()
        {
            return teleportBase;
        }

        public void setTeleportBase(Location teleportBase)
        {
            this.teleportBase = teleportBase;
        }

        public TeleporterInfo getTeleporterInfo() {
            return teleporterInfo;
        }

        public void setTeleporterInfo(TeleporterInfo teleporterInfo) {
            this.teleporterInfo = teleporterInfo;
        }

        public Player getPlayer()
        {
            return player;
        }

        public void setPlayer(Player player)
        {
            this.player = player;
        }
    }
}
