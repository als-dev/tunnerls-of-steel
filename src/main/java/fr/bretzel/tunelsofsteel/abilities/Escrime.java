package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Escrime extends Ability {
    public static String item_name = "&aEscrime";
    private static List<UUID> PLAYER_ECRIME = new ArrayList<>();
    private static Map<Player, EscrimeTask> escrimeTaskMap = new HashMap<>();
    private double loading_time = 1.5D;

    public Escrime() {
        super("Escrime");
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player) || !(event.getEntity() instanceof Player))
            return;

        Player playerDamager = (Player) event.getDamager();
        ItemStack mainHand = playerDamager.getInventory().getItemInMainHand();

        if (mainHand.getItemMeta() != null) {
            mainHand.getItemMeta().getDisplayName();
            if (!mainHand.getItemMeta().getDisplayName().isEmpty() && (!mainHand.getItemMeta().getDisplayName().equals(item_name))) {
                return;
            }
        }

        Player playerDamage = (Player) event.getEntity();
        TOSPlayer tosPlayerDamage = TOSPlayer.getPlayer(playerDamage);
        ItemStack damagerInHand = playerDamager.getInventory().getItemInMainHand();

        if (PLAYER_ECRIME.contains(playerDamager.getUniqueId()) && !tosPlayerDamage.isImmunized() && damagerInHand.getItemMeta() != null
                && damagerInHand.getItemMeta().getDisplayName().equals(item_name)) {
            if (escrimeTaskMap.containsKey(playerDamager)) {
                escrimeTaskMap.get(playerDamager).tick = 0;
            }

            ItemStack stackInHand = playerDamage.getInventory().getItemInMainHand();

            if (TOSUtils.hasPlaceInInventory(playerDamage)) {
                List<Integer> freeSlots = new ArrayList<>();

                for (int i = 9; i <= 35; i++) {
                    if (playerDamage.getInventory().getItem(i) == null) {
                        freeSlots.add(i);
                    }
                }

                playerDamage.getInventory().setItem(freeSlots.get(new Random().nextInt(freeSlots.size())), stackInHand);
                freeSlots.clear();
                playerDamage.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
            } else {
                playerDamage.getWorld().dropItemNaturally(playerDamage.getLocation().clone().add(0.5D, 0.2, 0.5D), stackInHand);
                stackInHand.setAmount(0);
            }

        }
    }

    @EventHandler
    public void onPlayerSwitchSlot(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        ItemStack stack = player.getInventory().getItem(event.getNewSlot());

        if (stack != null && stack.getType() == Material.GOLDEN_SWORD) {
            ItemMeta meta = stack.getItemMeta();

            if (meta != null && !meta.getDisplayName().isEmpty() && meta.getDisplayName().equals(item_name)) {
                if (escrimeTaskMap.containsKey(player)) {
                    escrimeTaskMap.get(player).cancel();
                    escrimeTaskMap.remove(player);
                }

                EscrimeTask task = new EscrimeTask(player, loading_time, event.getNewSlot());
                escrimeTaskMap.put(player, task);
            }
        } else if (escrimeTaskMap.containsKey(player)) {
            escrimeTaskMap.get(player).cancel();
            escrimeTaskMap.remove(player);
        } else PLAYER_ECRIME.remove(player.getUniqueId());
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (!config.has("loading_time"))
            config.addProperty("loading_time", loading_time);

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        for (EscrimeTask task : escrimeTaskMap.values())
            task.cancel();

        escrimeTaskMap.clear();
        PLAYER_ECRIME.clear();

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("loading_time"))
            loading_time = config.get("loading_time").getAsDouble();
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.GOLDEN_SWORD, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();

            meta.setDisplayName(item_name);

            meta.addItemFlags(ItemFlag.values());

            if (meta instanceof Damageable)
                ((Damageable) meta).setDamage(28);

            meta.setLore(Arrays.asList(
                    ChatColor.RESET + "Cette épée ne développe sont potentiel",
                    ChatColor.RESET + "que lorsque sont attaque est chargée (" + loading_time + " sec)",
                    ChatColor.RESET + "à ce moment la, elle désarmera l'adversaire",
                    ChatColor.RESET + "en envoyant l'item qu'il tient actuellement en main",
                    ChatColor.RESET + "dans sont inventaire.",
                    ChatColor.RESET + "  ",
                    ChatColor.GRAY + "When in main hand:",
                    ChatColor.RED + " -3.4 Attack Speed")
            );

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }

    private static class EscrimeTask implements Runnable {
        private final String TITLE = ChatColor.GREEN + "" + ChatColor.BOLD + "Escrime Load: %value%%";
        public Player player;
        private ChatColor READY = ChatColor.GREEN;

        private int taskID;

        private double tick;
        private double maxTick = 30D;

        private BossBar bossBar;
        private NumberFormat format = new DecimalFormat("###");

        private int clignote = 0;

        private int slot = -1;

        public EscrimeTask(Player player, double loadingTime, int slot) {
            taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
            this.player = player;
            this.slot = slot;

            PLAYER_ECRIME.add(player.getUniqueId());

            maxTick = loadingTime * 20;

            this.bossBar = Bukkit.createBossBar(TITLE.replace("%value%", format.format((tick / maxTick) * 100)), BarColor.GREEN, BarStyle.SOLID);
            this.bossBar.setProgress(0);

            this.bossBar.addPlayer(player);
        }

        @Override
        public void run() {
            tick++;

            if (!PLAYER_ECRIME.contains(player.getUniqueId()))
                cancel();

            ItemStack stack = player.getInventory().getItem(slot);
            if (stack == null || stack.getType() != Material.GOLDEN_SWORD) {
                cancel();
            } else {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && !meta.getDisplayName().isEmpty() && !meta.getDisplayName().equals(item_name)) {
                    cancel();
                }
            }

            if (tick >= maxTick) {
                clignote++;

                bossBar.setProgress(1);

                if (clignote >= 13) {
                    clignote = 0;
                    READY = READY == ChatColor.GREEN ? ChatColor.DARK_GREEN : ChatColor.GREEN;
                }

                bossBar.setTitle(READY + "" + ChatColor.BOLD + "Escrime READY !");
            } else {
                bossBar.setProgress((tick / maxTick));

                bossBar.setTitle(TITLE.replace("%value%", format.format((tick / maxTick) * 100)));
            }
        }

        public void cancel() {
            bossBar.removeAll();
            Bukkit.getScheduler().cancelTask(taskID);
        }
    }
}
