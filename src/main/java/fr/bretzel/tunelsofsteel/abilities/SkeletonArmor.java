package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.event.ArmorEquipEvent;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.Disguise;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.FlagWatcher;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SkeletonArmor extends Ability {
    private static List<Material> materials = Arrays.asList(Material.CHAINMAIL_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS);
    private HashMap<Player, Long> lastSend = new HashMap<>();

    public SkeletonArmor() {
        super("SkeletonArmor");
    }

    private static int checkPercentage(Player player) {
        int percent = 0;
        PlayerInventory playerInventory = player.getInventory();

        for (ItemStack armorContent : playerInventory.getArmorContents())
            if (armorContent != null && !armorContent.getType().isAir() && isSkeletonArmor(armorContent))
                percent += 25;

        return percent;
    }

    private static boolean isSkeletonArmor(ItemStack itemStack) {
        boolean skeletonArmor = false;

        for (Material material : materials)
            if (itemStack.getType() == material) {
                ItemMeta meta = itemStack.getItemMeta();
                skeletonArmor = (meta != null && !meta.getDisplayName().isEmpty() && ChatColor.stripColor(meta.getDisplayName()).startsWith("Skeleton"));
            }

        return skeletonArmor;
    }

    @EventHandler
    public void onEntityTarget(EntityTargetEvent event) {
        if (event.getEntity() instanceof Monster && event.getTarget() instanceof Player) {
            TOSPlayer tosPlayer = TOSPlayer.getPlayer((Player) event.getTarget());
            if (!tosPlayer.cantTarget())
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        int percent = checkPercentage(player);
        if (percent >= 100) {
            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () -> disguisePlayer(player), 20L);
            TOSPlayer.getPlayer(player).setTarget(false);
        } else if (percent >= 50) {
            TOSPlayer.getPlayer(player).setTarget(false);
        } else {
            TOSPlayer.getPlayer(player).setTarget(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (DisguiseAPI.isDisguised(player)) {
            Disguise disguise = DisguiseAPI.getDisguise(player);
            if (disguise instanceof MobDisguise && disguise.getType().getEntityType() == EntityType.SKELETON) {
                player.playSound(player.getLocation(), Sound.ENTITY_BAT_DEATH, SoundCategory.AMBIENT, 0.2F, 0.1F);

                boolean sendActionBar = false;

                if (!lastSend.containsKey(player)) {
                    sendActionBar = true;
                    lastSend.put(player, System.nanoTime());
                } else if (TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - lastSend.get(player)) >= 1.5) {
                    lastSend.replace(player, System.nanoTime());
                    sendActionBar = true;
                }

                if (sendActionBar)
                    TOSPlayer.getPlayer(player).sendActionBar(ChatColor.RED + "Sorry you can not interact because you are a " + ChatColor.WHITE + "" + ChatColor.BOLD + "Skeleton !", 1.6);

                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerEquipArmor(ArmorEquipEvent event) {
        if (DisguiseAPI.isDisguised(event.getPlayer())) {
            Disguise disguise = DisguiseAPI.getDisguise(event.getPlayer());
            if (disguise instanceof MobDisguise && disguise.getType().getEntityType() == EntityType.SKELETON && checkPercentage(event.getPlayer()) < 100)
                DisguiseAPI.undisguiseToAll(event.getPlayer());
        }

        if ((event.getOldArmorPiece() != null && isSkeletonArmor(event.getOldArmorPiece())) || (event.getNewArmorPiece() != null && isSkeletonArmor(event.getNewArmorPiece()))) {
            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
            {
                Player player = event.getPlayer();
                int percent = checkPercentage(player);
                TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

                if (percent < 50) {
                    unDisguisePlayer(player);
                    //Re set the target
                    tosPlayer.setTarget(true);
                }

                if (percent == 100) {
                    //Create a disguise !
                    disguisePlayer(player);
                    tosPlayer.setTarget(false);

                } else if (percent < 100 && percent >= 50) {
                    unDisguisePlayer(player);
                    tosPlayer.setTarget(false);
                } else {
                    unDisguisePlayer(player);
                }
            }, 2);
        }
    }

    public void unDisguisePlayer(Player player) {
        if (player.getGameMode() != GameMode.CREATIVE)
            player.setGameMode(GameMode.SURVIVAL);

        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

        if (DisguiseAPI.isDisguised(player)) {
            tosPlayer.sendActionBar(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You are no longer a skeleton", 3.5D);
            DisguiseAPI.undisguiseToAll(player);
        }
        //Set target
        TOSPlayer.getPlayer(player).setTarget(true);
    }

    public void disguisePlayer(Player player) {
        if (player.getGameMode() != GameMode.CREATIVE)
            player.setGameMode(GameMode.ADVENTURE);

        for (Entity entity : TunnelsOfSteel.getTOSWorld().getEntities()) {
            if (entity instanceof Monster) {
                Monster monster = (Monster) entity;

                if (monster.getTarget() != null && monster.getTarget().equals(player)) {
                    monster.setTarget(null);
                }
            }
        }

        MobDisguise mobDisguise = new MobDisguise(DisguiseType.SKELETON);
        mobDisguise.setShowName(false).setHearSelfDisguise(false).setReplaceSounds(true);
        FlagWatcher watcher = mobDisguise.getWatcher();
        watcher.setArmor(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
        mobDisguise.setWatcher(watcher);
        DisguiseAPI.disguiseIgnorePlayers(player, mobDisguise, player);
        TOSPlayer.getPlayer(player).sendActionBar(ChatColor.GREEN + "" + ChatColor.BOLD + "You are a skeleton !", Integer.MAX_VALUE, tosPlayer -> !DisguiseAPI.isDisguised(tosPlayer.getPlayer()));
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        for (Player player : Bukkit.getOnlinePlayers())
            if (DisguiseAPI.isDisguised(player))
                DisguiseAPI.undisguiseToAll(player);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            int percent = checkPercentage(player);
            if (percent >= 100) {
                Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () -> disguisePlayer(player), 20L);
                TOSPlayer.getPlayer(player).setTarget(false);
            } else if (percent >= 50) {
                TOSPlayer.getPlayer(player).setTarget(false);
            } else {
                TOSPlayer.getPlayer(player).setTarget(true);
            }
        }
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ArrayList<ItemStack> stacks = new ArrayList<>();

        int index = 0;

        for (Material material : materials) {
            ItemStack stack = new ItemStack(material, 1);
            ItemMeta meta = stack.getItemMeta();

            if (meta != null) {
                meta.setCustomModelData(1000 + index);
                index++;

                switch (material.name()) {
                    case "CHAINMAIL_HELMET":
                        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Skeleton Helmet");
                        break;
                    case "CHAINMAIL_CHESTPLATE":
                        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Skeleton Chestplate");
                        break;
                    case "CHAINMAIL_LEGGINGS":
                        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Skeleton Leggings");
                        break;
                    case "CHAINMAIL_BOOTS":
                        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Skeleton Boots");
                        break;
                }

                stack.setItemMeta(meta);
            }

            stacks.add(stack);
        }
        return stacks;
    }
}
