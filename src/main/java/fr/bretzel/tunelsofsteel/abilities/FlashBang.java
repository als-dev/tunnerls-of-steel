package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.raytrace.RayTrace;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class FlashBang extends Ability {

    private static ArrayList<FlashBangTask> flashBangTasks = new ArrayList<>();
    public List<String> lore = new ArrayList<>();
    private String ITEM_NAME = "&b&lFLASH BANG";
    private Sound sound = Sound.BLOCK_PISTON_EXTEND;
    private float volume = 0.2F;
    private float pitch = 2F;
    private double duration_time = 7;
    private double range = 3;
    private ArrayList<Material> transparentBlock = new ArrayList<>();

    public FlashBang() {
        super("FlashBang");

        for (Material material : Material.values()) {
            if (material.name().toLowerCase().contains("glass"))
                transparentBlock.add(material);

            if (material.name().toLowerCase().contains("fence"))
                transparentBlock.add(material);
        }

        transparentBlock.add(Material.VINE);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        Player player = event.getPlayer();

        ItemStack itemStackInMainHand = player.getInventory().getItemInMainHand();

        if (itemStackInMainHand.getType() == Material.SNOWBALL && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            ItemMeta meta = itemStackInMainHand.getItemMeta();
            if (meta == null || itemStackInMainHand.getItemMeta().getDisplayName().isEmpty())
                return;

            if (itemStackInMainHand.getItemMeta().getDisplayName().equals(ITEM_NAME)) {
                Location eyeLocation = player.getEyeLocation().clone();
                Vector direction = eyeLocation.getDirection();
                player.getWorld().playSound(player.getLocation(), sound, volume, pitch);

                Snowball snowball = player.launchProjectile(Snowball.class, direction.multiply(0.9F));
                snowball.setShooter(player);
                snowball.setCustomName(ITEM_NAME);
                snowball.setCustomNameVisible(false);

                if (player.getGameMode() != GameMode.CREATIVE)
                    itemStackInMainHand.setAmount(itemStackInMainHand.getAmount() - 1);

                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityDeath(ProjectileHitEvent event)
    {
        if (event.getEntity() instanceof Snowball)
        {
            Snowball snowball = (Snowball) event.getEntity();
            if (snowball.getCustomName() != null && !snowball.getCustomName().isEmpty() && snowball.getCustomName().equals(ITEM_NAME)) {
                snowball.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, snowball.getLocation().getX(),
                        snowball.getLocation().getY(), snowball.getLocation().getZ(), (int) (33 * range), range / 2.5D, range / 2, range / 2, 0, null, true);

                snowball.getWorld().playSound(snowball.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_LARGE_BLAST_FAR, 2F, 1.5F);

                Collection<Entity> entities = snowball.getWorld().getNearbyEntities(snowball.getLocation(), 3.5D, 3.5D, 3.5D, entity -> entity instanceof Player);

                for (Entity entity : entities) {
                    Player p = (Player) entity;
                    TOSPlayer player = TOSPlayer.getPlayer(p);

                    Location origin = snowball.getLocation(); //event.getHitBlock().getRelative(event.getHitBlockFace()).getLocation();
                    origin.setZ(origin.getBlockZ() + 0.5);
                    origin.setX(origin.getBlockX() + 0.5);
                    origin.setY(origin.getBlockY() + 0.5);

                    RayTraceResult result = RayTrace.rayTraceBlock(origin, p.getEyeLocation(), range + 1, 0.25, block -> block.getType().isSolid() && !transparentBlock.contains(block.getType()));

                    RayTraceResult result2 = RayTrace.rayTraceBlock(origin, p.getLocation(), range + 1, 0.25, block -> block.getType().isSolid() && !transparentBlock.contains(block.getType()));
                    boolean playerHit = false;

                    if (result.getHitBlock() == null || result2.getHitBlock() == null) {
                        playerHit = true;
                    }

                    if (playerHit && !player.isImmunized()) {

                        FlashBangTask task;
                        int taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, task = new FlashBangTask(duration_time, p), 0, 1).getTaskId();
                        task.setTaskID(taskID);
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, ((int) duration_time * 20), 2, true));

                        if (snowball.getShooter() instanceof Player) {
                            Player shooter = ((Player) snowball.getShooter());
                            shooter.playSound(p.getEyeLocation(), Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, SoundCategory.AMBIENT, 2F, 1.7F);
                        }
                    }
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", ITEM_NAME);

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("duration_time"))
            config.addProperty("duration_time", duration_time);

        if (!config.has("range"))
            config.addProperty("range", range);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rProjectile explosant à l'impact",
                    "&rAveugle tout les joueurs dans un ",
                    "&rrayon de 4 blocs pendant 5s")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        lore.clear();

        for (FlashBangTask flashBangTask : flashBangTasks) {
            flashBangTask.cancel();
        }

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            ITEM_NAME = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        try {
            if (config.has("sound"))
                sound = Sound.valueOf(config.get("sound").getAsString());
        } catch (Exception e) {
            sound = Sound.BLOCK_PISTON_EXTEND;
            Logger.log("The sound is null, please check your config file !");
        } finally {
            if (sound == null) {
                sound = Sound.BLOCK_PISTON_EXTEND;
                Logger.log("The sound is null, please check your config file !");
            }
        }

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            volume = config.get("pitch").getAsFloat();

        if (config.has("duration_time"))
            duration_time = config.get("duration_time").getAsDouble();

        if (config.has("range"))
            range = config.get("range").getAsInt();

        if (config.has("lore"))
            config.get("lore").getAsJsonArray().forEach(jsonElement -> lore.add(addLore(ChatColor.translateAlternateColorCodes('&', jsonElement.getAsString()))));
    }

    public String addLore(String str) {
        return str.replaceAll("%range%", String.valueOf((int) range)).replaceAll("duration_time", String.valueOf(duration_time));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack stack = new ItemStack(Material.SNOWBALL);
        stack.setAmount(1);
        ItemMeta meta = stack.getItemMeta();
        if (meta != null) {
            meta.setLore(lore);
            meta.setDisplayName(ITEM_NAME);
            stack.setItemMeta(meta);
        }

        return Collections.singletonList(stack);
    }

    private static class FlashBangTask implements Runnable {
        private Player player;
        private double maxTick;
        private double tick = 0;
        private int taskID = 0;
        private BossBar bossBar;
        private NumberFormat format = new DecimalFormat("0.0");
        private String bossbar_display = ChatColor.WHITE + "" + ChatColor.BOLD + "Flashed during: %value%s";

        private FlashBangTask(double sec, Player player) {
            this.maxTick = sec * 20;
            this.player = player;
            this.bossBar = Bukkit.createBossBar(bossbar_display.replace("%value%", format.format(sec)), BarColor.WHITE, BarStyle.SOLID);
            this.bossBar.setProgress(0);
            this.bossBar.addPlayer(player);
            flashBangTasks.add(this);
        }

        @Override
        public void run()
        {
            if (tick >= maxTick || TOSPlayer.getPlayer(player).isImmunized()) {
                cancel();
            } else if (player.isOnline()) {
                if (player.isSprinting())
                    player.setSprinting(false);

                Location location = player.getEyeLocation().clone().add(0, 0.1D, 0);

                player.spawnParticle(Particle.END_ROD, location.add(location.getDirection().normalize()), 150, 0.7F, 0.40F, 0.7F, 0, null);

                World world = location.getWorld();
                if (world != null)
                    world.spawnParticle(Particle.FIREWORKS_SPARK, player.getEyeLocation().add(0D, 0.5D, 0D), 1, 0.5D, 0D, 0.5D, 0);
                bossBar.setProgress((tick / maxTick));

                bossBar.setTitle(bossbar_display.replace("%value%", format.format((maxTick - tick) / 20)));
            }

            tick++;
        }

        void setTaskID(int taskID) {
            this.taskID = taskID;
        }

        public void cancel() {
            bossBar.removeAll();
            Bukkit.getScheduler().cancelTask(taskID);
        }
    }
}
