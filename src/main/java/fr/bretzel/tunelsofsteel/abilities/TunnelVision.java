package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class TunnelVision extends Ability {

    private String item_name = "&b&lTunnel Vision";

    private Sound sound = Sound.ENTITY_ELDER_GUARDIAN_HURT;
    private float volume = 0.3F;
    private float pitch = 1.5F;

    private double duration = 30D;
    private double range = 10D;

    private List<String> lore = new ArrayList<>();

    private List<TunnelsVisionTask> tasks = new ArrayList<>();

    private HashMap<Player, Long> lastSendActionBar = new HashMap<>();

    public TunnelVision() {
        super("TunnelVision");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        ItemStack stack = event.getItem();
        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && stack != null && stack.getItemMeta() != null) {
            ItemMeta meta = stack.getItemMeta();
            Player player = event.getPlayer();

            if (meta.getDisplayName().equals(item_name) && event.getItem() != null) {
                event.setCancelled(true);

                if (player.getGameMode() != GameMode.CREATIVE)
                    event.getItem().setAmount(stack.getAmount() - 1);

                player.playSound(player.getLocation(), sound, volume, pitch);

                new TunnelsVisionTask(player, duration);
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("range"))
            config.addProperty("range", range);

        if (!config.has("duration"))
            config.addProperty("duration", duration);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rPendant %duration% secondes,",
                    "&rtout les adversaires se situant dans",
                    "&run rayon de %range% blocs autour de vous",
                    "&rsont affectés par l'effet Blindess",
                    "&r&7&o(Grosse réduction de leur champ de vision)")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        lore.clear();

        for (TunnelsVisionTask tunnelsVisionTask : tasks) {
            tunnelsVisionTask.cancel();
        }

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("sound"))
            sound = Sound.valueOf(config.get("sound").getAsString());

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        if (config.has("range"))
            range = config.get("range").getAsDouble();

        if (config.has("duration"))
            duration = config.get("duration").getAsDouble();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));

    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%range%", String.valueOf((int) range)).replaceAll("%duration%", String.valueOf((int) duration)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.ENDER_EYE, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();

            meta.setDisplayName(item_name);

            meta.addItemFlags(ItemFlag.values());

            meta.setLore(lore);

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }

    protected class TunnelsVisionTask implements Runnable {
        private final String TITLE = ChatColor.YELLOW + "" + ChatColor.BOLD + "Tunnel vision active for " + ChatColor.AQUA + "%sec%s" +
                ChatColor.YELLOW + ChatColor.BOLD + " | Player around you: " + ChatColor.AQUA + "%players%";

        private int taskID;

        private double maxTick;
        private double tick;

        private BossBar bossBar;
        private NumberFormat format = new DecimalFormat("00");

        private Player player;

        TunnelsVisionTask(Player player, double maxSec) {
            this.maxTick = maxSec * 20;

            this.player = player;

            taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();

            this.bossBar = Bukkit.createBossBar(TITLE.replaceAll("%sec%", format.format((maxTick - tick) / 20)).replaceAll("%players%", String.valueOf(0)), BarColor.YELLOW, BarStyle.SEGMENTED_20);
            this.bossBar.setProgress(0);

            this.bossBar.addPlayer(player);

            tasks.add(this);
        }

        @Override
        public void run() {
            tick++;

            if (tick >= maxTick) {
                cancel();
            } else {
                bossBar.setProgress(tick / maxTick);

                int playerInZone = 0;

                for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), range, range, range)) {
                    if (entity instanceof Player) {
                        Player p = (Player) entity;
                        TOSPlayer tosP = TOSPlayer.getPlayer(p);
                        TOSTeam pT = TOSPlayer.getPlayer(p).getTeam();
                        TOSTeam playerTeam = TOSPlayer.getPlayer(player).getTeam();

                        if (pT != null && playerTeam != null && !pT.getName().equals(playerTeam.getName()) && !tosP.isImmunized()) {
                            playerInZone++;
                            p.removePotionEffect(PotionEffectType.BLINDNESS);
                            p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 27, 0, true));
                            boolean sendActionBar = false;

                            if (!lastSendActionBar.containsKey(player)) {
                                sendActionBar = true;
                                lastSendActionBar.put(player, System.nanoTime());
                            } else if (TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - lastSendActionBar.get(player)) >= 1.5) {
                                lastSendActionBar.replace(player, System.nanoTime());
                                sendActionBar = true;
                            }

                            if (sendActionBar)
                                tosP.sendActionBar(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You've been tunnel visioned, get away to clear the effect".toUpperCase(), 1.4);
                        }
                    }
                }

                bossBar.setTitle(TITLE.replaceAll("%sec%", format.format((maxTick - tick) / 20)).replaceAll("%players%", String.valueOf(playerInZone)));
            }
        }

        public void cancel() {
            bossBar.removeAll();
            Bukkit.getScheduler().cancelTask(taskID);
        }
    }
}
