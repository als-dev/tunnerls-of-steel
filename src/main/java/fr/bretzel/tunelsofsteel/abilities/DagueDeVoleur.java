package fr.bretzel.tunelsofsteel.abilities;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DagueDeVoleur extends Ability {

    private String item_name = "&aDague de Voleur";

    public DagueDeVoleur() {
        super("Dague de Voleur");
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player) || !(event.getEntity() instanceof Player))
            return;

        Player playerDamager = (Player) event.getDamager();
        ItemStack mainHand = playerDamager.getInventory().getItemInMainHand();

        if (mainHand.getItemMeta() != null) {
            if (!mainHand.getItemMeta().getDisplayName().isEmpty() && (!mainHand.getItemMeta().getDisplayName().equals(item_name))) {
                return;
            }
        }

        Player playerDamage = (Player) event.getEntity();
        TOSPlayer tosPlayerDamage = TOSPlayer.getPlayer(playerDamage);
        ItemStack damagerInHand = playerDamager.getInventory().getItemInMainHand();

        if (!tosPlayerDamage.isImmunized() && damagerInHand.getItemMeta() != null
                && damagerInHand.getItemMeta().getDisplayName().equals(item_name)) {
            if (playerDamager.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
                boolean hasLootTalentiumPur = false;

                if (TOSUtils.getMaxTalentiumPur(playerDamage) != null) {
                    ItemStack stack = TOSUtils.getMaxTalentiumPur(playerDamage);

                    if (TOSUtils.hasPlaceInInventory(playerDamager))
                        playerDamager.getInventory().addItem(stack);
                    else
                        playerDamager.getWorld().dropItemNaturally(playerDamager.getLocation().clone().add(0.5, 0.2, 0.5), stack.clone());

                    stack.setAmount(0);
                    hasLootTalentiumPur = true;
                }

                if (TOSUtils.getMaxTalentium(playerDamage) != null && !hasLootTalentiumPur) {
                    ItemStack stack = TOSUtils.getMaxTalentium(playerDamage);
                    if (TOSUtils.hasPlaceInInventory(playerDamager))
                        playerDamager.getInventory().addItem(stack);
                    else
                        playerDamager.getWorld().dropItemNaturally(playerDamager.getLocation().clone().add(0.5, 0.2, 0.5), stack.clone());

                    stack.setAmount(0);
                } else {
                    ItemStack stack = TOSUtils.getRandomStack(playerDamage);
                    if (stack != null) {
                        if (TOSUtils.hasPlaceInInventory(playerDamager))
                            playerDamager.getInventory().addItem(stack);
                        else
                            playerDamager.getWorld().dropItemNaturally(playerDamager.getLocation().clone().add(0.5, 0.2, 0.5), stack.clone());

                        stack.setAmount(0);
                    } else {
                        TOSPlayer.getPlayer(playerDamager).sendActionBar(ChatColor.DARK_RED + "" + ChatColor.BOLD + "NO ITEM ON THIS PLAYER !", 1.5);
                    }
                }
            } else {
                ItemStack stack = TOSUtils.getRandomStack(playerDamage);
                if (stack != null) {
                    if (TOSUtils.hasPlaceInInventory(playerDamager))
                        playerDamager.getInventory().addItem(stack);
                    else
                        playerDamager.getWorld().dropItemNaturally(playerDamager.getLocation().clone().add(0.5, 0.2, 0.5), stack.clone());

                    stack.setAmount(0);
                } else {
                    ActionBarAPI.sendActionBar(playerDamager, ChatColor.DARK_RED + "" + ChatColor.BOLD + "NO ITEM ON THIS PLAYER !");
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.WOODEN_SWORD, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();

            meta.setDisplayName(item_name);

            meta.addItemFlags(ItemFlag.values());

            if (meta instanceof Damageable)
                ((Damageable) meta).setDamage(57);

            meta.setLore(Arrays.asList(
                    ChatColor.RESET + "A chaque fois que vous frappez un adversaire",
                    ChatColor.RESET + "avec cette dague vous vous emparez",
                    ChatColor.RESET + "d'une case de sont inventaire au hasard",
                    ChatColor.RESET + "(Qu'il s'agisse d'un item unique",
                    ChatColor.RESET + "ou d'un stack entier, ne vole pas",
                    ChatColor.RESET + "les pelles en fer, les torches,",
                    ChatColor.RESET + "et les pommes de terre).",
                    ChatColor.RESET + "   ",
                    ChatColor.RESET + "PASSIF: Si vous êtes invisible,",
                    ChatColor.RESET + "la dague volera un priorité le",
                    ChatColor.YELLOW + "" + ChatColor.BOLD + "TALENTIUM " + ChatColor.RESET + "de la personne ciblée.",
                    ChatColor.RESET + "     ",
                    ChatColor.GRAY + "When in main hand:",
                    ChatColor.GRAY + " 1.6 Attack Speed",
                    ChatColor.GRAY + " 4 Attack Damage"));

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }
}
