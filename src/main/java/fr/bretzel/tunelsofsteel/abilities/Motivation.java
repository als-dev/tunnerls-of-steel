package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Motivation extends Ability {
    private String item_name = ChatColor.translateAlternateColorCodes('&', "&b&lMotivation");

    private Sound sound = Sound.ENTITY_EVOKER_PREPARE_WOLOLO;
    private float volume = 0.3F;
    private float pitch = 1.5F;
    private double duration = 30D;
    private double range = 10;

    private List<String> lore = new ArrayList<>();

    public Motivation() {
        super("Motivation");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        ItemStack stack = event.getItem();
        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && stack != null && stack.getItemMeta() != null) {
            ItemMeta meta = stack.getItemMeta();
            Player player = event.getPlayer();

            if (meta.getDisplayName().equals(item_name) && event.getItem() != null) {
                event.setCancelled(true);

                if (player.getGameMode() != GameMode.CREATIVE)
                    event.getItem().setAmount(stack.getAmount() - 1);

                player.playSound(player.getLocation(), sound, volume, pitch);

                for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), range + 2, range + 2, range + 2)) {
                    if (entity instanceof Player && entity.getLocation().distance(player.getEyeLocation()) <= range + 1) {
                        Player p = (Player) entity;
                        TOSTeam pT = TOSPlayer.getPlayer(p).getTeam();
                        TOSTeam playerTeam = TOSPlayer.getPlayer(player).getTeam();
                        if (pT != null && playerTeam != null && pT.getName().equals(playerTeam.getName())) {
                            p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, ((int) duration * 20), 0, true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, ((int) duration * 20), 0, true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, ((int) duration * 20), 0, true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, ((int) duration * 20), 9, true));

                            p.playSound(player.getLocation(), sound, volume, pitch);
                            p.setHealth(p.getMaxHealth());
                        }
                    }
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", "&b&lMotivation");

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("range"))
            config.addProperty("range", range);

        if (!config.has("duration"))
            config.addProperty("duration", duration);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rTout les alliés dans un rayon de %range% blocs",
                    "&rregagnent tout leur point de vie",
                    "&ret sont motivés pendant %duration% secondes",
                    "  ",
                    "&r&7&o(Heath Boost, Absorbtion, Instant Heal X,",
                    "&r&7&oHaste, Saturation X)")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("sound"))
            sound = Sound.valueOf(config.get("sound").getAsString());

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        if (config.has("range"))
            range = config.get("range").getAsDouble();

        if (config.has("duration"))
            duration = config.get("duration").getAsDouble();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));
    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%range%", String.valueOf((int) range)).replaceAll("%duration%", String.valueOf(duration)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_FAR, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();
            meta.setDisplayName(item_name);
            meta.addItemFlags(ItemFlag.values());
            meta.setLore(lore);
            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }
}
