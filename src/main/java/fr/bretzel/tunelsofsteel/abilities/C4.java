package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.*;

public class C4 extends Ability
{
    public static String ITEM_NAME = ChatColor.AQUA + "" + ChatColor.BOLD + "C4";
    public static String ITEM_LEVER_NAME = ChatColor.AQUA + "" + ChatColor.BOLD + "TELECOMANDE";


    public static UUID OWNER_PLAYER_HEAD = UUID.fromString("8b8fcca0-6880-4108-a46a-5a21320e0d7c");

    public C4()
    {
        super("C4");
    }

    public static boolean isPlayerHead(Material material)
    {
        return material == Material.PLAYER_HEAD || material == Material.PLAYER_WALL_HEAD;
    }

    public static ItemStack getLever(Location blockToExplode)
    {
        ItemStack stack = new ItemStack(Material.LEVER, 1);
        ItemMeta meta = stack.getItemMeta();
        if (meta != null)
        {
            meta.setDisplayName(ITEM_LEVER_NAME);
            meta.setLore(Arrays.asList(blockToExplode.getWorld().getName(), String.valueOf(blockToExplode.getBlockX()), String.valueOf(blockToExplode.getBlockY()), String.valueOf(blockToExplode.getBlockZ())));
            stack.setItemMeta(meta);
        }
        return stack;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockPlace(BlockPlaceEvent event)
    {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (isPlayerHead(block.getType()))
        {
            Skull skull = (Skull) block.getState();

            if (skull.getOwningPlayer() != null && skull.getOwningPlayer().getUniqueId().equals(OWNER_PLAYER_HEAD))
            {
                ItemStack lever = getLever(block.getLocation());

                player.getInventory().addItem(lever);
                block.setMetadata("C4Lever", new FixedMetadataValue(TunnelsOfSteel.INSTANCE, lever));
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockBreakEvent(BlockBreakEvent event)
    {
        Block block = event.getBlock();

        if (isPlayerHead(block.getType()))
        {
            Skull skull = (Skull) block.getState();

            if (skull.getOwningPlayer() != null && skull.getOwningPlayer().getUniqueId().equals(OWNER_PLAYER_HEAD))
            {
                block.getWorld().dropItemNaturally(block.getLocation().add(0.5D, 0.2D, 0.5D), getAbilityItem().get(0));
                event.setDropItems(false);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();

        if (player.getInventory().getItemInMainHand().getType() == Material.LEVER)
        {
            ItemStack stack = player.getInventory().getItemInMainHand();

            if (stack.getItemMeta() != null && !stack.getItemMeta().getDisplayName().isEmpty() &&
                    stack.getItemMeta().getDisplayName().equals(ITEM_LEVER_NAME))
            {
                ItemMeta meta = stack.getItemMeta();
                if (meta != null && meta.getLore() != null && meta.getLore().size() > 0)
                {
                    World world = Bukkit.getWorld(meta.getLore().get(0));
                    Location location = new Location(world, Integer.parseInt(meta.getLore().get(1)), Integer.parseInt(meta.getLore().get(2)), Integer.parseInt(meta.getLore().get(3)));

                    if (world != null && isPlayerHead(location.getBlock().getType()))
                    {
                        for (Entity entity : world.getNearbyEntities(location, 5, 5, 5))
                        {
                            if (entity instanceof Player)
                            {
                                if (entity.getLocation().distance(location) <= 2.5)
                                {
                                    ((Player) entity).damage(20D);
                                }
                            }
                        }

                        location.getBlock().setType(Material.AIR);
                        world.createExplosion(location.getX(), location.getY(), location.getZ(), 3F, false, true);
                    }
                    stack.setAmount(stack.getAmount() - 1);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onExplosion(BlockExplodeEvent event)
    {
        float powerAdd = 0f;
        for (Block block : event.blockList())
        {
            if (isPlayerHead(block.getType()))
            {
                Skull skull = (Skull) block.getState();
                if (skull.getOwningPlayer() != null && skull.getOwningPlayer().getUniqueId().equals(OWNER_PLAYER_HEAD))
                {
                    Location location = block.getLocation();
                    World world = block.getWorld();

                    for (Player player : world.getPlayers())
                    {
                        for (int i = 0; i <= player.getInventory().getSize(); i++)
                        {
                            ItemStack slotStack = player.getInventory().getItem(i);
                            if (slotStack != null && slotStack.getType() == Material.LEVER)
                            {
                                ItemMeta meta = slotStack.getItemMeta();
                                if (meta != null && meta.getDisplayName().equals(ITEM_LEVER_NAME))
                                {
                                    List<String> strings = meta.getLore();

                                    if (strings != null && strings.size() > 0)
                                    {
                                        String worldName = strings.get(0);
                                        int x = Integer.parseInt(strings.get(1));
                                        int y = Integer.parseInt(strings.get(2));
                                        int z = Integer.parseInt(strings.get(3));

                                        if (world.getName().equalsIgnoreCase(worldName) && location.getBlockX() == x && location.getBlockY() == y && location.getBlockZ() == z)
                                            slotStack.setAmount(slotStack.getAmount() - 1);
                                    }
                                }
                            }
                        }
                    }

                    for (Entity entity : world.getNearbyEntities(location, 5, 5, 5)) {
                        if (entity instanceof Player) {
                            if (entity.getLocation().clone().add(0, 1, 0).distance(location) <= 2.5) {
                                ((Player) entity).damage(20D);
                            }
                        }
                    }

                    powerAdd += 0.25F;

                    if (powerAdd >= 8)
                        powerAdd = 8F;

                    float finalPowerAdd = powerAdd;

                    Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
                    {
                        location.getBlock().setType(Material.AIR);
                        world.createExplosion(location.getX(), location.getY(), location.getZ(), 4.5F + finalPowerAdd, false, true);
                    }, 2 + new Random().nextInt(5));
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject)
    {
        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject)
    {
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack stack = TOSUtils.getSkullTexture(OWNER_PLAYER_HEAD, "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGRhMzMyYWJkZTMzM2ExNWE2YzZmY2ZlY2E4M2YwMTU5ZWE5NGI2OGU4ZjI3NGJhZmMwNDg5MmI2ZGJmYyJ9fX0=");

        if (stack != null && stack.getItemMeta() != null) {
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(ITEM_NAME);

            meta.setLore(Arrays.asList(
                    ChatColor.RESET + "Placer le sur une surface puis",
                    ChatColor.RESET + "utiliser la télécommande pour le faire exploser"));

            stack.setItemMeta(meta);
        }

        return Collections.singletonList(stack);
    }
}
