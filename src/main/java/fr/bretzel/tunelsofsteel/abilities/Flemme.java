package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Flemme extends Ability {
    private String item_name = "&b&lFLEMME";

    private Sound sound = Sound.ENTITY_ELDER_GUARDIAN_HURT;
    private float volume = 0.3F;
    private float pitch = 1.5F;
    private double duration = 30D;
    private double range = 10;

    private List<String> lore = new ArrayList<>();

    public Flemme() {
        super("Flemme");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            ItemStack stack = event.getItem();
            if (stack != null && stack.getItemMeta() != null && !stack.getItemMeta().getDisplayName().isEmpty()) {
                ItemMeta meta = stack.getItemMeta();

                Player player = event.getPlayer();

                if (meta.getDisplayName().equals(item_name)) {
                    event.setCancelled(true);

                    if (player.getGameMode() != GameMode.CREATIVE)
                        event.getItem().setAmount(stack.getAmount() - 1);

                    player.playSound(player.getLocation(), sound, volume, pitch);

                    for (Entity entity : player.getWorld().getNearbyEntities(player.getLocation(), range + 1, range + 1, range + 1)) {
                        if (entity instanceof Player && entity.getLocation().distance(player.getEyeLocation()) <= range) {
                            Player p = (Player) entity;
                            TOSPlayer tosP = TOSPlayer.getPlayer(p);
                            TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
                            TOSTeam pT = tosP.getTeam();
                            TOSTeam playerTeam = tosPlayer.getTeam();

                            if (pT != null && playerTeam != null && !pT.getName().equals(playerTeam.getName()) && !tosP.isImmunized()) {

                                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, ((int) duration * 20), 0, true));
                                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, ((int) duration * 20), 3, true));
                                p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, ((int) duration * 20), 0, true));
                                p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, ((int) duration * 20), 9, true));

                                p.playSound(p.getLocation(), sound, volume, pitch);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("range"))
            config.addProperty("range", range);

        if (!config.has("duration"))
            config.addProperty("duration", duration);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rTout les adversaires dans un rayon de 10 blocs",
                    "&rsont touchés par la flemme pendant 30 secondes",
                    "&run rayon de 10 blocs autour de vous",
                    "  ",
                    "&r&7&0(Slowness, Mining Fatigue IV, Weakness, Hunger X)")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("sound"))
            sound = Sound.valueOf(config.get("sound").getAsString());

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        if (config.has("range"))
            range = config.get("range").getAsDouble();

        if (config.has("duration"))
            duration = config.get("duration").getAsDouble();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));
    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%range%", String.valueOf((int) range)).replaceAll("%duration%", String.valueOf(duration)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_11, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();

            meta.setDisplayName(item_name);

            meta.addItemFlags(ItemFlag.values());

            meta.setLore(lore);

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }
}
