package fr.bretzel.tunelsofsteel.abilities.dirtbomb;

import com.google.common.collect.ImmutableList;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.util.PlaceBlockRunnable;
import fr.bretzel.tunelsofsteel.util.raytrace.RayTrace;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class DirtBombProjectile {
    private Item item;
    private Player shooter;
    private DirtBomb dirtBomb;

    private ImmutableList<BlockFace> blockFaces = ImmutableList.copyOf(BlockFace.values());

    public DirtBombProjectile(Item item, Player shooter, DirtBomb dirtBomb) {
        this.item = item;
        this.shooter = shooter;
        this.dirtBomb = dirtBomb;

        trackItem();
    }

    private void onGround() {
        Location origin = item.getLocation().clone();
        World world = origin.getWorld();

        if (world != null) {
            List<Location> locationList = getSphere(origin);
            int blockToPlace = locationList.size() * 5 / 100;
            new PlaceBlockRunnable(world, Material.DIRT, Sound.BLOCK_GRASS_PLACE, true, true, locationList, 1, blockToPlace);
        }

        item.remove();
    }

    private void trackItem() {
        Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, bukkitTask ->
        {
            Location location = item.getLocation().clone();
            Block locBlock = location.getBlock();
            int taskId = bukkitTask.getTaskId();

            if (item.isDead())
                Bukkit.getScheduler().cancelTask(taskId);

            if (!locBlock.getType().isAir() && !item.isOnGround()) {
                onGround();
                Bukkit.getScheduler().cancelTask(taskId);
            } else {
                for (BlockFace blockFace : blockFaces) {
                    Vector direction = blockFace.getDirection().normalize().multiply(0.2D);
                    Location clonedLocation = location.clone().add(direction);
                    if (clonedLocation.getBlock().getType().isSolid()) {
                        onGround();
                        Bukkit.getScheduler().cancelTask(taskId);
                        break;
                    }
                }
            }
        }, 0, 1);
    }

    private List<Location> getSphere(Location origin) {
        ArrayList<Location> locations = new ArrayList<>();

        World world = origin.getWorld();

        Validate.notNull(world, "World cannot be null");

        int cx = origin.getBlockX();
        int cy = origin.getBlockY();
        int cz = origin.getBlockZ();

        for (int z = cz - dirtBomb.getSphereRange(); z <= cz + dirtBomb.getSphereRange(); z++) {
            for (int y = cy - dirtBomb.getSphereRange(); y <= cy + dirtBomb.getSphereRange(); y++) {
                for (int x = cx - dirtBomb.getSphereRange(); x <= cx + dirtBomb.getSphereRange(); x++) {
                    double distance = ((cx - x) * (cx - x) + ((cz - z) * (cz - z)) + ((cy - y) * (cy - y)));

                    Location location = new Location(world, x, y, z);

                    if (distance < dirtBomb.getSphereRange() * dirtBomb.getSphereRange()) {
                        // Bukkit.broadcastMessage(String.valueOf((int) origin.distance(location)));
                        RayTraceResult result = RayTrace.rayTraceBlock(origin, location, dirtBomb.getSphereRange(), 0.1D, block -> !block.getType().isAir());

                        if (validRayTrace(result) && location.getBlock().getType().name().contains("AIR")) {
                            locations.add(location);
                        }
                    }
                }
            }
        }

        locations.sort((location1, location2) ->
        {
            double distance1 = origin.distance(location1);
            double distance2 = origin.distance(location2);

            return Double.compare(distance2, distance1);
        });

        return locations;
    }

    private boolean validRayTrace(RayTraceResult result) {
        return result == null || result.getHitBlock() == null || result.getHitBlock().getType() == Material.AIR;
    }
}