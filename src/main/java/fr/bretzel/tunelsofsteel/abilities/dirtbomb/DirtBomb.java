package fr.bretzel.tunelsofsteel.abilities.dirtbomb;

import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.List;

public class DirtBomb extends Ability {
    private String item_name = ChatColor.translateAlternateColorCodes('&', "&b&lDirt Bomb");

    private int sphereRange = 15;
    private double vectorPower = 1.2D;

    public DirtBomb() {
        super("DirtBomb");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            ItemStack stack = event.getItem();
            if (stack != null && stack.getItemMeta() != null) {
                ItemMeta meta = stack.getItemMeta();
                Player player = event.getPlayer();
                World world = player.getWorld();

                if (meta.getDisplayName().equals(item_name)) {
                    event.setCancelled(true);

                    Item item = world.dropItem(player.getEyeLocation().clone().subtract(0, 0.5, 0), getAbilityItem().get(0));
                    item.setPickupDelay(Integer.MAX_VALUE);
                    item.setCustomName(item_name);
                    item.setVelocity(player.getEyeLocation().getDirection().multiply(vectorPower));
                    new DirtBombProjectile(item, player, this);
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        Bukkit.getWorlds().forEach(world -> world.getEntities().stream()
                .filter(entity -> entity instanceof Item && entity.getCustomName() != null && entity.getCustomName().equals(item_name)).forEach(entity ->
                {
                    new DirtBombProjectile((Item) entity, null, this);
                }));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack stack = new ItemStack(Material.BEETROOT_SEEDS, 1);

        if (stack.getItemMeta() != null) {
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(item_name);

            stack.setItemMeta(meta);
        }

        return Collections.singletonList(stack);
    }

    public double getVectorPower() {
        return vectorPower;
    }

    public int getSphereRange() {
        return sphereRange;
    }
}
