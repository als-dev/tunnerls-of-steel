package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.List;

public class Lantern extends Ability {
    private String item_name = "&b&lLantern";

    public Lantern() {
        super("Lantern");
    }

    @EventHandler
    public void onBlockPos(BlockPlaceEvent event) {

    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {

    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.LANTERN);
        ItemMeta meta = itemStack.getItemMeta();

        if (meta != null) {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', item_name));
        }

        return Collections.singletonList(itemStack);
    }
}
