package fr.bretzel.tunelsofsteel.abilities;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Reset extends Ability {
    private static ArrayList<PlayerImmunizeTask> tasks = new ArrayList<>();
    private String item_name = "&b&lReset";
    private ArrayList<PotionEffectType> potionEffectTypes = new ArrayList<>();
    private ArrayList<String> lore = new ArrayList<>();
    private Sound sound = Sound.ENTITY_PLAYER_LEVELUP;
    private float volume = 0.4F;
    private float pitch = 1.5F;
    private double second = 10D;

    public Reset() {
        super("Reset");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            ItemStack stack = event.getItem();
            if (stack != null && stack.getItemMeta() != null) {
                ItemMeta meta = stack.getItemMeta();
                Player player = event.getPlayer();
                TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

                if (meta.getDisplayName().equals(item_name)) {
                    event.setCancelled(true);

                    if (player.getGameMode() != GameMode.CREATIVE)
                        event.getItem().setAmount(stack.getAmount() - 1);

                    player.playSound(player.getLocation(), sound, volume, pitch);

                    for (PotionEffect potionEffect : player.getActivePotionEffects()) {
                        if (potionEffectTypes.contains(potionEffect.getType()))
                            player.removePotionEffect(potionEffect.getType());
                    }

                    tosPlayer.setImmunized(true);

                    new PlayerImmunizeTask(player, second);
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("second"))
            config.addProperty("second", second);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rRetire tout les debuffs actifs sur vous",
                    "&rvous et vous rend inciblable par",
                    "&rtout les debuffs et effets de repérage",
                    "&rpendant %second% secondes")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        if (!jsonObject.has("potionToRemove")) {
            JsonArray jsonArray = new JsonArray();
            for (PotionEffectType potionEffect : Arrays.asList(PotionEffectType.BLINDNESS, PotionEffectType.SLOW, PotionEffectType.SLOW_DIGGING,
                    PotionEffectType.HUNGER, PotionEffectType.WEAKNESS, PotionEffectType.POISON, PotionEffectType.LEVITATION)) {
                jsonArray.add(potionEffect.getName());
            }
            jsonObject.add("potionToRemove", jsonArray);
        }

        for (PlayerImmunizeTask task : tasks)
            task.cancel();

        tasks.clear();

        lore.clear();

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("sound"))
            sound = Sound.valueOf(config.get("sound").getAsString());

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        if (config.has("second"))
            second = config.get("second").getAsDouble();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));

        if (mainObject.has("potionToRemove"))
            mainObject.get("potionToRemove").getAsJsonArray().forEach(jsonElement -> potionEffectTypes.add(PotionEffectType.getByName(jsonElement.getAsString())));

    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%second%", String.valueOf(second)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.NETHER_STAR, 1);
        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();
            meta.setDisplayName(item_name);
            meta.addItemFlags(ItemFlag.values());
            meta.setLore(lore);

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }

    private static class PlayerImmunizeTask implements Runnable {
        private int taskID;

        private double maxTick;
        private double tick = 0;

        private NumberFormat format = new DecimalFormat("0.0");

        private BossBar bossBar;

        private Player player;
        private TOSPlayer tosPlayer;

        PlayerImmunizeTask(Player player, double maxSec) {
            this.maxTick = maxSec * 20;
            this.player = player;

            tosPlayer = TOSPlayer.getPlayer(player);

            taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();

            bossBar = Bukkit.createBossBar(ChatColor.WHITE + "" + ChatColor.BOLD + "Immuned for " + ChatColor.AQUA +
                            format.format((maxTick - tick) / 20) + ChatColor.WHITE + ChatColor.BOLD + " Sec",
                    BarColor.WHITE, BarStyle.SEGMENTED_12);
            bossBar.setProgress(0);

            bossBar.addPlayer(player);

            tasks.add(this);
        }

        @Override
        public void run() {
            tick += 1;

            if (!tosPlayer.isImmunized())
                tosPlayer.setImmunized(true);

            if (tick <= maxTick) {
                double percent = tick / maxTick;
                bossBar.setProgress(percent);
                bossBar.setTitle(ChatColor.WHITE + "" + ChatColor.BOLD + "Immuned for " + ChatColor.AQUA +
                        format.format((maxTick - tick) / 20) + ChatColor.WHITE + ChatColor.BOLD + " Sec");
            } else {
                cancel();
            }
        }

        public void cancel() {
            ActionBarAPI.sendActionBar(player, " ");
            tosPlayer.setImmunized(false);
            bossBar.removeAll();
            Bukkit.getScheduler().cancelTask(taskID);
        }
    }
}
