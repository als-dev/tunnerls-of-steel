package fr.bretzel.tunelsofsteel.abilities.manager;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import fr.bretzel.tunelsofsteel.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class AbilityManager {
    private static List<Ability> abilities = new ArrayList<>();
    private Plugin plugin;
    private PluginManager pluginManager = Bukkit.getPluginManager();

    public AbilityManager(Plugin plugin) {
        this.plugin = plugin;
    }

    public static List<Ability> getAbilities() {
        return abilities;
    }

    public void registerAbility(Ability ability) {
        if (!hasAbility(ability)) {
            abilities.add(ability);

            try {
                Field field = getManagerField(ability);
                if (field != null)
                {
                    field.setAccessible(true);
                    field.set(ability, this);
                    field.setAccessible(false);
                } else
                {
                    Logger.log("Cannot detect the manager Field !");
                }
            } catch (IllegalAccessException | NoSuchFieldException e)
            {
                abilities.remove(ability);
                e.printStackTrace();
            }

            ability.setEnable(false);//Make sure this ability is disable
            Logger.log("Registered ability: %s".replace("%s", ability.getName()));
        } else
        {
            Logger.log("Ability %s is already registered.".replace("%s", ability.getName()));
        }
    }

    public void loadAbilities()
    {
        for (Ability ability : abilities) {
            Logger.log("Loading ability: " + ChatColor.AQUA + ability.getName());
            try {
                ability.load(parseJsonFile(getFileConfiguration(ability)));
            } catch (Exception e) {
                Logger.log(ChatColor.RED + "Error wen loading " + ability.getName() + " : " + e.getMessage());
                e.printStackTrace();
            }

            pluginManager.registerEvents(ability, plugin);
        }
    }

    public List<ItemStack> getAllItemAbilities() {
        ArrayList<ItemStack> stacks = new ArrayList<>();
        abilities.forEach(ability -> stacks.addAll(ability.getAbilityItem()));
        return stacks;
    }

    public void saveAbilities() {
        for (Ability ability : abilities) {
            Logger.log("Saving ability: " + ChatColor.AQUA + ability.getName());
            HandlerList.unregisterAll(ability);
            try {
                JsonObject object = parseJsonFile(getFileConfiguration(ability));
                object = ability.save(object);
                writeJsonFile(object, getFileConfiguration(ability));
            } catch (Exception e) {
                Logger.log(ChatColor.RED + "Error wen saving " + ability.getName() + " : " + e.getMessage());
                e.printStackTrace();
            }
        }

        abilities.clear();
    }

    public boolean hasAbility(Ability ability)
    {
        return abilities.contains(ability) && hasAbility(ability.getName());
    }

    public boolean hasAbility(String string)
    {
        for (Ability ability : abilities)
        {
            if (ability.getName().equalsIgnoreCase(string))
                return true;
        }

        return false;
    }

    public Ability getAbility(String name)
    {
        for (Ability ability : abilities)
        {
            if (ability.getName().equalsIgnoreCase(name))
                return ability;
        }

        return null;
    }

    public void disable(String abilityName)
    {
        if (hasAbility(abilityName))
        {
            Ability ability = getAbility(abilityName);
            ability.setEnable(false);
            HandlerList.unregisterAll(ability);
        }
    }

    public void enable(String abilityName)
    {
        if (hasAbility(abilityName))
        {
            Ability ability = getAbility(abilityName);
            ability.setEnable(true);
            pluginManager.registerEvents(ability, plugin);
        }
    }

    public File getFileConfiguration(Ability ability)
    {
        File folder = new File(this.plugin.getDataFolder(), "abilities");

        if (!folder.exists())
            folder.mkdirs();

        File configFile = new File(folder, ability.getName() + ".json");
        boolean created = false;
        if (!configFile.exists())
        {
            try
            {
                created = configFile.createNewFile();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (created)
            writeJsonFile(ability.save(new JsonObject()), configFile);

        return configFile;
    }

    private Field getManagerField(Ability ability) throws NoSuchFieldException
    {
        Class clazz = ability.getClass();

        while (clazz != Object.class)
        {
            if (clazz.equals(Ability.class))
            {
                return clazz.getDeclaredField("manager");
            } else if (clazz.getSuperclass() != null)
            {
                clazz = clazz.getSuperclass();
            }
        }

        return null;
    }

    private void writeJsonFile(JsonElement json, File file)
    {
        try
        {
            FileOutputStream obj = new FileOutputStream(file);
            obj.write(new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(json).getBytes(StandardCharsets.UTF_8));
            obj.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public JsonObject parseJsonFile(File file)
    {
        try
        {
            FileInputStream inputStream = new FileInputStream(file);
            return new JsonParser().parse(new JsonReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))).getAsJsonObject();
        } catch (FileNotFoundException e)
        {
            e.fillInStackTrace();
            return new JsonObject();
        }
    }
}
