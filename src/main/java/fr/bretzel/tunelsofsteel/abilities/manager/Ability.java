package fr.bretzel.tunelsofsteel.abilities.manager;

import com.google.gson.JsonObject;

import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.List;

public abstract class Ability implements Listener
{
    private String name;
    private boolean enable;
    private AbilityManager manager;

    public Ability(String name)
    {
        this.name = name;
    }

    public abstract JsonObject save(JsonObject jsonObject);
    public abstract void load(JsonObject mainObject);

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public abstract List<ItemStack> getAbilityItem();

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    public File getFileConfiguration()
    {
        if (manager != null)
            return manager.getFileConfiguration(this);
        return null;
    }
}
