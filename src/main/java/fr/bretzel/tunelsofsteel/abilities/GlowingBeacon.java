package fr.bretzel.tunelsofsteel.abilities;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.event.TeamUpdateEvent;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Material;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

import java.util.*;

public class GlowingBeacon extends Ability {

    private static String item_name = ChatColor.AQUA + "" + ChatColor.BOLD + "GLOWING BEACON";
    private static double second_to_bip = 3;

    private ArrayList<String> lore = new ArrayList<>();
    private static Sound sound = Sound.BLOCK_NOTE_BLOCK_HARP;
    private static double activation_range = 10.9D;
    private static float pitch = 1.5F;
    private static float volume = 0.2F;
    private ArrayList<BlockGlowingBeacon> blockGlowingBeaconsList = new ArrayList<>();

    public GlowingBeacon() {
        super("GlowingBeacon");
        Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, new GlowingBeaconTask(), 20, 1);
    }

    public void addGlowingBeacons(Location location, TOSTeam team) {
        blockGlowingBeaconsList.add(new BlockGlowingBeacon(location, team));
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerPlaceBlock(BlockPlaceEvent event) {
        if (event.getPlayer().getItemInHand().getItemMeta() != null
                && !event.getPlayer().getItemInHand().getItemMeta().getDisplayName().isEmpty()
                && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(item_name)) {
            TOSTeam team = TOSPlayer.getPlayer(event.getPlayer()).getTeam();

            if (team != null) {
                addGlowingBeacons(event.getBlock().getLocation(), team);

                event.getBlock().getWorld().playSound(event.getBlock().getLocation(), Sound.BLOCK_BEACON_ACTIVATE, SoundCategory.AMBIENT, 1F, 1.8F);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerBreakBlock(BlockBreakEvent event) {
        ArrayList<BlockGlowingBeacon> toRemove = new ArrayList<>();

        for (BlockGlowingBeacon glowingBeacon : blockGlowingBeaconsList)
            if (glowingBeacon.is(event.getBlock().getLocation())) {
                glowingBeacon.removeAllGlowingBeacon();
                toRemove.add(glowingBeacon);
                event.setDropItems(false);
            }

        for (BlockGlowingBeacon glowingBeacon : toRemove)
            blockGlowingBeaconsList.remove(glowingBeacon);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null)
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.BEACON && !event.getPlayer().isSneaking())
                for (BlockGlowingBeacon glowingBeacon : blockGlowingBeaconsList)
                    if (glowingBeacon.is(event.getClickedBlock().getLocation()))
                        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        TOSTeam team = TOSPlayer.getPlayer(player).getTeam();

        if (team != null) {
            List<BlockGlowingBeacon> beaconArrayList = getGlowingBeacon(team);

            if (beaconArrayList.size() > 0)
                for (BlockGlowingBeacon blockGlowingBeacon : beaconArrayList)
                    blockGlowingBeacon.getPlayerViewer().remove(player);
        }
    }

    @EventHandler
    public void onPlayerConnect(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        TOSTeam team = TeamManager.getTeamOfPlayer(player);

        if (team != null) {
            List<BlockGlowingBeacon> beaconArrayList = getGlowingBeacon(team);

            if (beaconArrayList.size() > 0)
                for (BlockGlowingBeacon blockGlowingBeacon : beaconArrayList)
                    blockGlowingBeacon.showGlowingBeacon(event.getPlayer());
        }
    }

    @EventHandler
    public void onTeamUpdate(TeamUpdateEvent event) {
        TOSTeam tosTeam = event.getTeam();
        List<BlockGlowingBeacon> blockGlowingBeacons = getGlowingBeacon(tosTeam);

        if (blockGlowingBeacons != null && blockGlowingBeacons.size() > 0)
            for (BlockGlowingBeacon blockGlowingBeacon : blockGlowingBeacons)
                blockGlowingBeacon.actualiseViewer();

    }

    public List<BlockGlowingBeacon> getGlowingBeacon(TOSTeam team) {
        ArrayList<BlockGlowingBeacon> arrayList = new ArrayList<>();
        blockGlowingBeaconsList.forEach(blockGlowingBeacon ->
        {
            if (blockGlowingBeacon.getOwner().getName().equals(team.getName()))
                arrayList.add(blockGlowingBeacon);
        });
        return arrayList;
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", "&b&lGLOWING BEACON");

        if (!config.has("second_to_bip"))
            config.addProperty("second_to_bip", second_to_bip);

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("activation_range"))
            config.addProperty("activation_range", activation_range);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rUne fois placé il permet de voir tout les adversaires",
                    "&rdans un rayon de %range% blocs à travers les parois.",
                    "&rAttention car dans ce mème rayon, il est possible d'entendre",
                    "&rla position du beacon et lorsque que se dernier est détruit",
                    "&ril est perdu.")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        JsonArray glowingBeaconArray = new JsonArray();

        for (TOSTeam team : TeamManager.getRegisteredTeam()) {
            List<BlockGlowingBeacon> glowingBeacons = getGlowingBeacon(team);
            if (glowingBeacons.size() > 0) {
                JsonObject teamObject = new JsonObject();
                teamObject.addProperty("team", team.getName());

                JsonArray beacon = new JsonArray();

                for (BlockGlowingBeacon blockGlowingBeacon : glowingBeacons) {
                    blockGlowingBeacon.removeAllGlowingBeacon();
                    beacon.add(TunnelsOfSteel.toStringLocation(blockGlowingBeacon.getLocation()));
                }

                teamObject.add("beacon", beacon);
                glowingBeaconArray.add(teamObject);
            }
        }

        blockGlowingBeaconsList.clear();
        lore.clear();

        if (jsonObject.has("glowing_beacon")) {
            jsonObject.remove("glowing_beacon");
        }
        jsonObject.add("glowing_beacon", glowingBeaconArray);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("second_to_bip"))
            second_to_bip = config.get("second_to_bip").getAsDouble();

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        try {
            if (config.has("sound"))
                sound = Sound.valueOf(config.get("sound").getAsString());
        } catch (Exception e) {
            sound = Sound.BLOCK_NOTE_BLOCK_HARP;
            Logger.log("The sound is null, please check your config file !");
        } finally {
            if (sound == null) {
                sound = Sound.BLOCK_NOTE_BLOCK_HARP;
                Logger.log("The sound is null, please check your config file !");
            }
        }
        if (config.has("activation_range"))
            activation_range = config.get("activation_range").getAsDouble();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));

        if (mainObject.has("glowing_beacon")) {
            mainObject.getAsJsonArray("glowing_beacon").forEach(jsonElement ->
            {
                JsonObject teamObject = jsonElement.getAsJsonObject();
                TOSTeam tosTeam = TeamManager.getTeam(Color.match(teamObject.get("team").getAsString()));

                teamObject.getAsJsonArray("beacon").forEach(beacon ->
                        addGlowingBeacons(TunnelsOfSteel.toLocationString(beacon.getAsString()), tosTeam));
            });
        }
    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%range%", String.valueOf((int) activation_range)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.BEACON, 1);
        ItemMeta meta = itemStack.getItemMeta();

        if (meta != null) {
            meta.setDisplayName(item_name);
            meta.setLore(lore);
            meta.addItemFlags(ItemFlag.values());
            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }

    private static class BlockGlowingBeacon
    {
        private List<Player> playerViewer = new ArrayList<>();
        private EntityShulker shulker;
        private TOSTeam owner;
        private Location location;
        public double currentTick;

        BlockGlowingBeacon(Location location, TOSTeam owner) {
            if (location == null)
                throw new NullPointerException();
            if (owner == null)
                throw new NullPointerException();

            if (location.getWorld() == null)
                throw new NullPointerException();

            this.location = location;
            this.owner = owner;

            this.shulker = new EntityShulker(EntityTypes.SHULKER, ((CraftWorld) location.getWorld()).getHandle());
            shulker.setPosition(location.getBlockX() + 0.5, location.getBlockY(), location.getBlockZ() + 0.5);
            shulker.setInvisible(true);
            shulker.setNoGravity(true);
            shulker.setFlag(6, true);
            shulker.glowing = true;
            shulker.setNoAI(true);

            Team glowingTeam = getOwner().getScoreboard().getTeam("GLBeacon");

            if (glowingTeam == null) {
                glowingTeam = getOwner().getScoreboard().registerNewTeam("GLBeacon");
                glowingTeam.setColor(getOwner().getColor().getChatColor());

            }

            glowingTeam.addEntry(shulker.getUniqueID().toString());

            showAllGlowingBeacon();
        }

        public boolean is(Location location)
        {
            if (location == null || location.getWorld() == null || getLocation().getWorld() == null)
                return false;

            return getLocation().getBlockX() == location.getBlockX() && getLocation().getBlockY() == location.getBlockY() && getLocation().getBlockZ() == location.getBlockZ() &&
                    getLocation().getWorld().getName().equals(location.getBlock().getWorld().getName());
        }

        public Location getLocation()
        {
            return location;
        }

        public TOSTeam getOwner()
        {
            return owner;
        }

        public List<Player> getPlayerViewer()
        {
            return playerViewer;
        }

        public void actualiseViewer()
        {
            List<Player> toRemoveViewer = new ArrayList<>();

            for (Player player : getPlayerViewer()) {
                if (!player.isOnline()) {
                    toRemoveViewer.add(player);
                    continue;
                }

                TOSTeam playerTeam = TeamManager.getTeamOfPlayer(player);

                if (playerTeam == null) {
                    toRemoveViewer.add(player);
                    removeGlowingBeacon(player);
                } else if (player.isOnline() && !playerTeam.getName().equalsIgnoreCase(getOwner().getName())) {
                    toRemoveViewer.add(player);
                    removeGlowingBeacon(player);
                }
            }

            getPlayerViewer().removeAll(toRemoveViewer);

            for (OfflinePlayer offlinePlayer : getOwner().getPlayers())
            {
                if (offlinePlayer.isOnline())
                {
                    Player player = offlinePlayer.getPlayer();
                    if (player != null)
                    {
                        if (!getPlayerViewer().contains(player))
                        {
                            getPlayerViewer().add(player);
                            showGlowingBeacon(player);
                        }
                    }
                }
            }
        }

        private void removeGlowingBeacon(OfflinePlayer offlinePlayer)
        {
            if (offlinePlayer.isOnline())
            {
                Player player = offlinePlayer.getPlayer();
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), new PacketPlayOutEntityDestroy(shulker.getId()));
                if (player != null && !getPlayerViewer().contains(player))
                {
                    getPlayerViewer().remove(player);
                }
            }
        }

        private void showGlowingBeacon(OfflinePlayer offlinePlayer)
        {
            if (offlinePlayer.isOnline()) {
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), new PacketPlayOutSpawnEntityLiving(shulker));
                TOSUtils.sendPacket(offlinePlayer.getPlayer(), new PacketPlayOutEntityMetadata(shulker.getId(), shulker.getDataWatcher(), true));

                Player player = offlinePlayer.getPlayer();
                if (player != null && !getPlayerViewer().contains(player)) {
                    getPlayerViewer().add(player);
                }
            }
        }

        public void removeAllGlowingBeacon()
        {
            for (Player player : getPlayerViewer())
            {
                if (player.isOnline())
                {
                    removeGlowingBeacon(player);
                }
            }

            getPlayerViewer().clear();
        }

        public void showAllGlowingBeacon()
        {
            for (OfflinePlayer offlinePlayer : getOwner().getPlayers())
            {
                if (offlinePlayer.isOnline())
                {
                    showGlowingBeacon(offlinePlayer);
                }
            }
        }
    }

    private class GlowingBeaconTask implements Runnable {
        private double maxTick = second_to_bip * 20;

        @Override
        public void run() {
            if (blockGlowingBeaconsList.size() <= 0)
                return;

            ArrayList<BlockGlowingBeacon> toRemove = new ArrayList<>();

            for (BlockGlowingBeacon blockGlowingBeacon : blockGlowingBeaconsList) {
                if (blockGlowingBeacon.getLocation().getBlock().getType() != Material.BEACON) {
                    toRemove.add(blockGlowingBeacon);
                } else {
                    blockGlowingBeacon.currentTick += 1;
                    World world = blockGlowingBeacon.getLocation().getWorld();

                    if (world != null) {
                        Collection<Entity> entityList = world.
                                getNearbyEntities(blockGlowingBeacon.getLocation(), activation_range + 1D, activation_range + 1D, activation_range + 1D, entity -> entity instanceof Player);

                        if (entityList.size() > 0) {
                            for (Entity entity : entityList)
                            {
                                Player player = (Player) entity;
                                TOSTeam playerTeam = TOSPlayer.getPlayer(player).getTeam();

                                if (playerTeam != null && !playerTeam.getName().equals(blockGlowingBeacon.getOwner().getName()) && !TOSPlayer.getPlayer(player).isImmunized() &&
                                        player.getLocation().distance(blockGlowingBeacon.getLocation()) <= activation_range) {
                                    if (blockGlowingBeacon.currentTick >= maxTick)
                                        ActionBarAPI.sendActionBar(player, ChatColor.DARK_RED + "" + ChatColor.BOLD + "YOU HAVE BEEN SPOTTED !");

                                    player.removePotionEffect(PotionEffectType.GLOWING);
                                    player.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 6, 0, true));
                                }
                            }
                        }
                    }

                    if (blockGlowingBeacon.currentTick >= maxTick) {
                        blockGlowingBeacon.currentTick = 0;

                        if (world != null)
                            world.playSound(blockGlowingBeacon.getLocation(), sound, volume, pitch);
                    }
                }
            }

            //Remove GlowingBeacon
            if (toRemove.size() > 0) {
                for (BlockGlowingBeacon blockGlowingBeacon : toRemove) {
                    blockGlowingBeacon.removeAllGlowingBeacon();
                    blockGlowingBeaconsList.remove(blockGlowingBeacon);
                }
            }
        }
    }
}
