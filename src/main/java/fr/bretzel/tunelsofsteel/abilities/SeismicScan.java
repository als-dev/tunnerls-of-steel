package fr.bretzel.tunelsofsteel.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.bretzel.tunelsofsteel.abilities.manager.Ability;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class SeismicScan extends Ability {
    private String item_name = "&b&lSeismic Scan";

    private Sound sound = Sound.ENTITY_PIG_SADDLE;
    private float volume = 0.3F;
    private float pitch = 1.5F;
    private double range = 15D;
    private int second = 3;

    private List<String> lore = new ArrayList<>();

    private HashMap<Player, Long> lastSendActionBar = new HashMap<>();

    public SeismicScan() {
        super("SeismicScan");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.useInteractedBlock() == Event.Result.DENY || event.useItemInHand() == Event.Result.DENY)
            return;

        ItemStack stack = event.getItem();
        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && stack != null && stack.getItemMeta() != null) {
            ItemMeta meta = stack.getItemMeta();
            Player player = event.getPlayer();

            if (meta.getDisplayName().equals(item_name) && event.getItem() != null) {
                event.setCancelled(true);
                if (player.getGameMode() != GameMode.CREATIVE)
                    event.getItem().setAmount(stack.getAmount() - 1);

                player.playSound(player.getLocation(), sound, volume, pitch);

                Collection<Entity> entities = player.getWorld().getNearbyEntities(player.getLocation(), range + 2, range + 2, range + 2, entity -> entity instanceof Player);

                boolean foundPlayer = false;

                if (entities.size() > 1) {
                    for (Entity entity : entities) {
                        if (entity.getLocation().distance(player.getEyeLocation()) <= (range + 1)) {
                            Player p = (Player) entity;
                            TOSTeam pT = TOSPlayer.getPlayer(p).getTeam();
                            TOSPlayer tosP = TOSPlayer.getPlayer(p);
                            TOSTeam playerTeam = TOSPlayer.getPlayer(player).getTeam();

                            if (pT != null && playerTeam != null && !pT.getName().equals(playerTeam.getName()) && !tosP.isImmunized()) {
                                foundPlayer = true;

                                boolean sendActionBar = false;

                                if (!lastSendActionBar.containsKey(player)) {
                                    sendActionBar = true;
                                    lastSendActionBar.put(player, System.nanoTime());
                                } else if (TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - lastSendActionBar.get(player)) >= 1.5) {
                                    lastSendActionBar.replace(player, System.nanoTime());
                                    sendActionBar = true;
                                }

                                if (sendActionBar)
                                    tosP.sendActionBar(ChatColor.DARK_RED + "" + ChatColor.BOLD + "YOU HAVE BEEN SPOTTED !", 1.4);

                                p.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, (second * 20), 0, true));
                                p.playSound(player.getLocation(), sound, volume, pitch);
                            }
                        }
                    }
                }

                if (foundPlayer) {
                    player.sendTitle("", ChatColor.GREEN + "Ennemies spotted, look around you !", 7, 15, 7);
                } else {
                    player.sendTitle("", ChatColor.RED + "No ennemy in your proximity...", 7, 15, 7);
                }
            }
        }
    }

    @Override
    public JsonObject save(JsonObject jsonObject) {
        JsonObject config = jsonObject.has("config") ? jsonObject.get("config").getAsJsonObject() : new JsonObject();

        if (!config.has("item_name"))
            config.addProperty("item_name", item_name);

        if (!config.has("sound_comment"))
            config.addProperty("sound_comment", "Look all available sound here: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");

        if (!config.has("sound"))
            config.addProperty("sound", sound.name());

        if (!config.has("volume_comment"))
            config.addProperty("volume_comment", "The volume of the sound, max is 2, min is 0");

        if (!config.has("volume"))
            config.addProperty("volume", volume);

        if (!config.has("pitch_comment"))
            config.addProperty("pitch_comment", "The pitch of the sound, max is 2.0, min is 0.0");

        if (!config.has("pitch"))
            config.addProperty("pitch", pitch);

        if (!config.has("range"))
            config.addProperty("range", range);

        if (!config.has("second"))
            config.addProperty("second", second);

        if (!config.has("lore")) {
            JsonArray array = new JsonArray();

            for (String defaultLore : Arrays.asList(
                    "&rRévèle tout les joueurs adverses",
                    "&rdans un rayon de %range% blocs a travers",
                    "&rles murs pendant %second% secondes")) {
                array.add(defaultLore);
            }

            config.add("lore", array);
        }

        if (jsonObject.has("config"))
            jsonObject.remove("config");

        jsonObject.add("config", config);

        return jsonObject;
    }

    @Override
    public void load(JsonObject mainObject) {
        JsonObject config = mainObject.has("config") ? mainObject.get("config").getAsJsonObject() : new JsonObject();

        if (config.has("item_name"))
            item_name = ChatColor.translateAlternateColorCodes('&', config.get("item_name").getAsString());

        if (config.has("sound"))
            sound = Sound.valueOf(config.get("sound").getAsString());

        if (config.has("volume"))
            volume = config.get("volume").getAsFloat();

        if (config.has("pitch"))
            pitch = config.get("pitch").getAsFloat();

        if (config.has("range"))
            range = config.get("range").getAsDouble();

        if (config.has("second"))
            second = config.get("second").getAsInt();

        if (config.has("lore"))
            config.getAsJsonArray("lore").forEach(jsonElement -> lore.add(addLore(jsonElement.getAsString())));
    }

    private String addLore(String str) {
        return ChatColor.translateAlternateColorCodes('&', str.replaceAll("%range%", String.valueOf((int) range)).replaceAll("%second%", String.valueOf(second)));
    }

    @Override
    public List<ItemStack> getAbilityItem() {
        ItemStack itemStack = new ItemStack(Material.TRIPWIRE_HOOK, 1);

        if (itemStack.getItemMeta() != null) {
            ItemMeta meta = itemStack.getItemMeta();

            meta.setDisplayName(item_name);

            meta.addItemFlags(ItemFlag.values());

            meta.setLore(lore);

            itemStack.setItemMeta(meta);
        }

        return Collections.singletonList(itemStack);
    }
}
