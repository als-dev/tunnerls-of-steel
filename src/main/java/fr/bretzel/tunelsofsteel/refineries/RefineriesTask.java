package fr.bretzel.tunelsofsteel.refineries;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import org.bukkit.material.Furnace;
import org.bukkit.util.Vector;

import java.util.Objects;

public class RefineriesTask implements Runnable
{
    private final int DEFAULT_BURNING = 0;
    private final int DEFAULT_MAX_BURNING_TIME = 56;
    private Refineries refineries;
    private int taskID;
    private int burnTick = 0;
    private int MAX_BURNING_TIME = 56;
    private boolean BURNING_RUN = false;
    private boolean PAUSE = false;

    private ItemStack MAGMA;
    private ItemStack NETHER_WART_BLOCK;

    private ItemStack BACKUP_INPUT;
    private ItemStack BACKUP_INPUT_FOR_OUTPUT;


    private Upgrade UPGRADE = Upgrade.DEFAULT;

    private boolean remove1Block = false;

    private long timeStart = System.nanoTime();

    public RefineriesTask(Refineries refineries)
    {
        this.refineries = refineries;
        this.taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
        this.MAGMA = new ItemStack(Material.MAGMA_BLOCK);
        ItemMeta meta = MAGMA.getItemMeta();
        meta.setDisplayName(RefineriesManager.REFINERIES_MAGMA_NAME);
        this.MAGMA.setItemMeta(meta);
        this.NETHER_WART_BLOCK = new ItemStack(Material.NETHER_WART_BLOCK);
        meta = NETHER_WART_BLOCK.getItemMeta();
        meta.setDisplayName(RefineriesManager.REFINERIES_NETHER_WART_NAME);
        this.NETHER_WART_BLOCK.setItemMeta(meta);
    }

    @Override
    public void run()
    {
        if (PAUSE)
            return;

        Upgrade backup_upgrade = UPGRADE;

        ItemStack upgrade_stack = refineries.getInventory().getItem(22);

        UPGRADE = RefineriesManager.getUpgradeFromStack(upgrade_stack);

        if (UPGRADE == Upgrade.DEFAULT)
        {
            if (upgrade_stack != null)
            {
                dropItemStackInRefinerieDir(upgrade_stack);
                refineries.getInventory().setItem(22, new ItemStack(Material.AIR));
                refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);
            }
            MAX_BURNING_TIME = DEFAULT_MAX_BURNING_TIME;
        } else if (UPGRADE == Upgrade.BRIGHT_FIRE)
        {
            MAX_BURNING_TIME = Double.valueOf(DEFAULT_MAX_BURNING_TIME / 1.5).intValue();
        } else if (UPGRADE == Upgrade.SOFT_FIRE)
        {
            MAX_BURNING_TIME = Double.valueOf(DEFAULT_MAX_BURNING_TIME * 4).intValue();
        } else if (UPGRADE == Upgrade.ALCHEMIST)
        {
            MAX_BURNING_TIME = DEFAULT_MAX_BURNING_TIME;
        } else if (UPGRADE == Upgrade.PRIVATE)
        {
            MAX_BURNING_TIME = DEFAULT_MAX_BURNING_TIME;
        }

        if (backup_upgrade != UPGRADE)
        {
            if (backup_upgrade == Upgrade.ALCHEMIST && !TunnelsOfSteel.isWhiteListedBurnItems(BACKUP_INPUT))
            {
                if (refineries.getInputStack() != null)
                {
                    dropItemStackInRefinerieDir(refineries.getInputStack());
                }
                if (BACKUP_INPUT != null && !BURNING_RUN)
                {
                    dropItemStackInRefinerieDir(BACKUP_INPUT);
                }
                if (BURNING_RUN && BACKUP_INPUT != null)
                {
                    ItemStack stack = BACKUP_INPUT.clone();
                    stack.setAmount(1);
                    dropItemStackInRefinerieDir(stack);
                }

                refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);
                refineries.getInventory().setItem(9, new ItemStack(Material.AIR));

                BURNING_RUN = false;
                refineries.visualFire(false);
                burnTick = DEFAULT_BURNING;
                remove1Block = false;
                BACKUP_INPUT = null;

                for (int i = 10; i <= 16; i++)
                {
                    refineries.getInventory().setItem(i, NETHER_WART_BLOCK);
                }

                return;
            } else if (UPGRADE == Upgrade.PRIVATE)
            {
                if (!RefineriesManager.isWhiteListedUpgradeItems(upgrade_stack))
                {
                    if (refineries.getOutputStack() != null)
                    {
                        dropItemStackInRefinerieDir(refineries.getInventory().getItem(22));
                    }

                    refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);
                    refineries.getInventory().setItem(22, new ItemStack(Material.AIR));
                }

                Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () -> refineries.getInventory().setItem(22, new ItemStack(Material.AIR)), (20 * 60) * 6);
            }

            refineries.setUpgrade(UPGRADE);

            burnTick = DEFAULT_BURNING;

            BURNING_RUN = false;
            refineries.visualFire(false);

            for (int i = 10; i <= 16; i++)
            {
                refineries.getInventory().setItem(i, NETHER_WART_BLOCK);
            }

            return;
        }

        if (refineries.getInputStack() != null || BURNING_RUN)
        {
            //Set furnace active !
            if (BURNING_RUN)
            {
                if (refineries.getFurnaceLoc().getBlock().getType() == Material.FURNACE)
                {
                    //#Effect !
                    if (UPGRADE == Upgrade.BRIGHT_FIRE)
                    {
                        for (Location center : refineries.getBurningBlocks())
                            center.getWorld().
                                    spawnParticle(Particle.FLAME, center.getX(), center.getY(), center.getZ(), 1, 0, 0, 0, 1, null, true);

                    } else if (UPGRADE == Upgrade.SOFT_FIRE)
                    {
                        for (Location center : refineries.getBurningBlocks())
                            center.getWorld().
                                    spawnParticle(Particle.SMOKE_LARGE, center.getX(), center.getY(), center.getZ(), 1, 0, 0, 0, 1, null, true);

                    }


                    refineries.visualFire(true);
                    Furnace dataFurnace = new Furnace(refineries.getFacing());
                    dataFurnace.setFacingDirection(refineries.getFacing());
                    org.bukkit.block.Furnace blockFurnace = (org.bukkit.block.Furnace) refineries.getFurnaceLoc().getBlock().getState();
                    blockFurnace.setData(dataFurnace);
                    refineries.getFurnaceLoc().getBlock().getState().update();
                    blockFurnace.update();
                }
            } else
            {
                if (refineries.getFurnaceLoc().getBlock().getType() != Material.FURNACE)
                {
                    refineries.visualFire(false); //Moved after 100%

                    refineries.getFurnaceLoc().getBlock().setType(Material.FURNACE);
                    Furnace dataFurnace = new Furnace(refineries.getFacing());
                    dataFurnace.setFacingDirection(refineries.getFacing());

                    org.bukkit.block.Furnace blockFurnace = (org.bukkit.block.Furnace) refineries.getFurnaceLoc().getBlock().getState();

                    blockFurnace.setData(dataFurnace);
                    refineries.getFurnaceLoc().getBlock().getState().update();
                    blockFurnace.update();
                }
            }

            ItemStack alreadyOutputtedStack = refineries.getOutputStack();
            ItemStack inputStack = refineries.getInputStack();

            //Furnace CODE
            if (BURNING_RUN)
            {

                if (inputStack != null && BACKUP_INPUT.getType() != inputStack.getType())
                {
                    BACKUP_INPUT = inputStack.clone();
                }

                if (TunnelsOfSteel.isWhiteListedBurnItems(BACKUP_INPUT) || (UPGRADE == Upgrade.ALCHEMIST && BACKUP_INPUT.getType().isBlock()) || (backup_upgrade == Upgrade.ALCHEMIST && remove1Block))
                {
                    int addToOutputStack = TunnelsOfSteel.getOutputForItem(BACKUP_INPUT_FOR_OUTPUT);

                    if (UPGRADE == Upgrade.SOFT_FIRE)
                    {
                        addToOutputStack = addToOutputStack * 2;
                    }

                    int simulateAddOutput = (alreadyOutputtedStack == null ? 0 : alreadyOutputtedStack.getAmount()) + addToOutputStack;

                    if (simulateAddOutput <= 64)
                    {
                        double multiply = (100 * 8 / (double) MAX_BURNING_TIME) / 8;
                        double percent = burnTick * multiply;
                        int position = Double.valueOf(Math.round(8 * percent / 100)).intValue() + 9;

                        //Fix a duplicate bug
                        if (position == 10 && refineries.getInputStack() == null)
                        {
                            burnTick = DEFAULT_BURNING;
                            BURNING_RUN = false;
                            refineries.visualFire(false);
                        }

                        if (position >= 11)
                        {
                            if (position == 11 && !remove1Block)
                            {
                                if (inputStack != null && BACKUP_INPUT_FOR_OUTPUT.getType() != inputStack.getType())
                                {
                                    BACKUP_INPUT_FOR_OUTPUT = inputStack.clone();
                                }

                                TOSUtils.removeItem(refineries.getInventory(), 9, 1);
                                remove1Block = true;
                            }

                            BURNING_RUN = true;

                            if (position <= 17)
                                refineries.getInventory().setItem(position - 1, MAGMA);
                        }

                        if (percent >= 100)
                        {
                            if (refineries.getInputStack() == null)
                            {
                                refineries.visualFire(false);
                                BURNING_RUN = false;
                            }

                            remove1Block = false;

                            burnTick = DEFAULT_BURNING;

                            ItemStack out = new ItemStack(Material.MAGMA_CREAM, 1);
                            ItemMeta meta = out.getItemMeta();
                            meta.addEnchant(Enchantment.OXYGEN, 1, true);
                            meta.setDisplayName(RefineriesManager.TALENTIUM_NAME);
                            out.setItemMeta(meta);

                            if (UPGRADE == Upgrade.PRIVATE)
                            {
                                ItemMeta upgrade_stackItemMeta = upgrade_stack.getItemMeta();
                                CustomItemTagContainer tagContainer = upgrade_stackItemMeta.getCustomTagContainer();
                                NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "UpgradeTeamOwner");

                                fr.bretzel.tunelsofsteel.team.Color team = null;

                                if (tagContainer.hasCustomTag(namespacedKey, ItemTagType.STRING))
                                {
                                    team = Color.match(tagContainer.getCustomTag(namespacedKey, ItemTagType.STRING));
                                }

                                TOSTeam owner = TeamManager.getTeam(team);

                                if (TeamManager.getEnderChest(owner).firstEmpty() != -1)
                                {
                                    Inventory inv = TeamManager.getEnderChest(owner);

                                    out.setAmount(addToOutputStack);

                                    inv.addItem(TOSUtils.hideEnchantments(out));
                                } else
                                {
                                    TOSUtils.addInInventoryItem(refineries.getInventory(), 17, addToOutputStack, TOSUtils.hideEnchantments(out));
                                }

                            } else
                            {
                                TOSUtils.addInInventoryItem(refineries.getInventory(), 17, addToOutputStack, TOSUtils.hideEnchantments(out));
                            }

                            long value = System.nanoTime() - timeStart;

                            //Bukkit.broadcastMessage(TimeUnit.NANOSECONDS.toSeconds(value) + "s to burn");

                            for (int i = 10; i <= 16; i++)
                            {
                                refineries.getInventory().setItem(i, NETHER_WART_BLOCK);
                            }
                        }

                        if (burnTick >= 0)
                        {
                            if (alreadyOutputtedStack != null && alreadyOutputtedStack.getAmount() >= 64)
                            {
                                refineries.visualFire(false);
                                BURNING_RUN = false;
                                burnTick = DEFAULT_BURNING;
                            }
                        }

                        burnTick++;
                    } else
                    {
                        refineries.visualFire(false);
                        BURNING_RUN = false;
                        burnTick = DEFAULT_BURNING;
                    }
                } else
                {
                    refineries.visualFire(false);
                    BURNING_RUN = false;
                    burnTick = DEFAULT_BURNING;
                    remove1Block = false;
                    BACKUP_INPUT = null;

                    refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);

                    if (refineries.getInputStack() != null)
                    {
                        dropItemStackInRefinerieDir(refineries.getInputStack());
                    }

                    refineries.getInventory().setItem(9, new ItemStack(Material.AIR));

                    for (int i = 10; i <= 16; i++)
                    {
                        refineries.getInventory().setItem(i, NETHER_WART_BLOCK);
                    }
                }

                //Set furnace active if is possible !
            } else if (!BURNING_RUN && inputStack.getType() != null && (TunnelsOfSteel.isWhiteListedBurnItems(inputStack.clone()) || UPGRADE == Upgrade.ALCHEMIST))
            {
                if (inputStack.getType().isBlock() || TunnelsOfSteel.isWhiteListedBurnItems(inputStack))
                {
                    int addToOutputStack = TunnelsOfSteel.getOutputForItem(inputStack);

                    if (UPGRADE == Upgrade.SOFT_FIRE)
                        addToOutputStack = addToOutputStack * 2;

                    int simulateAddOutput = (alreadyOutputtedStack == null ? 0 : alreadyOutputtedStack.getAmount()) + addToOutputStack;

                    if (simulateAddOutput <= 64)
                    {
                        BURNING_RUN = true;
                        BACKUP_INPUT = inputStack.clone();
                        BACKUP_INPUT_FOR_OUTPUT = inputStack.clone();
                        burnTick++;
                        timeStart = System.nanoTime();
                    }
                } else
                {
                    refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);
                    refineries.getInventory().setItem(9, new ItemStack(Material.AIR));
                    dropItemStackInRefinerieDir(inputStack);
                    BACKUP_INPUT = null;
                }
            } else if (!TunnelsOfSteel.isWhiteListedBurnItems(inputStack))
            {
                refineries.getFurnaceLoc().getWorld().playSound(refineries.getFurnaceLoc(), Sound.BLOCK_ANVIL_DESTROY, 0.2F, 0.1F);
                refineries.getInventory().setItem(9, new ItemStack(Material.AIR));
                dropItemStackInRefinerieDir(inputStack);
                BURNING_RUN = false;
                refineries.visualFire(false);
                burnTick = DEFAULT_BURNING;
                BACKUP_INPUT = null;
            }
        } else
        {
            BURNING_RUN = false;
            burnTick = DEFAULT_BURNING;
        }
    }

    public void dropItemStackInRefinerieDir(ItemStack stack)
    {
        Item item = Objects.requireNonNull(refineries.getFurnaceLoc().getWorld()).dropItemNaturally(refineries.getFurnaceLoc().clone().add(refineries.getFacing().getModX() + 0.5D, 0.5, refineries.getFacing().getModZ() + +0.5D), stack);
        item.setVelocity(new Vector(refineries.getFacing().getModX(), 0.2, refineries.getFacing().getModZ()).multiply(0.4D));
    }

    public void cancel()
    {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    public void pause()
    {
        PAUSE = true;
    }

    public void start()
    {
        PAUSE = false;
    }
}