package fr.bretzel.tunelsofsteel.refineries;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.HashMap;

public class RefineriesManager
{

    public static final String REFINERIES_NAME = "\u00A7f\uF808\uE002";
    public static final String REFINERIES_NETHER_WART_NAME = ChatColor.RED + "" + ChatColor.BOLD + "SUPERCONDUCTING";
    public static final String REFINERIES_BEDROCK_NAME = ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "HEAT PIPES";
    public static final String REFINERIES_MAGMA_NAME = ChatColor.DARK_RED + "" + ChatColor.BOLD + "HOT SUPERCONDUCTING";
    public static final String TALENTIUM_NAME = ChatColor.YELLOW + "" + ChatColor.BOLD + "TALENTIUM";

    private static HashMap<Location, Refineries> refineriesHashMap = new HashMap<>();

    public static void clear()
    {
        refineriesHashMap.clear();
    }

    public static Collection<Refineries> getRefineries()
    {
        return refineriesHashMap.values();
    }

    public static Refineries getRefinery(Location location)
    {
        return refineriesHashMap.values().stream().filter(refineries -> refineries.is(location)).findFirst().orElse(null);
    }

    public static Refineries getRefinery(String name)
    {
        return refineriesHashMap.values().stream().filter(refineries -> refineries.getRefineriesName().equals(name)).findFirst().orElse(null);
    }

    public static void removeRefineries(String name)
    {
        removeRefineries(getRefinery(name));
    }

    public static void removeRefineries(Location location)
    {
        removeRefineries(getRefinery(location));
    }

    public static void removeRefineries(Refineries refineries)
    {
        getRefineries().remove(refineries);
    }

    public static Refineries registerRefineries(Location location, String name)
    {
        return refineriesHashMap.put(location.clone(), new Refineries(location.clone(), name));
    }

    public static boolean isWhiteListedUpgradeItems(ItemStack stack)
    {
        for (Upgrade upgrade : Upgrade.values())
            if (upgrade.getItemUpgrade() == stack.getType())
            {
                if (stack.getItemMeta() != null)
                {
                    ItemMeta meta = stack.getItemMeta();
                    if (meta.hasDisplayName() && meta.getDisplayName().equals(upgrade.getDisplayName()))
                    {
                        return true;
                    }
                }
            }
        return false;
    }

    public static Upgrade getUpgradeFromStack(ItemStack stack)
    {
        if (stack == null)
            return Upgrade.DEFAULT;

        for (Upgrade upgrade : Upgrade.values())
            if (upgrade.getItemUpgrade() == stack.getType())
            {
                if (stack.getItemMeta() != null)
                {
                    ItemMeta meta = stack.getItemMeta();
                    if (meta.hasDisplayName() && meta.getDisplayName().equals(upgrade.getDisplayName()))
                    {
                        return upgrade;
                    }
                }
            }
        return Upgrade.DEFAULT;
    }
}
