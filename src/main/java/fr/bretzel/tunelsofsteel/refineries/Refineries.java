package fr.bretzel.tunelsofsteel.refineries;

import fr.bretzel.region.Region;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;

import java.util.ArrayList;
import java.util.List;

public class Refineries
{
    /**
     * Inventory:
     * Slot 9 = InputStack (field: burnStack)
     * Slot 17 = OutPutStack (no field, direct set to inventory)
     * Slot 22 = UpgradeStack
     */

    private Region region;
    private Location furnaceLoc;
    private BlockFace facing;
    private World world = TunnelsOfSteel.getTOSWorld();
    private Inventory inventory;
    private ArrayList<Location> burningBlocks = new ArrayList<>();
    private RefineriesTask refineriesTask;
    private String refineriesName;
    private Upgrade upgrade = Upgrade.DEFAULT;

    Refineries(Location location, String refineriesName)
    {
        this.furnaceLoc = location.clone();
        this.refineriesName = refineriesName;
        this.inventory = Bukkit.createInventory(null, 27, RefineriesManager.REFINERIES_NAME);

        try
        {
            Directional directional = (Directional) location.getBlock().getState().getData();
            facing = directional.getFacing();
        } catch (Exception e)
        {
            facing = BlockFace.NORTH;
        }


        for (int i = 0; i <= 26; i++)
        {
            if (i <= 8 || i >= 18)
            {
                if (i == 22)
                {
                    continue;
                }

                inventory.setItem(i, TOSUtils.setMarkedAsTOS(TOSUtils.generateStack(Material.BEDROCK, 1, 0, new String[]{}, RefineriesManager.REFINERIES_BEDROCK_NAME, false, ItemFlag.values())));
            } else if (i >= 10 && i <= 16)
            {
                inventory.setItem(i, TOSUtils.setMarkedAsTOS(TOSUtils.generateStack(Material.NETHER_WART_BLOCK, 1, 0, new String[]{}, RefineriesManager.REFINERIES_NETHER_WART_NAME, false, ItemFlag.values())));
            }
        }

        start();
    }

    public void visualFire(boolean b)
    {
        for (Location location : getBurningBlocks())
        {
            Material material = b ? Material.FIRE : Material.AIR;
            location.getBlock().setType(material);

        }
    }

    public ArrayList<Location> getBurningBlocks()
    {
        return burningBlocks;
    }

    public BlockFace getFacing()
    {
        return facing;
    }

    public Inventory getInventory()
    {
        return inventory;
    }

    public Location getFurnaceLoc()
    {
        return furnaceLoc;
    }

    public World getWorld()
    {
        return world;
    }

    public void start()
    {
        if (refineriesTask == null)
            refineriesTask = new RefineriesTask(this);
        else refineriesTask.start();
    }

    public Upgrade getUpgrade()
    {
        return upgrade;
    }

    public void setUpgrade(Upgrade upgrade)
    {
        this.upgrade = upgrade;
    }

    public ItemStack getInputStack()
    {
        return getInventory().getItem(9);
    }

    public void setInputStack(ItemStack inputStack)
    {
        if (inputStack != null && inputStack.getType() != Material.AIR)
            getInventory().setItem(9, inputStack);
    }

    public ItemStack getOutputStack()
    {
        return getInventory().getItem(17);
    }

    public void setOutputStack(ItemStack inputStack)
    {
        if (inputStack != null && inputStack.getType() != Material.AIR)
            getInventory().setItem(17, inputStack);
    }

    public String getRefineriesName()
    {
        return refineriesName;
    }

    public boolean containsBurningBlock(Location location)
    {
        for (Location loc : getBurningBlocks())
        {
            if (loc.getBlockX() == location.getBlockX() && loc.getBlockY() == location.getBlockY() && loc.getBlockZ() == location.getBlockZ())
                return true;
        }

        return false;
    }

    public void addBurningBlock(Location location)
    {
        getBurningBlocks().add(location);
    }

    public void removeBurningBlock(Location location)
    {
        List<Location> toRemove = new ArrayList<>();

        for (Location loc : getBurningBlocks())
        {
            if (loc.getBlockX() == location.getBlockX() && loc.getBlockY() == location.getBlockY() && loc.getBlockZ() == location.getBlockZ())
                toRemove.add(loc);
        }

        getBurningBlocks().removeAll(toRemove);
    }

    public void stop()
    {
        refineriesTask.cancel();
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public boolean is(Location location) {
        return (getFurnaceLoc().getBlockX() == location.getBlockX() &&
                getFurnaceLoc().getBlockY() == location.getBlockY() &&
                getFurnaceLoc().getBlockZ() == location.getBlockZ());
    }
}
