package fr.bretzel.tunelsofsteel.refineries;


import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;

public enum Upgrade
{
    DEFAULT(Material.AIR, ""),
    BRIGHT_FIRE(Material.YELLOW_GLAZED_TERRACOTTA, ChatColor.GOLD + "" + ChatColor.BOLD + "BRIGHT FIRE"),
    SOFT_FIRE(Material.BLACK_GLAZED_TERRACOTTA, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "SOFT FIRE"),
    ALCHEMIST(Material.RED_GLAZED_TERRACOTTA, ChatColor.DARK_RED + "" + ChatColor.BOLD + "ALCHEMIST"),
    PRIVATE(Material.MAGENTA_GLAZED_TERRACOTTA, ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "PRIVATE");

    private Material itemUpgrade;
    private String displayName;

    Upgrade(Material block, String displayName)
    {
        this.displayName = displayName;
        this.itemUpgrade = block;
    }

    public String getDisplayName()
    {
        return displayName + " UPGRADE";
    }

    public Material getItemUpgrade()
    {
        return itemUpgrade;
    }

    public ItemStack getPrivateItemStack(TOSTeam owner)
    {
        if (this == PRIVATE)
        {
            ItemStack upgradeStack = new ItemStack(itemUpgrade);
            ItemMeta meta = upgradeStack.getItemMeta();
            meta.setDisplayName(displayName);
            meta.addItemFlags(ItemFlag.values());
            CustomItemTagContainer tagContainer = meta.getCustomTagContainer();
            NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "UpgradeTeamOwner");
            tagContainer.setCustomTag(namespacedKey, ItemTagType.STRING, owner.getName());
            upgradeStack.setItemMeta(meta);
            return upgradeStack;
        } else
        {
            return new ItemStack(Material.AIR);
        }
    }
}
