package fr.bretzel.tunelsofsteel.scoreboard;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.ColorScroller;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LobbyScoreboard
{
    private static final ColorScroller scroller = new ColorScroller(ChatColor.AQUA, "TunnelsOfSteel²", ChatColor.WHITE.toString(), ChatColor.DARK_AQUA.toString(), ChatColor.DARK_AQUA.toString(), true, false, ColorScroller.ScrollType.FORWARD);

    private static final String OBJECTIVE_NAME = "TOS_Welcome";

    private static Map<Player, Objective> playerList = new HashMap<>();
    private static BukkitTask bukkitTask;

    public static void actualiseLobbyScoreboard(Player player)
    {
        playerList.remove(player);

        Scoreboard scoreboard = newScoreboardForPlayer(player);
        player.setScoreboard(scoreboard);

        playerList.put(player, scoreboard.getObjective(OBJECTIVE_NAME));

        //Init the scroller display name
        if (bukkitTask == null)
        {
            bukkitTask = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, () ->
            {
                if (playerList.size() <= 0)
                    return;

                if (scroller.getScrollType() == ColorScroller.ScrollType.FORWARD)
                {
                    if (scroller.getPosition() >= scroller.getString().length())
                    {
                        scroller.setScrollType(ColorScroller.ScrollType.BACKWARD);
                    }
                } else
                {
                    if (scroller.getPosition() <= -1)
                    {
                        scroller.setScrollType(ColorScroller.ScrollType.FORWARD);
                    }
                }

                String display = scroller.next();

                List<Player> playersToRemove = new ArrayList<>();

                for (Map.Entry<Player, Objective> entry : playerList.entrySet())
                {
                    Scoreboard sc = entry.getKey().getScoreboard();

                    if (!player.isOnline() || sc.getObjective(OBJECTIVE_NAME) == null) {
                        playersToRemove.add(entry.getKey());
                    } else {
                        entry.getValue().setDisplayName(display);
                    }
                }

                for (Player p : playersToRemove)
                    playerList.remove(p);

            }, 20L, 2L);
        }
    }

    private static Scoreboard newScoreboardForPlayer(Player player)
    {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective objective = scoreboard.registerNewObjective(OBJECTIVE_NAME, "dummy", "Tunnels Of Steel");

        objective.setDisplayName(scroller.getString());

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.getScore("§r").setScore(-1);
        objective.getScore("§r§r").setScore(-2);

        objective.getScore(ChatColor.translateAlternateColorCodes('&', "    &bWelcome &a" + player.getName() + "      &r")).setScore(-3);

        objective.getScore("§r§r§r").setScore(-4);

        objective.getScore(ChatColor.translateAlternateColorCodes('&', "         &bYour Team &r      &r")).setScore(-5);

        TOSTeam team = TeamManager.getTeamOfPlayer(player);

        String string = team == null ? ChatColor.RED + "None" : team.getColor().getChatColor() + team.getName();

        objective.getScore(ChatColor.translateAlternateColorCodes('&', "&r      &r      &r" + string)).setScore(-6);

        objective.getScore("§r§r§r§r§r").setScore(-7);
        objective.getScore("§r§r§r§r§r§r").setScore(-8);

        return scoreboard;
    }
}
