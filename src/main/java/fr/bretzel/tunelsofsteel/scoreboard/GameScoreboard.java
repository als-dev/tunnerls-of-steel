package fr.bretzel.tunelsofsteel.scoreboard;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.game.Statue;
import fr.bretzel.tunelsofsteel.refineries.RefineriesManager;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

public class GameScoreboard implements Runnable {
    private static String OBJECTIVE_NAME = "TOS_MAIN";

    private HashMap<TOSTeam, Integer> talentiumBackup = new HashMap<>();

    private int tick = 0;

    private int maxSecond;

    private NumberFormat format = new DecimalFormat("00");

    /**
     * ---------------5
     * Timer:         4
     * ----------     3
     * TALENTIUM:     2
     * LE NOMBRE      1
     * -------------- 0
     */

    public GameScoreboard() {
        Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
        maxSecond = (30 * 60) + ((60) * 60);
    }

    @Override
    public void run() {
        if (TunnelsOfSteel.STATUE == Statue.START)
        {
            if (tick >= 20 && maxSecond >= 0) {
                tick = 0;

                for (Player player : Bukkit.getOnlinePlayers()) {
                    TOSTeam team = TeamManager.getTeamOfPlayer(player);

                    if (team != null && team.getScoreboard() != null) {
                        if (!talentiumBackup.containsKey(team))
                            talentiumBackup.put(team, team.getTalentium());

                        Scoreboard scoreboard = team.getScoreboard();

                        checkScoreboard(team);

                        if (player.isOnline() && player.getScoreboard() != scoreboard) {
                            player.setScoreboard(scoreboard);
                        }

                        if (team.getTalentium() != talentiumBackup.get(team)) {
                            Objective objective = scoreboard.getObjective(OBJECTIVE_NAME);

                            if (objective != null) {
                                Score oldScore = objective.getScore(RefineriesManager.TALENTIUM_NAME + "§r: " + team.getColor() + team.getName() + "§l" + talentiumBackup.get(team));
                                objective.getScore(RefineriesManager.TALENTIUM_NAME + "§r: " + team.getColor() + team.getName() + "§l" + team.getTalentium()).setScore(1);

                                scoreboard.resetScores(oldScore.getEntry());

                                talentiumBackup.replace(team, team.getTalentium());
                            }
                        }
                    }
                }

                //Update Timer Score
                for (TOSTeam tosTeam : TeamManager.getRegisteredTeam()) {
                    Scoreboard scoreboard = tosTeam.getScoreboard();
                    checkScoreboard(tosTeam);
                    scoreboard.resetScores(formatTimer(maxSecond));
                }

                //Starting Timer
                maxSecond--;

                //Update Timer Score
                for (TOSTeam tosTeam : TeamManager.getRegisteredTeam()) {
                    Scoreboard scoreboard = tosTeam.getScoreboard();
                    checkScoreboard(tosTeam);
                    Objective objective = scoreboard.getObjective(OBJECTIVE_NAME);

                    if (objective != null) {
                        Score score = objective.getScore(formatTimer(maxSecond));
                        score.setScore(4);
                    }
                }
            }

            tick++;
        }
    }

    /*

    //TODO IS the end Task !
                {

                    TunnelsOfSteel.STATUE = Statue.END;

                    Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

                    Objective objective = scoreboard.registerNewObjective("FinalScore", "dummy");
                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                    objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&b~ &fTunnels§bOf§fSteel §b~"));

                    TOSTeam winingTeam = null;

                    int backupWining = -1;

                    for (TOSTeam team : TeamManager.getRegisteredTeam())
                    {
                        int value = TunnelsOfSteel.getScore(team);

                        objective.getScore(team.getColor() + team.getName() + ChatColor.RESET).setScore(value);

                        if (backupWining < value)
                        {
                            backupWining = value;
                            winingTeam = team;
                        }
                    }

                    for (Player player : Bukkit.getOnlinePlayers())
                    {
                        player.setScoreboard(scoreboard);

                        player.setAllowFlight(true);
                        player.setFlying(true);

                        player.teleport(new Location(player.getWorld(), 0, 47, 0));

                        TOSTeam pt = TeamManager.getTeamOfPlayer(player);

                        TitleAPI.sendTitle(player, 20, 40, 20, ChatColor.AQUA + "Team " + winingTeam.getColor() + winingTeam.getName() + " has win !");

                        player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_DEATH, 0.3F, 1.3F);

                        if (pt != null && pt.getName().equals(winingTeam.getName()))
                        {
                            for (int i = 0; i <= player.getInventory().getSize(); i++)
                            {
                                player.getInventory().setItem(i, new ItemStack(Material.DIAMOND, 64));
                            }
                        } else
                        {
                            for (int i = 0; i <= player.getInventory().getSize(); i++)
                            {
                                player.getInventory().setItem(i, new ItemStack(Material.DIRT, 64, (short) 2));
                            }
                        }
                    }

                    new ClearGame();

                    HashMap<TOSTeam, ArrayList<Location>> blMap = BlockEvent.getBlockPosed();

                    for (TOSTeam team : blMap.keySet())
                    {
                        new EndSpawnBlock(team, blMap.get(team));
                    }

                    Bukkit.getScheduler().cancelTask(taskID);
                    return;
                }
     */


    private void clearAllEntry() {
        TeamManager.getRegisteredTeam().forEach(tosTeam -> tosTeam.getScoreboard().getEntries().forEach(s -> tosTeam.getScoreboard().resetScores(s)));
    }

    public void setMaxSecond(int maxSecond) {
        clearAllEntry();
        this.maxSecond = maxSecond;
    }

    public int getCurrentMaxSecond() {
        return maxSecond;
    }

    private void checkScoreboard(TOSTeam tosTeam) {
        Scoreboard scoreboard = tosTeam.getScoreboard();

        Objective objective = scoreboard.getObjective(OBJECTIVE_NAME);

        if (objective == null)
            objective = scoreboard.registerNewObjective(OBJECTIVE_NAME, "dummy", ChatColor.translateAlternateColorCodes('&', "&b~ &fTunnels&bOf&fSteel&b&l² &b~"));

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public String formatTimer(int seconds) {
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;

        seconds -= minutes * 60;
        minutes -= hours * 60;
        hours -= days * 24;

        return ChatColor.translateAlternateColorCodes('&', "&r      &f" + format.format(hours) + "&b&l:&f" + format.format(minutes) + "&b&l:&f" + format.format(seconds) + "    &r");
    }
}
