package fr.bretzel.tunelsofsteel.task;

import fr.bretzel.region.Region;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

public class ClearLiner implements Runnable
{
    private int y, x, z, taskID;
    private boolean isFinish = false;
    private final int dX;
    private final int dZ;
    private Region region;

    public ClearLiner()
    {
        this.taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
        this.region = TunnelsOfSteel.GAME_REGION;

        this.y = region.getMaxLocation().getBlockY();

        this.x = region.getMaxLocation().getBlockX();
        this.z = region.getMaxLocation().getBlockZ();

        this.dX = -x;
        this.dZ = -z;
    }

    @Override
    public void run()
    {
        if (isFinish)
            return;

        int i = 0;

        while (i < 10)
        {
            i++;

            replaceBlock();
        }
    }

    private void replaceBlock()
    {
        Location location = checkLocation();

        if (location != null)
        {
            System.out.println(location.toString());
            location.getBlock().setType(Material.AIR);
        } else
        {
            Bukkit.getScheduler().cancelTask(taskID);
            System.out.println("Finish for task: " + taskID);
        }
    }

    private Location checkLocation()
    {
        boolean xOk = x >= region.getMinLocation().getBlockX();
        boolean zOk = z >= region.getMinLocation().getBlockZ();

        Location location = new Location(TunnelsOfSteel.getTOSWorld(), x, y, z);

        if (xOk) {
            x--;
        } else {
            x = region.getMaxLocation().getBlockX();

            if (zOk)
                z--;
        }

        if (x <= dX && z <= dZ)
        {
            System.out.println("Finish for task: " + taskID);
            this.isFinish = true;
            Bukkit.getScheduler().cancelTask(taskID);
            return null;
        }

        return location;
    }

    public boolean isFinish()
    {
        return isFinish;
    }
}
