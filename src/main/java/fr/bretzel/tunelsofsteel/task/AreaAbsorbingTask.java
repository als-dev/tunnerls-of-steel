package fr.bretzel.tunelsofsteel.task;


import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;

public class AreaAbsorbingTask implements Runnable
{
    public AreaAbsorbingTask()
    {
        Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();
    }

    @Override
    public void run()
    {
        if (AddPlayersScoreToTeam.getActiveDepositeTeam().size() > 0)
        {
            for (TOSTeam team : AddPlayersScoreToTeam.getActiveDepositeTeam())
            {
                AbsorbingRegion region = team.getAbsorbingRegion();

                if (region != null)
                {
                    double middleX = region.getMaxLocation().getX() / region.getMinLocation().getX();
                    double middleZ = region.getMaxLocation().getZ() / region.getMinLocation().getZ();
                    region.getOwnerWorld().spawnParticle(Particle.PORTAL,
                                    new Location(region.getOwnerWorld(), middleX, region.getMinLocation().getY() + 0.1D, middleZ),
                                    1, 0.1, 0, 0.1, null);
                }
            }
        }
    }
}
