package fr.bretzel.tunelsofsteel.task;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClearGame implements Runnable
{
    private int y = TunnelsOfSteel.GAME_REGION.getMaxLocation().getBlockY();

    private int taskID;
    private LinkedList<ClearLiner> clearLiners = new LinkedList<>();

    public ClearGame()
    {
        taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 20).getTaskId();
    }

    @Override
    public void run()
    {
        if (y <= TunnelsOfSteel.GAME_REGION.getMinLocation().getBlockY()) {
            AtomicBoolean isAllFinish = new AtomicBoolean(true);

            clearLiners.forEach(clearLiner ->
            {
                if (!clearLiner.isFinish())
                    isAllFinish.set(false);
            });

            if (isAllFinish.get()) {
                clearLiners.clear();
                Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () -> EndSpawnBlock.canStartBlockPlace = true, 20);
                Bukkit.getScheduler().cancelTask(taskID);
                Bukkit.broadcastMessage(ChatColor.RED + "[DEBUG] Clear Game Done. All ClearLiner has finished.");
            }
        } else
        {
            clearLiners.add(new ClearLiner());
            y--;
        }
    }
}
