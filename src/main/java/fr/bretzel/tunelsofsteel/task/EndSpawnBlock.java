package fr.bretzel.tunelsofsteel.task;

import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.List;

public class EndSpawnBlock implements Runnable
{

    public static boolean canStartBlockPlace = false;

    private List<Location> locations;
    private TOSTeam owner;
    private int index = -1;
    private int taskID;

    public EndSpawnBlock(TOSTeam team, List<Location> locations)
    {
        this.owner = team;
        this.locations = locations;

        this.taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1).getTaskId();

        this.index = locations.size() - 1;
    }



    @Override
    public void run()
    {
        if (canStartBlockPlace)
        {
            if (index < 0)
            {
                Bukkit.getScheduler().cancelTask(taskID);
                return;
            }

            locations.get(index).getBlock().setType(owner.getDepositBotomMaterial());

            index--;
        }
    }

}
