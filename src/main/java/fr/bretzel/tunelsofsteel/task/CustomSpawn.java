package fr.bretzel.tunelsofsteel.task;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.Random;

public class CustomSpawn implements Listener {
    public CustomSpawn() {
        //Bukkit.getServer().getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 3 * 20).getTaskId();
        Bukkit.getPluginManager().registerEvents(this, TunnelsOfSteel.INSTANCE);
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Bat || event.getEntity() instanceof Slime) {
            event.setCancelled(true);
            event.getEntity().remove();
        }
    }


    /*
    Test
                for (Player player : Bukkit.getOnlinePlayers())
            {
                if (player.getGameMode() != GameMode.SPECTATOR && TunnelsOfSteel.GAME_REGION.contains(player.getLocation()))
                {
                    Location origin = player.getLocation();
                    World world = TunnelsOfSteel.GAME_REGION.getOwnerWorld();

                    int entityToSpawn = new Random().nextInt(2) + 1;

                    int cx = origin.getBlockX();
                    int cy = origin.getBlockY();
                    int cz = origin.getBlockZ();

                    for (int z = cz - checkAroundPlayer; z <= cz + checkAroundPlayer; z++)
                    {
                        for (int y = cy - checkAroundPlayer; y <= cy + checkAroundPlayer; y++)
                        {
                            for (int x = cx - checkAroundPlayer; x <= cx + checkAroundPlayer; x++)
                            {
                                double distance = ((cx - x) * (cx - x) + ((cz - z) * (cz - z)) + ((cy - y) * (cy - y)));
                                Location location = new Location(world, x, y, z);

                                if (distance < checkAroundPlayer * checkAroundPlayer && location.distance(origin) > secureDistance && location.getBlock().getType().isAir()
                                        && canSpawnAt(location) && entityToSpawn >= 0)
                                {
                                    entityToSpawn--;
                                    Bukkit.getServer().getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
                                    {
                                        location.setY(location.getBlockY() + 0.2);
                                        location.setZ(location.getBlockZ() + 0.5);
                                        location.setX(location.getBlockX() + 0.5);

                                        LivingEntity monster = spawnRandomEntity(location);

                                        if (monster != null)
                                            spawnedEntity.add(monster);
                                    }, 10 + (entityToSpawn * -1));
                                }
                            }
                        }
                    }
                }
            }
     */

    private EntityType getRandomEntityType() {
        EntityType entityType = EntityType.values()[new Random().nextInt(EntityType.values().length)];
        Class<? extends Entity> entityClass = entityType.getEntityClass();

        try {
            if (!(entityClass != null && Monster.class.isAssignableFrom(entityClass) && !Boss.class.isAssignableFrom(entityClass)) || entityType == EntityType.ELDER_GUARDIAN)
                return getRandomEntityType();
        } catch (Exception ignored) {
        }


        return entityType;
    }

    private LivingEntity spawnRandomEntity(Location location) {
        EntityType entityType = getRandomEntityType();

        World world = location.getWorld();
        Monster monster = null;

        if (world != null && entityType.getEntityClass() != null) {
            monster = (Monster) world.spawn(location, entityType.getEntityClass());
            monster.setAI(false);
            //monster.setCustomName("Entity n°: " + spawnedEntity.size() + 1);
            Bukkit.broadcastMessage("Spawned a new entity (" + monster.getType().name() + ") !!!");
            monster.setCustomNameVisible(true);
        }

        return monster;
    }

    /*private boolean canSpawnAt(Location location)
    {
        Block block = location.getBlock();
        World world = block.getWorld();
        int lightLevel = block.getLightLevel();
        int nearbyMob = world.getNearbyEntities(location, checkAroundSpawnAt, checkAroundSpawnAt, checkAroundSpawnAt).size();
        boolean isTopAirBlock = block.getRelative(BlockFace.UP).getType().isAir();
        boolean isBotSolidBlock = block.getRelative(BlockFace.DOWN).getType().isSolid();

        return lightLevel < canSpawnLightLevel && nearbyMob < maxMobAroundSpawnAt && isTopAirBlock && isBotSolidBlock;
    }*/
}
