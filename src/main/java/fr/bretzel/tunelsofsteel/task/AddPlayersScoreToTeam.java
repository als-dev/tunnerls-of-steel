package fr.bretzel.tunelsofsteel.task;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class AddPlayersScoreToTeam implements Runnable
{
    private static HashMap<UUID, AddPlayersScoreToTeam> containsPlayer = new HashMap<>();

    private int taskID;
    private Player player;
    private TOSTeam team;
    private TOSPlayer tosPlayer;

    public AddPlayersScoreToTeam(Player player, TOSTeam team)
    {
        this.player = player;
        this.tosPlayer = TOSPlayer.getPlayer(player);
        this.team = team;

        containsPlayer.put(player.getUniqueId(), this);

        this.taskID = Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, (long) (0.5 * 20)).getTaskId();
    }

    public static boolean hasPlayer(Player player)
    {
        return containsPlayer.containsKey(player.getUniqueId());
    }

    public static Collection<TOSTeam> getActiveDepositeTeam()
    {
        List<TOSTeam> teams = new ArrayList<>();

        for (AddPlayersScoreToTeam addPlayersScoreToTeam : containsPlayer.values())
        {
            if (!teams.contains(addPlayersScoreToTeam.team))
            {
                teams.add(addPlayersScoreToTeam.team);
            }
        }

        return teams;
    }

    @Override
    public void run()
    {
        AbsorbingRegion region = team.getAbsorbingRegion();
        Block blockUnderPlayer = TOSUtils.getBlockUnderPlayer(player);

        if (player.isOnline() && region != null && region.contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()) &&
                blockUnderPlayer != null && blockUnderPlayer.getType() == team.getDepositBotomMaterial() && TOSUtils.hasTalentiumInInventory(player))
        {
            Bukkit.broadcastMessage("Hello WAZAAAAAAA 1");
            ItemStack talentium = TOSUtils.getFirstTalentium(player);

            if (talentium != null)
            {
                Bukkit.broadcastMessage("Hello WAZAAAAAAA 2");
                tosPlayer.addTalentiumDeposed(1);
                team.addTalentium(1);
                talentium.setAmount(talentium.getAmount() - 1);
                player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 0.1F, 2F);
            }
        } else
        {
            cancel();
        }
    }

    private void cancel()
    {
        containsPlayer.remove(player.getUniqueId());
        Bukkit.getScheduler().cancelTask(taskID);
    }
}
