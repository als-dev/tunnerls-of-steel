package fr.bretzel.tunelsofsteel.player;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.player.actionbar.ActionBarMessage;
import fr.bretzel.tunelsofsteel.player.actionbar.ActionBarTask;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

public class TOSPlayer {
    private static HashMap<UUID, TOSPlayer> playersMap = new HashMap<>();
    private static ActionBarTask actionBarTask;

    private File PLAYER_FILE;
    private YamlConfiguration PLAYER_CONFIG;

    private long lastRoolBack = System.nanoTime();

    private boolean immunized = false;
    private boolean isTarget = true;

    private UUID uuid;
    private int kill;
    private int death;
    private int oreMined;
    private int talentiumDeposed;
    private int potentialTalentiumMined;
    private double playerDamaged;
    private double playerHealRegen;
    private List<ActionBarMessage> actionBarList = new ArrayList<>();
    private TOSTeam team;

    protected TOSPlayer(UUID uuid) {
        if (actionBarTask == null)
            actionBarTask = new ActionBarTask();

        File dataLocation = new File(TunnelsOfSteel.INSTANCE.getDataFolder() + "/players/");

        if (!dataLocation.exists() && dataLocation.mkdirs())
            Logger.log(ChatColor.RED + "Successfully created directory " + dataLocation.toString());

        PLAYER_FILE = new File(dataLocation, uuid.toString() + ".yml");

        try {
            if (!PLAYER_FILE.exists() && PLAYER_FILE.createNewFile())
                Logger.log(ChatColor.RED + "Successfully created file " + PLAYER_FILE.toString());
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        PLAYER_CONFIG = YamlConfiguration.loadConfiguration(PLAYER_FILE);
        this.uuid = uuid;

        load();
    }

    public static TOSPlayer getPlayer(Player player)
    {
        return getPlayer(player.getUniqueId());
    }

    public static TOSPlayer getPlayer(OfflinePlayer player)
    {
        return getPlayer(player.getUniqueId());
    }

    public static TOSPlayer getPlayer(UUID uuid) {
        OfflinePlayer offlinePlayer = TOSUtils.getOfflinePlayer(uuid);

        if (offlinePlayer == null) {
            Logger.log("Error, the UUID: " + uuid.toString() + " is not a Player UUID");
            return null;
        }

        if (playersMap.containsKey(uuid) && playersMap.get(uuid) != null)
            return playersMap.get(uuid);


        TOSPlayer player = new TOSPlayer(uuid);
        Logger.log("Successfully register TOSPlayer: " + offlinePlayer.getName());
        playersMap.put(uuid, player);
        return player;
    }

    public static Collection<TOSPlayer> getPlayers() {
        return playersMap.values();
    }

    public static boolean contains(Player player) {
        return contains(player.getUniqueId());
    }

    public static boolean contains(UUID uuid) {
        return playersMap.containsKey(uuid);
    }

    public static void saveAllAndClear()
    {
        for (TOSPlayer player : playersMap.values())
        {
            try
            {
                player.save();
            } catch (Exception e)
            {
                Logger.log(ChatColor.RED + "Error cannot save player: [uuid:" + player.getPlayerID().toString() + ", name:" + player.getPlayer().getName() + "]");
            }
        }

        playersMap.clear();
    }

    public YamlConfiguration getPlayerConfig()
    {
        return PLAYER_CONFIG;
    }

    public UUID getPlayerID()
    {
        return uuid;
    }

    public Player getPlayer()
    {
        return Bukkit.getPlayer(getPlayerID());
    }

    public String getName()
    {
        return TOSUtils.getOfflinePlayer(getPlayerID()).getName();
    }

    public TOSTeam getTeam()
    {
        return team;
    }

    public void setTeam(TOSTeam team) {
        if (team != null)
            this.team = team;
    }

    public int getDeath()
    {
        return death;
    }

    public void setDeath(int death)
    {
        this.death = death;
    }

    public void addDeath(int death)
    {
        setDeath(getDeath() + death);
    }

    public int getKill()
    {
        return kill;
    }

    public void setKill(int kill)
    {
        this.kill = kill;
    }

    public void addKill(int kill)
    {
        setKill(getKill() + kill);
    }

    public int getOreMined()
    {
        return oreMined;
    }

    public void setOreMined(int oreMined)
    {
        this.oreMined = oreMined;
    }

    public void addOreMined(int oreMined)
    {
        setOreMined(getOreMined() + oreMined);
    }

    public double getPlayerDamaged()
    {
        return playerDamaged;
    }

    public void setPlayerDamaged(double playerDamaged)
    {
        this.playerDamaged = playerDamaged;
    }

    public void addPlayerDamaged(double playerDamaged)
    {
        setPlayerDamaged(getPlayerDamaged() + playerDamaged);
    }

    public int getTalentiumDeposed()
    {
        return talentiumDeposed;
    }

    public void setTalentiumDeposed(int talentiumDeposed)
    {
        this.talentiumDeposed = talentiumDeposed;
    }

    public void addTalentiumDeposed(int talentiumDeposed)
    {
        setTalentiumDeposed(getTalentiumDeposed() + talentiumDeposed);
    }

    public int getPotentialTalentiumMined()
    {
        return potentialTalentiumMined;
    }

    public void setPotentialTalentiumMined(int potentialTalentiumMined) {
        this.potentialTalentiumMined = potentialTalentiumMined;
    }

    public void addPotentialTalentiumMined(int potentialTalentiumMined) {
        setTalentiumDeposed(getPotentialTalentiumMined() + potentialTalentiumMined);
    }

    public boolean isImmunized() {
        return immunized;
    }

    public void setImmunized(boolean immunized) {
        this.immunized = immunized;
    }

    public double getPlayerHealRegen() {
        return playerHealRegen;
    }

    public void setPlayerHealRegen(double playerHealRegen) {
        this.playerHealRegen = playerHealRegen;
    }

    public void addPlayerHealRegen(double playerHealRegen) {
        setPlayerHealRegen(getPlayerHealRegen() + playerHealRegen);
    }

    public long getLastRoolBack() {
        return lastRoolBack;
    }

    public void setLastRoolBack(long lastRoolBack) {
        this.lastRoolBack = lastRoolBack;
    }

    public boolean cantTarget() {
        return isTarget;
    }

    public void setTarget(boolean target) {
        isTarget = target;
    }

    public void sendActionBar(String message, double duration, Predicate<TOSPlayer> test) {
        actionBarList.add(new ActionBarMessage(message, duration, test));
        actionBarTask.run();
    }

    public void sendActionBar(String message, double duration) {
        sendActionBar(message, duration, tosPlayer -> false);
    }

    public boolean hasActionBarMessage() {
        return !actionBarList.isEmpty();
    }

    public List<ActionBarMessage> getActionBarList() {
        return actionBarList;
    }

    private void load() {
        if (PLAYER_CONFIG.contains("team"))
            setTeam(TeamManager.getTeam(Color.match(PLAYER_CONFIG.getString("team"))));
    }

    private void save() throws IOException {
        if (getTeam() != null)
            PLAYER_CONFIG.set("team", getTeam().getColor().name());

        PLAYER_CONFIG.save(PLAYER_FILE);
    }
}
