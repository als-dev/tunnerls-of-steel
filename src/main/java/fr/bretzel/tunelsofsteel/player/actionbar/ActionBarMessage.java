package fr.bretzel.tunelsofsteel.player.actionbar;

import fr.bretzel.tunelsofsteel.player.TOSPlayer;

import java.util.function.Predicate;

public class ActionBarMessage {
    long startDisplayTime = -1L;
    private String message;
    private Double duration;
    private Predicate<TOSPlayer> test;

    public ActionBarMessage(String message, double duration, Predicate<TOSPlayer> test) {
        this.message = message;
        this.duration = duration;
        this.test = test;
    }

    public Double getDuration() {
        return duration;
    }

    public String getMessage() {
        return message;
    }

    public Predicate<TOSPlayer> getTest() {
        return test;
    }
}
