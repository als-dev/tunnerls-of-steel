package fr.bretzel.tunelsofsteel.player.actionbar;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ActionBarTask implements Runnable {

    private String APPEND_STRING_SPACE = ChatColor.DARK_GRAY.toString() + " | " + ChatColor.RESET.toString();

    public ActionBarTask() {
        Bukkit.getScheduler().runTaskTimer(TunnelsOfSteel.INSTANCE, this, 0, 1L);
    }

    @Override
    public void run() {
        for (TOSPlayer tosPlayer : TOSPlayer.getPlayers()) {
            if (tosPlayer.hasActionBarMessage()) {
                List<ActionBarMessage> toRemove = new ArrayList<>();
                Player player = tosPlayer.getPlayer();
                StringBuilder builder = new StringBuilder();

                for (ActionBarMessage actionBarMessage : tosPlayer.getActionBarList()) {
                    String message = actionBarMessage.getMessage();
                    double duration = actionBarMessage.getDuration();

                    if (player != null && player.isOnline()) {
                        if (actionBarMessage.startDisplayTime == -1L)
                            actionBarMessage.startDisplayTime = System.nanoTime();

                        boolean timeOut = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - actionBarMessage.startDisplayTime) >= duration;

                        if (timeOut || actionBarMessage.getTest().test(tosPlayer))
                            toRemove.add(actionBarMessage);
                        else
                            builder.append(message).append(APPEND_STRING_SPACE);
                    }
                }

                if (builder.length() > 0 && player != null && player.isOnline()) {
                    String message = builder.toString();
                    ActionBarAPI.sendActionBar(player, message.substring(0, message.length() - APPEND_STRING_SPACE.length()));
                }

                tosPlayer.getActionBarList().removeAll(toRemove);
            }
        }
    }
}
