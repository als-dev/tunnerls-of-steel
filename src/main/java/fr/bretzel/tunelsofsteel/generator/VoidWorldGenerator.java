package fr.bretzel.tunelsofsteel.generator;

import fr.bretzel.hologram.Hologram;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class VoidWorldGenerator extends ChunkGenerator
{

    @Override
    public ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ, BiomeGrid biome)
    {
        ChunkData chunkData = createChunkData(world);

        for (int x = 0; x < 16; x++)
        {
            for (int z = 0; z < 16; z++)
            {
                biome.setBiome(x, z, getVoidBiomes());
            }
        }

        //Purple color
        if (chunkX == 0 && chunkZ == 0)
        {
            chunkData.setRegion(0, 252, 0, 16, 253, 16, Material.WHITE_CONCRETE);

            chunkData.setRegion(0, 253, 0, 16, 256, 16, Material.WHITE_STAINED_GLASS);
            chunkData.setRegion(1, 253, 1, 16, 256, 16, Material.PURPLE_STAINED_GLASS);
            chunkData.setRegion(0, 253, 0, 15, 256, 15, Material.VOID_AIR);

            chunkData.setRegion(1, 252, 1, 16, 253, 16, Material.PURPLE_CONCRETE);

            chunkData.setBlock(0, 252, 0, Material.LIGHT_GRAY_CONCRETE);

            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
            {
                Hologram hologram = TunnelsOfSteel.HOLOGRAM_MANAGER.addHologram(new Location(world, chunkX * 16 + 8, 254, chunkZ * 16 + 8),
                        ChatColor.DARK_PURPLE + "Purple Team");

                hologram.setVisible(true);

                Hologram dedicated = TunnelsOfSteel.HOLOGRAM_MANAGER.addHologram(new Location(world, chunkX * 16, 254D, chunkZ * 16),
                        ChatColor.GOLD + "Made by MrBretzel and designed by Artylight",
                        ChatColor.translateAlternateColorCodes('&', "&b~ &fTunnels§bOf§fSteel ²§b~")
                );

                dedicated.setVisible(true);
            }, 20 * 2);

            //Orange color
        } else if (chunkX == -1 && chunkZ == 0)
        {
            chunkData.setRegion(0, 252, 0, 16, 253, 16, Material.WHITE_CONCRETE);

            chunkData.setRegion(0, 253, 0, 16, 256, 16, Material.WHITE_STAINED_GLASS);
            chunkData.setRegion(0, 253, 1, 15, 256, 16, Material.ORANGE_STAINED_GLASS);
            chunkData.setRegion(1, 253, 0, 16, 256, 15, Material.VOID_AIR);

            chunkData.setRegion(0, 252, 1, 15, 253, 16, Material.ORANGE_CONCRETE);

            chunkData.setBlock(15, 252, 0, Material.LIGHT_GRAY_CONCRETE);

            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
            {
                Hologram hologram = TunnelsOfSteel.HOLOGRAM_MANAGER.addHologram(new Location(world, chunkX * 16 + 8, 254, chunkZ * 16 + 8),
                        ChatColor.GOLD + "Orange Team");

                hologram.setVisible(true);


            }, 20 * 2);

            //Blue color
        } else if (chunkX == -1 && chunkZ == -1)
        {
            chunkData.setRegion(0, 252, 0, 16, 253, 16, Material.WHITE_CONCRETE);

            chunkData.setRegion(0, 253, 0, 16, 256, 16, Material.WHITE_STAINED_GLASS);
            chunkData.setRegion(0, 253, 0, 15, 256, 15, Material.LIGHT_BLUE_STAINED_GLASS);
            chunkData.setRegion(1, 253, 1, 16, 256, 16, Material.VOID_AIR);

            chunkData.setRegion(0, 252, 0, 15, 253, 15, Material.LIGHT_BLUE_CONCRETE);

            chunkData.setBlock(15, 252, 15, Material.LIGHT_GRAY_CONCRETE);

            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
            {
                Hologram hologram = TunnelsOfSteel.HOLOGRAM_MANAGER.addHologram(new Location(world, chunkX * 16 + 8, 254, chunkZ * 16 + 8),
                        ChatColor.AQUA + "Blue Team");

                hologram.setVisible(true);


            }, 20 * 2);

            //Lime color
        } else if (chunkX == 0 && chunkZ == -1)
        {
            chunkData.setRegion(0, 252, 0, 16, 253, 16, Material.WHITE_CONCRETE);

            chunkData.setRegion(0, 253, 0, 16, 256, 16, Material.WHITE_STAINED_GLASS);
            chunkData.setRegion(1, 253, 0, 16, 256, 15, Material.LIME_STAINED_GLASS);
            chunkData.setRegion(0, 253, 1, 15, 256, 16, Material.VOID_AIR);

            chunkData.setRegion(1, 252, 0, 16, 253, 15, Material.LIME_CONCRETE);

            chunkData.setBlock(0, 252, 15, Material.LIGHT_GRAY_CONCRETE);

            Bukkit.getScheduler().runTaskLater(TunnelsOfSteel.INSTANCE, () ->
            {
                Hologram hologram = TunnelsOfSteel.HOLOGRAM_MANAGER.addHologram(new Location(world, chunkX * 16 + 8, 254, chunkZ * 16 + 8),
                        ChatColor.GREEN + "Green Team");

                hologram.setVisible(true);


            }, 20 * 2);
        }

        return chunkData;
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random)
    {
        return new Location(world, 0D, 150D, 0D);
    }

    public Biome getVoidBiomes()
    {
        return Biome.PLAINS;
    }

    /**
     * From org.apache.commons.lang3.StringUtils.containsIgnoreCase(CharSequence str,
     * CharSequence searchStr);
     */
    private boolean containsIgnoreCase(String str, String searchStr)
    {
        if (str != null && searchStr != null)
        {
            int len = searchStr.length();
            int max = str.length() - len;

            for (int i = 0; i <= max; ++i)
            {
                if (str.regionMatches(true, i, searchStr, 0, len))
                {
                    return true;
                }
            }
            return false;
        } else
        {
            return false;
        }
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        world.getPopulators().addAll(Arrays.asList(new ArenaPopulator(), new SpawnPopulator()));
        return world.getPopulators();
    }
}
