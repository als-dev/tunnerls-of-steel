package fr.bretzel.tunelsofsteel.generator;

import fr.bretzel.hologram.Hologram;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import org.bukkit.*;
import org.bukkit.generator.BlockPopulator;

import java.util.Random;

public class SpawnPopulator extends BlockPopulator
{
    @Override
    public void populate(World world, Random random, Chunk chunk)
    {
        if (chunk.getX() == 0 && chunk.getZ() == 0)
            spawn(chunk, world);
    }

    private void spawn(Chunk chunk, World world)
    {

    }

    private void setBlock(Chunk chunk, int x, int y, int z, Material material)
    {
        chunk.getBlock(x * 16, y * 16, z * 16).setType(material, false);
    }

    private void setRegion(Chunk chunk, int minX, int minY, int minZ, int maxX, int maxY, int maxZ, Material material)
    {
        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                for (int z = minZ; z <= maxZ; z++)
                {
                    chunk.getBlock(x * 16, y * 16, z * 16).setType(material, false);
                }
            }
        }
    }
}
