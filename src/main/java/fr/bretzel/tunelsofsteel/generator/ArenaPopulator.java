package fr.bretzel.tunelsofsteel.generator;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

import java.util.Random;

public class ArenaPopulator extends BlockPopulator
{
    @Override
    public void populate(World world, Random random, Chunk chunk)
    {

    }

    public boolean inRange(int i, int min, int max)
    {
        return i >= min && i <= max;
    }
}
