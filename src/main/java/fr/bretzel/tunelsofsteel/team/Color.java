package fr.bretzel.tunelsofsteel.team;

import org.bukkit.ChatColor;

public enum Color
{
    WHITE(ChatColor.WHITE),
    SILVER(ChatColor.GRAY),
    GRAY(ChatColor.DARK_GRAY),
    BLACK(ChatColor.BLACK),
    RED(ChatColor.RED),
    YELLOW(ChatColor.YELLOW),
    LIME(ChatColor.GREEN),
    GREEN(ChatColor.DARK_GREEN),
    AQUA(ChatColor.AQUA),
    BLUE(ChatColor.BLUE),
    FUCHSIA(ChatColor.LIGHT_PURPLE),
    PURPLE(ChatColor.DARK_PURPLE),
    ORANGE(ChatColor.GOLD);

    private ChatColor chatColor;

    Color(ChatColor chatColor)
    {
        this.chatColor = chatColor;
    }

    public static Color match(String string)
    {
        for (Color c : values())
        {
            if (c.name().equalsIgnoreCase(string))
            {
                return c;
            }
        }

        return null;
    }

    public ChatColor getChatColor()
    {
        return chatColor;
    }

    @Override
    public String toString()
    {
        return chatColor.toString();
    }
}
