package fr.bretzel.tunelsofsteel.team;

import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.scoreboard.LobbyScoreboard;
import fr.bretzel.tunelsofsteel.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class TeamManager
{
    private static HashMap<Color, TOSTeam> colorTOSTeamHashMap = new HashMap<>();

    public static TOSTeam registerTeam(Color color)
    {
        if (colorTOSTeamHashMap.containsKey(color))
        {
            Logger.log("Cannot register team color: " + color.name());
            return null;
        } else
        {
            Logger.log("Register new team: " + color.name());
        }

        for (TOSTeam currentRegisteredTeam : getRegisteredTeam())
        {
            if (!currentRegisteredTeam.getName().equals(color.name()))
            {
                Team syncTeam = currentRegisteredTeam.getScoreboard().getTeam(color.name());

                if (syncTeam == null)
                {
                    syncTeam = currentRegisteredTeam.getScoreboard().registerNewTeam(color.name());
                    syncTeam.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.FOR_OWN_TEAM);
                }
            }
        }

        TOSTeam team = new TOSTeam(color);
        colorTOSTeamHashMap.put(color, team);

        TeamManager.syncTeamAndPlayer();

        return team;
    }

    public static void unregisterTeam(Color color)
    {
        if (colorTOSTeamHashMap.containsKey(color))
        {
            TOSTeam team = getTeam(color);

            for (OfflinePlayer player : team.getPlayers())
            {
                team.leave(player);

                if (player.isOnline())
                    LobbyScoreboard.actualiseLobbyScoreboard(player.getPlayer());
            }

            if (team.getTeamRegion() != null)
                TunnelsOfSteel.REGION_MANAGER.removeRegion(team.getTeamRegion());

            if (team.getAbsorbingRegion() != null)
                TunnelsOfSteel.REGION_MANAGER.removeRegion(team.getAbsorbingRegion());

            for (TOSTeam currentRegisteredTeam : getRegisteredTeam())
            {
                if (!currentRegisteredTeam.equals(team))
                {
                    Team syncTeam = currentRegisteredTeam.getScoreboard().getTeam(color.name());

                    if (syncTeam != null)
                    {
                        syncTeam.unregister();
                    }
                }
            }

            colorTOSTeamHashMap.remove(color);

            TeamManager.syncTeamAndPlayer();
        } else
        {
            Logger.log("Cannot unregister team color: " + color.name());
        }
    }

    public static boolean teamIsRegistered(Color color)
    {
        return getTeam(color) != null;
    }

    public static Inventory getEnderChest(TOSTeam tosTeam)
    {
        return tosTeam.getEnderInventory();
    }

    public static Inventory getEnderChest(Color color)
    {
        if (!teamIsRegistered(color))
            return null;

        return getTeam(color).getEnderInventory();
    }

    public static Inventory getEnderChest(Location location)
    {
        return getRegisteredTeam().stream().filter(tosTeam -> tosTeam.isEnderChest(location)).findFirst().map(TOSTeam::getEnderInventory).orElse(null);
    }

    public static boolean isTeamEnderChest(TOSTeam tosTeam, Location location)
    {
        return tosTeam.isEnderChest(location);
    }

    public static boolean isEnderChest(Location location)
    {
        return getRegisteredTeam().stream().anyMatch(tosTeam -> tosTeam.isEnderChest(location));
    }

    public static TOSTeam getTeam(Color color)
    {
        return colorTOSTeamHashMap.get(color);
    }

    public static TOSTeam getTeamOfPlayer(OfflinePlayer player)
    {
        return getRegisteredTeam().stream().filter(tosTeam -> tosTeam.containsPlayer(player)).findFirst().orElse(null);
    }

    public static boolean isRegistered(Color color)
    {
        return getRegisteredTeam().contains(getTeam(color));
    }

    public static Collection<TOSTeam> getRegisteredTeam()
    {
        return colorTOSTeamHashMap.values();
    }

    public static void removeAll()
    {
        colorTOSTeamHashMap.clear();
    }

    public static void syncTeamAndPlayer()
    {
        List<String> listName = new ArrayList<>();

        for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
            TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

            if (tosPlayer != null) {
                TOSTeam tosTeamPlayer = tosPlayer.getTeam();

                if (tosTeamPlayer != null) {
                    for (TOSTeam tosTeam : getRegisteredTeam()) {
                        checkTeams(tosTeam);

                        for (Team vanillaTeam : tosTeam.getScoreboard().getTeams()) {
                            if (tosTeamPlayer.equals(tosTeam) || tosTeam.getName().equals(vanillaTeam.getName()))
                                continue;

                            if (vanillaTeam.getName().equals(tosTeamPlayer.getName()) && !vanillaTeam.hasPlayer(player)) {
                                vanillaTeam.addPlayer(player);
                                System.out.println("Add " + player.getName() + " on MCTeam " + vanillaTeam.getName() + ", TOSTeam " + tosTeam.getName());
                            } else if (vanillaTeam.hasPlayer(player) && !vanillaTeam.getName().equals(tosTeamPlayer.getName()) && !tosTeamPlayer.getName().equals(vanillaTeam.getName())) {
                                vanillaTeam.removePlayer(player);
                                System.out.println("Removed " + player.getName() + " on MCTeam " + vanillaTeam.getName() + ", TOSTeam " + tosTeam.getName());
                            }
                        }
                    }
                } else if (!listName.contains(player.getName())) {
                    for (TOSTeam tosTeam : getRegisteredTeam()) {
                        checkTeams(tosTeam);

                        for (Team vanillaTeam : tosTeam.getScoreboard().getTeams()) {
                            if (vanillaTeam.hasPlayer(player)) {
                                System.out.println("Removed " + player.getName() + " on MCTeam " + vanillaTeam.getName() + ", TOSTeam " + tosTeam.getName());
                                vanillaTeam.removePlayer(player);
                            }
                        }
                    }
                }

                listName.add(player.getName());
            }
        }

        listName.clear();
    }

    private static void checkTeams(TOSTeam team)
    {
        for (TOSTeam tosTeam : getRegisteredTeam())
        {
            Team vanillaTeam = team.getScoreboard().getTeam(tosTeam.getName());

            if (vanillaTeam == null)
            {
                vanillaTeam = team.getScoreboard().registerNewTeam(tosTeam.getName());
            }

            vanillaTeam.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.FOR_OTHER_TEAMS);
            vanillaTeam.setColor(tosTeam.getColor().getChatColor());
        }
    }
}
