package fr.bretzel.tunelsofsteel.team;

import fr.bretzel.tunelsofsteel.event.TeamUpdateEvent;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.region.TeamRegion;
import fr.bretzel.tunelsofsteel.scoreboard.LobbyScoreboard;
import fr.bretzel.tunelsofsteel.util.Logger;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class TOSTeam
{
    private Team team;
    private Color color;
    private Scoreboard scoreboard;
    private List<Location> enderChests = new ArrayList<>();
    private Inventory enderInventory;
    private TeamRegion teamRegion;
    private Location spawnLocation;
    private Material material;
    private AbsorbingRegion absorbingRegion;
    private int talentium = 0;

    public TOSTeam(Color color)
    {
        this.color = color;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        this.team = getScoreboard().registerNewTeam(getColor().name());
        this.team.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.FOR_OTHER_TEAMS);
        this.team.setColor(color.getChatColor());

        this.enderInventory = Bukkit.createInventory(null, 9, ChatColor.DARK_PURPLE + "Ender Chest (Team " + getColor().getChatColor() + getColor().name() + ChatColor.DARK_PURPLE + ")");
    }

    public Collection<OfflinePlayer> getPlayers() {
        List<OfflinePlayer> offlinePlayers = new ArrayList<>();

        getTeam().getEntries().forEach(s ->
        {
            if (s.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}")) {
                OfflinePlayer offlinePlayer = TOSUtils.getOfflinePlayer(UUID.fromString(s));
                if (offlinePlayer != null)
                    offlinePlayers.add(offlinePlayer);
            }
        });

        return offlinePlayers;
    }

    public void join(OfflinePlayer player) {
        TOSTeam playerTeam = TeamManager.getTeamOfPlayer(player);

        if (playerTeam != null && playerTeam.containsPlayer(player))
            playerTeam.leave(player);

        if (getTeam() != null && player != null) {
            if (player.isOnline()) {
                Player p = player.getPlayer();
                if (p != null) {
                    p.setDisplayName(getColor() + player.getName() + ChatColor.RESET);
                    p.setPlayerListName(getColor() + player.getName() + ChatColor.RESET);
                }
            }

            getTeam().addEntry(player.getUniqueId().toString());

            TOSPlayer.getPlayer(player).setTeam(this);

            TeamManager.syncTeamAndPlayer();

            TeamUpdateEvent event = new TeamUpdateEvent(this);
            Bukkit.getPluginManager().callEvent(event);
        } else
        {
            Logger.log("The team or player is null !");
        }
    }

    public void leave(OfflinePlayer player)
    {
        if (getTeam() != null)
        {
            if ((player instanceof Player))
            {
                ((Player) player).setDisplayName(ChatColor.RESET + player.getName() + ChatColor.RESET);
                ((Player) player).setPlayerListName(ChatColor.RESET + player.getName() + ChatColor.RESET);
                LobbyScoreboard.actualiseLobbyScoreboard((Player) player);
            }

            getTeam().removePlayer(player);

            TOSPlayer tos_player = TOSPlayer.getPlayer(player);
            tos_player.setTeam(null);

            TeamManager.syncTeamAndPlayer();

            TeamUpdateEvent event = new TeamUpdateEvent(this);
            Bukkit.getPluginManager().callEvent(event);
        }
    }

    public boolean containsPlayer(OfflinePlayer player)
    {
        for (OfflinePlayer cst : getPlayers())
        {
            if (cst.getUniqueId().equals(player.getUniqueId()))
                return true;
        }

        return false;
    }

    public Scoreboard getScoreboard()
    {
        return scoreboard;
    }

    public Team getTeam()
    {
        return team;
    }

    public Color getColor()
    {
        return color;
    }

    public Inventory getEnderInventory()
    {
        return enderInventory;
    }

    public List<Location> getEnderChests()
    {
        return enderChests;
    }

    public void removeEncherChest(Location location)
    {
        if (!isEnderChest(location))
        {
            Logger.log("Sorry cannot remove ender chest for team " + getColor().name() + ", the location is not in the encer chest list");
        } else
        {

            getEnderChests().removeIf(loc -> loc.getWorld().getName().equals(location.getWorld().getName()) && loc.getBlockX() == location.getBlockX() && loc.getBlockY() == location.getBlockY()
                    && loc.getBlockZ() == location.getBlockZ());
        }
    }

    public void addEncherChest(Location location)
    {
        if (location.getBlock().getType() != Material.ENDER_CHEST)
        {
            Logger.log("Sorry cannot add ender chest for team " + getColor().name() + ", the location is not a ender chest");
            return;
        }

        if (isEnderChest(location))
        {
            Logger.log("Sorry cannot add ender chest for team " + getColor().name() + ", the location is already added");
            return;
        }

        getEnderChests().add(location);
    }

    public boolean isEnderChest(Location location)
    {
        if (location.getBlock().getType() != Material.ENDER_CHEST)
            return false;

        for (Location loc : getEnderChests())
        {
            if (loc.getWorld().getName().equals(location.getWorld().getName()) && loc.getBlockX() == location.getBlockX() && loc.getBlockY() == location.getBlockY()
                    && loc.getBlockZ() == location.getBlockZ())
            {
                return true;
            }
        }

        return false;
    }

    public Location getSpawnLocation()
    {
        return spawnLocation;
    }

    public void setSpawnLocation(Location spawnLocation)
    {
        this.spawnLocation = spawnLocation;
    }

    public TeamRegion getTeamRegion()
    {
        return teamRegion;
    }

    public void setTeamRegion(TeamRegion teamRegion)
    {
        this.teamRegion = teamRegion;
    }

    public String getName()
    {
        return getColor() == null ? "null" : getColor().name();
    }

    public Material getDepositBotomMaterial()
    {
        return material == null ? Material.AIR : material;
    }

    public void setDepositBotomMaterial(Material material)
    {
        this.material = material;
    }

    public AbsorbingRegion getAbsorbingRegion() {
        return absorbingRegion;
    }

    public void setAbsorbingRegion(AbsorbingRegion absorbingRegion) {
        this.absorbingRegion = absorbingRegion;
    }

    public int getTalentium() {
        return talentium;
    }

    public void setTalentium(int talentium) {
        this.talentium = talentium;
    }

    public void addTalentium(int talentium) {
        setTalentium(getTalentium() + talentium);
    }

}
