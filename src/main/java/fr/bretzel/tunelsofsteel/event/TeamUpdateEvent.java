package fr.bretzel.tunelsofsteel.event;

import fr.bretzel.tunelsofsteel.team.TOSTeam;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TeamUpdateEvent extends Event {
    private static final long serialVersionUID = 4803701525466747L;
    private static final HandlerList handlers = new HandlerList();
    private TOSTeam team;

    public TeamUpdateEvent(TOSTeam tosTeam) {
        this.team = tosTeam;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public TOSTeam getTeam() {
        return team;
    }
}
