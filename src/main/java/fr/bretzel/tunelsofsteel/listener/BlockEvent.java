package fr.bretzel.tunelsofsteel.listener;

import fr.bretzel.config.Config;
import fr.bretzel.config.table.ColumnType;
import fr.bretzel.config.table.Table;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.game.Statue;
import fr.bretzel.tunelsofsteel.team.Color;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class BlockEvent implements Listener {

    private static Config sqLiteConfig;
    private static Table table;

    public BlockEvent() {
        File file = new File(TunnelsOfSteel.INSTANCE.getDataFolder(), "tos_blocks.db");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        sqLiteConfig = new Config(file, Config.SQLType.SQLITE, table = new Table("tos_blocks", ColumnType.VARCHAR("team", "16"), ColumnType.VARCHAR("block", "max")));
    }

    public static HashMap<TOSTeam, ArrayList<Location>> getBlockPosed() {
        HashMap<TOSTeam, ArrayList<Location>> blMap = new HashMap<>();

        PreparedStatement statement = null;
        try {
            String query = "SELECT * FROM " + table.getTableName();
            statement = sqLiteConfig.openConnection().prepareStatement(query);

            ResultSet set = statement.executeQuery();
            while (set.next()) {
                TOSTeam tosTeam = TeamManager.getTeam(Color.match(set.getString("team")));
                Location location = TunnelsOfSteel.toLocationString(set.getString("block"));

                if (blMap.containsKey(tosTeam)) {
                    blMap.get(tosTeam).add(location);
                } else {
                    ArrayList<Location> locations = new ArrayList<>();
                    locations.add(location);
                    blMap.put(tosTeam, locations);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();

                sqLiteConfig.getConfig().getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return blMap;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlaced(BlockPlaceEvent event) {
        if (event.isCancelled())
            return;

        Player player = event.getPlayer();
        Block block = event.getBlock();

        TOSTeam team = TeamManager.getTeamOfPlayer(player);

        if (team != null && TunnelsOfSteel.STATUE == Statue.START && TunnelsOfSteel.GAME_REGION != null && TunnelsOfSteel.GAME_REGION.contains(block.getLocation())) {
            pushToDb(block, team);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled())
            return;

        Player player = event.getPlayer();
        Block block = event.getBlock();

        TOSTeam team = TeamManager.getTeamOfPlayer(player);

        if (team != null && TunnelsOfSteel.STATUE == Statue.START && TunnelsOfSteel.GAME_REGION != null && TunnelsOfSteel.GAME_REGION.contains(block.getLocation())) {
            pushToDb(block, team);
        }
    }

    public synchronized void pushToDb(Block block, TOSTeam tosTeam) {
        String queryBaseSet = "INSERT INTO " + table.getTableName() + " ( team, block ) VALUES (?, ?)";
        PreparedStatement statement = null;
        String locString = TunnelsOfSteel.toStringLocation(block.getLocation());
        try {
            if (!hasTeamAndBLockOnDB(tosTeam, block)) {
                statement = sqLiteConfig.openConnection().prepareStatement(queryBaseSet);
                statement.setString(1, tosTeam.getName());
                statement.setString(2, locString);
                statement.executeUpdate();
                statement.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();

                sqLiteConfig.getConfig().getConnection().close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized boolean hasTeamAndBLockOnDB(TOSTeam tosTeam, Block block) {
        PreparedStatement statement = null;
        try {
            String query = "SELECT * FROM " + table.getTableName() + " WHERE block = ?";
            statement = sqLiteConfig.openConnection().prepareStatement(query);

            statement.setString(1, TunnelsOfSteel.toStringLocation(block.getLocation()));

            ResultSet set = statement.executeQuery();
            while (set.next()) {
                String team = set.getString("team");
                if (team.equals(tosTeam.getName()))
                    return true;
            }

        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();

                sqLiteConfig.getConfig().getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
