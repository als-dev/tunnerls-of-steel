package fr.bretzel.tunelsofsteel.listener;

import com.connorlinfoot.titleapi.TitleAPI;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.game.Statue;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.refineries.Refineries;
import fr.bretzel.tunelsofsteel.refineries.RefineriesManager;
import fr.bretzel.tunelsofsteel.refineries.Upgrade;
import fr.bretzel.tunelsofsteel.scoreboard.LobbyScoreboard;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.team.TeamManager;
import fr.bretzel.tunelsofsteel.util.NBTItemStack;
import fr.bretzel.tunelsofsteel.util.Permission;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import net.minecraft.server.v1_14_R1.BlockPosition;
import net.minecraft.server.v1_14_R1.PacketPlayOutBlockAction;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_14_R1.util.CraftMagicNumbers;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.util.RayTraceResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class PlayerEvent implements Listener
{
    private List<UUID> onEnderChest = new ArrayList<>();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

        TeamManager.getRegisteredTeam().forEach(checkedTeam ->
        {
            if (checkedTeam.containsPlayer(player) && tosPlayer.getTeam() == null)
                tosPlayer.setTeam(checkedTeam);
        });

        TOSTeam tosTeam = tosPlayer.getTeam();

        if (tosTeam != null) {
            if (!tosTeam.containsPlayer(player))
                tosTeam.join(player);

            player.setDisplayName(tosTeam.getColor() + player.getName() + ChatColor.RESET);
            player.setPlayerListName(tosTeam.getColor() + player.getName() + ChatColor.RESET);
            player.setCustomName(tosTeam.getColor() + player.getName() + ChatColor.RESET);
        }

        if (TunnelsOfSteel.STATUE == Statue.WAITING)
        {
            player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 0.6F, 1F);
            TitleAPI.sendTitle(player, 20, 50, 20, ChatColor.translateAlternateColorCodes('&', "&b~ &fTunnels§bOf§fSteel §b~"),
                    ChatColor.GOLD + "Made by MrBretzel and designed by Artylight");

            //Set the welcome scoreboard of player
            LobbyScoreboard.actualiseLobbyScoreboard(player);

            if (!player.isOp() || !player.hasPermission(Permission.EVENT_TOS_BYPASS_JOIN))
            {
                player.setGameMode(GameMode.ADVENTURE);
                player.teleport(new Location(TunnelsOfSteel.getTOSWorld(), 0, 254, 0));
            }
        } else if (TunnelsOfSteel.STATUE == Statue.START) {
            if (!player.isOp() || !player.hasPermission(Permission.EVENT_TOS_BYPASS_JOIN) && player.getGameMode() != GameMode.CREATIVE) {
                player.setGameMode(GameMode.SURVIVAL);
            }
        }
    }

    @EventHandler
    public void onPlayerCloseInventory(InventoryCloseEvent event)
    {
        if (onEnderChest.contains(event.getPlayer().getUniqueId())) {
            Player player = (Player) event.getPlayer();
            World world = event.getPlayer().getWorld();
            RayTraceResult result = world.rayTraceBlocks(player.getEyeLocation(), player.getEyeLocation().getDirection(), 6);

            if (result != null && result.getHitBlock() != null) {
                Location location = result.getHitBlock().getLocation();
                TOSUtils.sendPacket(player,
                        new PacketPlayOutBlockAction(new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ()), CraftMagicNumbers.getBlock(Material.ENDER_CHEST), 0, 0));
                world.playSound(event.getPlayer().getLocation(), Sound.BLOCK_ENDER_CHEST_CLOSE, 1F, 1);
                onEnderChest.remove(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler
    public void onPlayerRightClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        TOSTeam tosTeam = tosPlayer.getTeam();

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && block != null) {
            if (block.getType() == Material.ENDER_CHEST && tosTeam != null && TeamManager.isTeamEnderChest(tosTeam, block.getLocation())) {
                Inventory inventory = TeamManager.getEnderChest(block.getLocation());

                if (inventory != null)
                    event.getPlayer().openInventory(inventory);
                else
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "Error when opening Ender Chest at location : " + block.getX() + ";" + block.getY() + ";" + block.getZ()
                            + " on world " + block.getWorld().getName());

                player.playSound(player.getLocation(), Sound.BLOCK_ENDER_CHEST_OPEN, 0.3F, 1F);
                onEnderChest.add(player.getUniqueId());
                Location location = block.getLocation();
                TOSUtils.sendPacket(player,
                        new PacketPlayOutBlockAction(new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ()), CraftMagicNumbers.getBlock(Material.ENDER_CHEST), 1, 1));
                event.setCancelled(true);
            } else if ((block.getType() == Material.FURNACE) && RefineriesManager.getRefinery(block.getLocation()) != null)
            {
                Refineries refineries = RefineriesManager.getRefinery(block.getLocation());
                player.openInventory(refineries.getInventory());
                event.setCancelled(true);
            }
        }
    }

    /*@EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        if (TunnelsOfSteel.STATUE == Statue.START)
        {
            Player player = event.getPlayer();
            TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
            Region region = TunnelsOfSteel.REGION_MANAGER.getRegion(TunnelsOfSteel.REGION_MIDDLE_NAME);

            if (region != null && tosPlayer.getTeam() != null && region.contains(player.getLocation()))
            {
                Block blockUnderFoot = TOSUtils.getBlockUnderPlayer(player);

                if (blockUnderFoot != null && blockUnderFoot.getType() == tosPlayer.getTeam().getDepositBotomMaterial())
                {
                    if (TOSUtils.hasTalentiumInInventory(player) && !AddPlayersScoreToTeam.hasPlayer(player))
                    {
                        new AddPlayersScoreToTeam(player, tosPlayer.getTeam());
                    }
                }
            }
        }
    }*/

    /*@EventHandler
    public void onPlayerClickInventory(InventoryClickEvent event)
    {
        if (TunnelsOfSteel.STATUE != Statue.START && !event.getWhoClicked().getWorld().getName().equals(TunnelsOfSteel.TOS_WORLD_NAME) || !(event.getWhoClicked() instanceof Player))
        {
            return;
        }

        Inventory inventory = event.getView().getTopInventory();

        ItemStack currentItemStack = event.getCurrentItem();
        ItemStack cursorItemStack = event.getCursor();

        Player player = (Player) event.getWhoClicked();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);

        if (tosPlayer.getTeam() != null && inventory.getType() == InventoryType.CHEST && (currentItemStack != null || cursorItemStack != null))
        {
            ItemStack upgradeStack = inventory.getItem(22);
            ItemStack inputStack = inventory.getItem(9);
            ItemStack outputStack = inventory.getItem(17);

            int clickedSlot = event.getSlot();

            //Input
            if (clickedSlot == 9)
            {
                if (upgradeStack != null)
                {
                    Upgrade upgrade = RefineriesManager.getUpgradeFromStack(upgradeStack);

                    if (upgrade == Upgrade.PRIVATE)
                    {
                        ItemMeta meta = upgradeStack.getItemMeta();
                        CustomItemTagContainer tagContainer = meta.getCustomTagContainer();
                        NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "UpgradeTeamOwner");

                        Color team = null;

                        if (tagContainer.hasCustomTag(namespacedKey, ItemTagType.STRING))
                        {
                            team = Color.match(tagContainer.getCustomTag(namespacedKey, ItemTagType.STRING));
                        }

                        TOSTeam clickedPlayerTeam = tosPlayer.getTeam();
                        TOSTeam owner = TeamManager.getTeam(team);

                        if (owner != null && clickedPlayerTeam != null && !owner.equals(clickedPlayerTeam) && (cursorItemStack == null || inputStack != null))
                        {
                            player.playSound(player.getLocation(), Sound.ENTITY_SHULKER_AMBIENT, 0.2F, 2F);
                            event.setCancelled(true);
                        }
                        return;
                    }
                }
            }

            //Output
            if (clickedSlot == 17)
            {
                if (upgradeStack != null)
                {
                    Upgrade upgrade = RefineriesManager.getUpgradeFromStack(upgradeStack);

                    if (upgrade == Upgrade.PRIVATE)
                    {
                        ItemMeta meta = upgradeStack.getItemMeta();
                        CustomItemTagContainer tagContainer = meta.getCustomTagContainer();
                        NamespacedKey namespacedKey = new NamespacedKey(TunnelsOfSteel.INSTANCE, "UpgradeTeamOwner");

                        Color team = null;

                        if (tagContainer.hasCustomTag(namespacedKey, ItemTagType.STRING))
                        {
                            team = Color.match(tagContainer.getCustomTag(namespacedKey, ItemTagType.STRING));
                        }

                        TOSTeam clickedPlayerTeam = tosPlayer.getTeam();
                        TOSTeam owner = TeamManager.getTeam(team);

                        if (owner != null && clickedPlayerTeam != null && !owner.getName().equals(clickedPlayerTeam.getName()))
                        {
                            player.playSound(player.getLocation(), Sound.ENTITY_SHULKER_AMBIENT, 0.2F, 2F);
                            event.setCancelled(true);
                        }
                        return;
                    }
                }
            }

            //Upgrade
            if (clickedSlot == 22)
            {
                if (upgradeStack != null)
                {
                    Upgrade upgrade = RefineriesManager.getUpgradeFromStack(upgradeStack);

                    if (upgrade == Upgrade.PRIVATE)
                    {
                        player.playSound(player.getLocation(), Sound.ENTITY_SHULKER_AMBIENT, 0.2F, 2F);
                        event.setCancelled(true);
                        return;
                    }
                }
            }
        }
    }*/

    @EventHandler
    public void onPlayerPlaceBlock(BlockPlaceEvent event)
    {
        Player player = event.getPlayer();
        //TODO
        ItemMeta metaInHand = player.getInventory().getItemInMainHand().getItemMeta();

        for (Upgrade upgrade : Upgrade.values())
        {
            if (metaInHand != null && metaInHand.hasDisplayName() && !metaInHand.getDisplayName().isEmpty() && metaInHand.getDisplayName().equals(upgrade.getDisplayName()))
            {
                event.setCancelled(true);
                return;
            }
        }

        if (player.getInventory().getItemInMainHand().getType() == Material.TORCH)
        {
            if (player.getInventory().getItemInMainHand().getItemMeta() != null
                    && !player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().isEmpty() &&
                    player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Infinite Torch"))
            {
                ItemStack newStack = player.getInventory().getItemInMainHand();
                newStack.setAmount(64);
                player.getInventory().setItemInMainHand(newStack);

                event.getBlockPlaced().setMetadata("isInfiniteTorch", new FixedMetadataValue(TunnelsOfSteel.INSTANCE, true));
            }
        } else if (player.getInventory().getItemInOffHand().getType() == Material.TORCH)
        {
            if (player.getInventory().getItemInOffHand().getItemMeta() != null
                    && !player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().isEmpty() &&
                    player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Infinite Torch"))
            {
                ItemStack newStack = player.getInventory().getItemInOffHand();
                newStack.setAmount(64);
                player.getInventory().setItemInOffHand(newStack);

                event.getBlockPlaced().setMetadata("isInfiniteTorch", new FixedMetadataValue(TunnelsOfSteel.INSTANCE, true));
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        List<ItemStack> newDrops = new ArrayList<>(event.getDrops());
        //TODO
        for (ItemStack stack : newDrops)
        {
            ItemMeta meta = stack.getItemMeta();
            if (meta != null && meta.hasDisplayName() && !meta.getDisplayName().isEmpty()
                    && (meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Infinite Torch") ||
                    meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Infinite Potato") ||
                    meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Pelle de l'Aventurier")))
            {
                event.getDrops().remove(stack);
            }
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event)
    {
        TOSPlayer player = TOSPlayer.getPlayer(event.getPlayer());

        if (TunnelsOfSteel.STATUE == Statue.START && player.getTeam() != null)
        {
            //TODO
            ItemStack potatoStack = new ItemStack(Material.BAKED_POTATO, 64);
            ItemMeta potatoMeta = potatoStack.getItemMeta();
            potatoMeta.setDisplayName(ChatColor.GREEN + "Infinite Potato");
            potatoStack.setItemMeta(potatoMeta);

            ItemStack torchStack = new ItemStack(Material.TORCH, 64);
            ItemMeta torchMeta = torchStack.getItemMeta();
            torchMeta.setDisplayName(ChatColor.GREEN + "Infinite Torch");
            torchStack.setItemMeta(torchMeta);

            ItemStack saddleStack = new ItemStack(Material.IRON_SHOVEL, 1);
            ItemMeta saddleMeta = saddleStack.getItemMeta();
            saddleMeta.setDisplayName(ChatColor.GREEN + "Pelle de l'Aventurier");
            saddleMeta.setLore(Arrays.asList(ChatColor.RESET + "Efficace pour creuser mais également pour se défendre", ChatColor.RESET + "Elle possède les mêmes stats de combat qu'une épée en Pierre."));
            saddleStack.setItemMeta(saddleMeta);

            String[] names = new String[]{"generic.attackDamage", "generic.attackSpeed"};
            String[] hand = new String[]{"mainhand", "mainhand"};
            double[] amout = new double[]{4, -2.4};
            double[] operation = new double[]{0, 0};

            saddleStack = NBTItemStack.addAttribute(saddleStack, names, hand, amout, operation, new int[]{17892, 53526}, new int[]{155197, 122973});

            event.getPlayer().getInventory().addItem(torchStack, potatoStack, saddleStack);
        }
    }

    @EventHandler
    public void onTorchBreak(BlockBreakEvent event)
    {
        if (event.getBlock().getType() == Material.TORCH)
        {
            Block block = event.getBlock();
            List<MetadataValue> metadataValueList = block.getMetadata("isInfiniteTorch");

            if (metadataValueList.size() > 0 &&
                    block.getMetadata("isInfiniteTorch").get(0).asBoolean())
            {
                event.setDropItems(false);
            }
        }
    }

    @EventHandler
    public void onPlayerConsumeItem(PlayerItemConsumeEvent event)
    {
        if (event.getItem().getType() == Material.BAKED_POTATO)
        {
            //TODO
            ItemStack newStack = event.getItem().clone();
            newStack.setAmount(64);
            event.setItem(newStack);
            event.getPlayer().getInventory().setItemInMainHand(newStack);
        }
    }

    @EventHandler
    public void onPlayerFoodChange(FoodLevelChangeEvent event)
    {
        if (TunnelsOfSteel.STATUE != Statue.START)
            event.setCancelled(true);
    }

    /*@EventHandler
    public void onPlayerDamage(EntityDamageEvent event)
    {
        if (event.getEntity() instanceof Player && TunnelsOfSteel.STATUE != Statue.START)
            event.setCancelled(true);
    }*/


}
