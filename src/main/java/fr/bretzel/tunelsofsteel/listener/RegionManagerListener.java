package fr.bretzel.tunelsofsteel.listener;

import com.google.common.collect.ImmutableList;
import fr.bretzel.region.Region;
import fr.bretzel.region.SimpleRegion;
import fr.bretzel.region.event.*;
import fr.bretzel.tunelsofsteel.TunnelsOfSteel;
import fr.bretzel.tunelsofsteel.game.Statue;
import fr.bretzel.tunelsofsteel.player.TOSPlayer;
import fr.bretzel.tunelsofsteel.region.AbsorbingRegion;
import fr.bretzel.tunelsofsteel.region.TeamRegion;
import fr.bretzel.tunelsofsteel.task.AddPlayersScoreToTeam;
import fr.bretzel.tunelsofsteel.team.TOSTeam;
import fr.bretzel.tunelsofsteel.util.TOSUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class RegionManagerListener implements Listener {
    private ImmutableList<BlockFace> blockFaces = ImmutableList.of(BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH, BlockFace.SOUTH);

    @EventHandler
    public void onBlockExploseInRegion(BlockExplodeInRegionEvent event) {
        if (event.getRegion() instanceof TeamRegion || event.getRegion() instanceof AbsorbingRegion)
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerJoinRegion(PlayerJoinRegionEvent event) {
        Player player = event.getPlayer();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        TOSTeam tosTeam = tosPlayer.getTeam();
        Region region = event.getRegion();

        if (tosTeam != null && region instanceof TeamRegion) {
            TOSTeam owner = ((TeamRegion) region).getOwner();
            if (!owner.equals(tosTeam) && TimeUnit.NANOSECONDS.toMillis(tosPlayer.getLastRoolBack()) > 100) {
                if (player.getVehicle() != null)
                    player.getVehicle().removePassenger(player);

                player.getWorld().spawnParticle(Particle.SMOKE_NORMAL, event.getTo(), 10, null);
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_HURT, 2f, 0.8f);

                rollBackPlayer(player, event.getFrom(), event.getTo());
                tosPlayer.sendActionBar(ChatColor.RED + "Sorry you cannot enter on " + owner.getColor() + owner.getName() + ChatColor.RED + " team spawn.", 1);
                event.setCancelled(true);
            } else if (tosTeam.equals(owner)) {
                tosPlayer.sendActionBar(ChatColor.DARK_GREEN + "You are entering on your team safe area", 1);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 2f, 0.8f);
            }
        } else if (tosTeam != null && region instanceof AbsorbingRegion) {
            if (TunnelsOfSteel.STATUE == Statue.START) {
                //TODO
                Block blockUnderFoot = TOSUtils.getBlockUnderPlayer(player);

                if (blockUnderFoot != null && blockUnderFoot.getType() == tosPlayer.getTeam().getDepositBotomMaterial()) {
                    Bukkit.broadcastMessage("Hello WAZAAAAAAA 1");
                    if (TOSUtils.hasTalentiumInInventory(player) && !AddPlayersScoreToTeam.hasPlayer(player)) {
                        Bukkit.broadcastMessage("Hello WAZAAAAAAA 2");
                        new AddPlayersScoreToTeam(player, tosPlayer.getTeam());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerLeaveRegion(PlayerLeaveRegionEvent event) {
        Player player = event.getPlayer();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        TOSTeam tosTeam = tosPlayer.getTeam();
        Region region = event.getRegion();

        if (tosTeam != null && region instanceof TeamRegion) {
            TOSTeam owner = ((TeamRegion) region).getOwner();
            if (owner.equals(tosTeam)) {
                tosPlayer.sendActionBar(ChatColor.DARK_RED + "You are leaving your team safe area", 1);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 2f, 0.8f);
            }
        }
    }

    @EventHandler
    public void onPlayerMoveRegion(PlayerMoveOnRegion event) {
        Player player = event.getPlayer();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        TOSTeam tosTeam = tosPlayer.getTeam();
        Region region = event.getRegion();

        if (tosTeam != null && region instanceof TeamRegion) {
            TOSTeam owner = ((TeamRegion) region).getOwner();

            if (player.getVehicle() != null)
                player.getVehicle().removePassenger(player);

            if (!owner.equals(tosTeam) && TimeUnit.NANOSECONDS.toMillis(tosPlayer.getLastRoolBack()) > 1000) {
                Location outsideLocation = getOutsideLocation(region, player.getLocation());
                player.teleport(outsideLocation);
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportInRegionEvent event) {
        Player player = event.getPlayer();
        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        TOSTeam tosTeam = tosPlayer.getTeam();
        Region region = event.getRegion();

        if (region instanceof TeamRegion)
            if (tosTeam != null && !((TeamRegion) region).getOwner().getName().equals(tosTeam.getName()))
                event.setCancelled(true);
    }

    @EventHandler
    public void vehicleEvent(VehicleMoveEvent event) {
        Vehicle vehicle = event.getVehicle();
        if (vehicle.getPassengers().size() > 0) {
            Collection<Region> regionList = TunnelsOfSteel.REGION_MANAGER.getRegion(event.getTo());

            vehicle.getPassengers().forEach(entity ->
            {
                if (entity instanceof Player) {
                    regionList.forEach(region ->
                    {
                        if (region instanceof TeamRegion) {
                            TeamRegion teamRegion = (TeamRegion) region;
                            TOSPlayer tosPlayer = TOSPlayer.getPlayer((Player) entity);
                            if (tosPlayer.getTeam() != null && !teamRegion.getOwner().getName().equals(tosPlayer.getTeam().getName())) {
                                vehicle.removePassenger(entity);
                                entity.teleport(event.getFrom().add(0, 0.5, 0));
                                entity.setVelocity(event.getFrom().toVector().subtract(event.getTo().toVector()));
                            }
                        }
                    });
                }
            });
        }
    }

    private void rollBackPlayer(Player player, Location from, Location to) {
        Vector velocity = new Vector().zero();

        velocity.setX(from.getX() - to.getX());
        velocity.setZ(from.getZ() - to.getZ());

        velocity = velocity.clone().normalize();

        velocity.setY(0.21D);

        if (!NumberConversions.isFinite(velocity.getX()))
            velocity.setX(0.0D);

        if (!NumberConversions.isFinite(velocity.getZ()))
            velocity.setZ(0.0D);

        Vector diffVec = velocity.subtract(player.getVelocity());

        //Fix multiple roll back #NoTest
        if ((diffVec.getX() > 0 && diffVec.getX() < 0.2) && (diffVec.getZ() > 0 && diffVec.getZ() < 0.2)) {
            return;
        }

        TOSPlayer tosPlayer = TOSPlayer.getPlayer(player);
        tosPlayer.setLastRoolBack(System.nanoTime());
        player.setVelocity(velocity.normalize().multiply(1.5D));
    }

    private Location getOutsideLocation(Region region, Location location) {
        double distance = Double.MAX_VALUE;
        Location reduplication = location.clone();

        for (BlockFace blockFace : blockFaces) {
            Location clonedLocation = location.clone();

            while (region.contains(clonedLocation)) {
                clonedLocation = clonedLocation.add(blockFace.getDirection());

                //Make sure the loop stop
                if (!region.contains(clonedLocation)) {
                    boolean hasVoid = true;

                    for (int y = clonedLocation.getBlockY(); y <= 0; y--) {
                        Location loc = clonedLocation.clone();
                        loc.setY(y);
                        if (!loc.getBlock().getType().name().equalsIgnoreCase("AIR")) {
                            hasVoid = false;
                            break;
                        }
                    }

                    double distanceSquared = location.distanceSquared(clonedLocation);

                    if (distanceSquared < distance && hasVoid) {
                        distance = distanceSquared;
                        reduplication = clonedLocation;
                    }
                }
            }
        }

        return reduplication;
    }

    @EventHandler
    public void onBlockPlaceInRegion(BlockPlaceInRegionEvent event) {
        Region region = event.getRegion();
        TOSPlayer player = TOSPlayer.getPlayer(event.getPlayer());
        TOSTeam tosTeam = player.getTeam();

        if (region instanceof TeamRegion) {
            TeamRegion teamRegion = (TeamRegion) region;

            if (tosTeam != null && !teamRegion.getOwner().equals(tosTeam)) {
                event.setCancelled(true);
            }
        } else if (region instanceof SimpleRegion) {
            SimpleRegion simpleRegion = (SimpleRegion) region;

            if (simpleRegion.getName().startsWith("TOS_R_") && TunnelsOfSteel.STATUE == Statue.START) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockBreakInRegion(BlockBreakInRegionEvent event) {
        Region region = event.getRegion();
        TOSPlayer player = TOSPlayer.getPlayer(event.getPlayer());
        TOSTeam tosTeam = player.getTeam();

        if (region instanceof TeamRegion) {
            TeamRegion teamRegion = (TeamRegion) region;

            if (tosTeam != null && !teamRegion.getOwner().equals(tosTeam)) {
                event.setCancelled(true);
            }
        } else if (region instanceof SimpleRegion) {
            SimpleRegion simpleRegion = (SimpleRegion) region;

            if (simpleRegion.getName().startsWith("TOS_R_") && TunnelsOfSteel.STATUE == Statue.START) {
                event.setCancelled(true);
            }
        }
    }

}
