package fr.bretzel.tunelsofsteel.listener;

import fr.bretzel.tunelsofsteel.util.Selection;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class SelectionEvent implements Listener
{
    private String INTRO;

    public SelectionEvent(JavaPlugin plugin)
    {
        INTRO = ChatColor.translateAlternateColorCodes('&', "&b[&r" + plugin.getName() + "&b]&r ");
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();

        if (player.isOp() && player.getGameMode() == GameMode.CREATIVE)
        {
            player.getInventory().getItemInMainHand();
            if (player.getInventory().getItemInMainHand().getType() == Material.GOLDEN_SWORD)
            {
                if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
                {
                    Selection selection = Selection.getSelection(player);
                    event.setCancelled(true);

                    if (selection.getLocationTwo() == null || !selection.getLocationTwo().toString().equals(event.getClickedBlock().getLocation().toString()))
                    {
                        selection.setLocationTwo(event.getClickedBlock().getLocation());
                        player.sendMessage(INTRO + ChatColor.GREEN + "Second location have selected !");
                    }
                } else if (event.getAction() == Action.LEFT_CLICK_BLOCK)
                {
                    Selection selection = Selection.getSelection(player);
                    event.setCancelled(true);
                    if (selection.getLocationOne() == null || !selection.getLocationOne().toString().equals(event.getClickedBlock().getLocation().toString()))
                    {
                        selection.setLocationOne(event.getClickedBlock().getLocation());
                        player.sendMessage(INTRO + ChatColor.GREEN + "First location have selected !");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event)
    {
        Player player = event.getPlayer();
        if (player.isOp() && player.getGameMode() == GameMode.CREATIVE && Selection.getSelection(player) != null)
        {
            Selection.removeSelection(player);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();
        if (player.isOp() && player.getGameMode() == GameMode.CREATIVE && Selection.getSelection(player) != null)
        {
            Selection.removeSelection(player);
        }
    }
}
