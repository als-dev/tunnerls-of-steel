package fr.bretzel.config.online;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import fr.bretzel.config.Config;
import fr.bretzel.config.table.ColumnType;
import fr.bretzel.config.table.Table;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MySQLConfig implements Config.IConfig {

    private Connection connection;

    private String hostname, username, password, database, port;

    private boolean hasFirstConnection = false;

    private List<Table> tables = new ArrayList<>();

    /**
     * Creates a new MySQL instance for a specific database
     *
     * @param hostname Name of the host
     * @param port     Port number
     * @param database Database name
     * @param username Username
     * @param password Password
     */
    public MySQLConfig(String hostname, String username, String password, String port, String database)
    {
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.database = database;
        this.port = port;
    }


    public Connection getConnection()
    {
        try
        {
            if (connection == null || connection.isClosed())
                return openConnection();

            return connection;
        } catch (ClassNotFoundException | SQLException e)
        {
            return null;
        }
    }

    @Override
    public boolean ifTableIsEmpty(Table table, String columns)
    {
        PreparedStatement statement = null;
        try
        {
            statement = openConnection().prepareStatement("SELECT " + columns + " FROM " + table.getTableName());

            ResultSet resultSet = statement.executeQuery();

            return !resultSet.next();
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean ifStringExistForColon(String value, String colon, Table table)
    {
        Statement statement = null;
        try
        {
            statement = openConnection().createStatement();
            String query = "SELECT " + colon + " FROM " + table.getTableName() + " WHERE " + colon + " = '" + value + "'";
            ResultSet set = statement.executeQuery(query);
            if (set.next())
            {
                String v = set.getString(colon);
                if (v.equalsIgnoreCase(value))
                    return true;
            }
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean ifColumnsExist(Table table, ColumnType column)
    {
        return ifColumnsExist(table, column.getName());
    }

    @Override
    public boolean ifColumnsExist(Table table, String column)
    {
        boolean r = false;
        try
        {
            DatabaseMetaData md = openConnection().getMetaData();
            ResultSet rs = md.getColumns(null, null, table.getTableName(), column);

            r = rs.next();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        return r;
    }

    @Override
    public Object getObject(String colon, String where, Table table)
    {
        Object o = null;
        PreparedStatement statement = null;
        try
        {
            statement = openConnection().prepareStatement("SELECT " + colon + " FROM " + table.getTableName() + " WHERE " + where);
            ResultSet set = statement.executeQuery();
            o = set.next() ? set.getObject(1) : null;
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return o;
    }

    @Override
    public void setObject(Object value, String colon, String where, Table table)
    {
        if (where == null || where.isEmpty())
        {
            insertInto(value, colon, table);
        } else
        {
            updateSet(value, colon, where, table);
        }
    }

    private void updateSet(Object value, String colon, String where, Table table)
    {
        Statement statement;
        try
        {
            String queryBaseSet = "UPDATE " + table.getTableName() + " SET " + colon + "=\"" + value + "\" WHERE " + where;
            statement = openConnection().createStatement();
            statement.executeUpdate(queryBaseSet);
            statement.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void insertInto(Object value, String colon, Table table)
    {
        PreparedStatement statement;
        try
        {
            String queryBaseSet = "INSERT INTO %table% ( %colon% ) VALUES (?)";
            statement = openConnection().prepareStatement(queryBaseSet.replace("%table%", table.getTableName()).replace("%colon%", colon));
            statement.setObject(1, value);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean execute(String SQL)
    {
        Statement statement = null;
        boolean r = false;

        try
        {
            r = (statement = openConnection().createStatement()).execute(SQL);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return r;
    }

    @Override
    public List<Table> getTables()
    {
        return tables;
    }

    @Override
    public Connection openConnection() throws ClassNotFoundException, SQLException
    {
        if (connection == null || connection.isClosed())
        {
            hasFirstConnection = true;
            String connectionURL = "jdbc:mysql://"
                    + this.hostname + ":" + this.port;

            if (database != null || !database.isEmpty())
                connectionURL = connectionURL + "/" + this.database + "?autoReconnect=true&useUnicode=yes";


            Class.forName("com.mysql.jdbc.Driver");

            System.out.println(connectionURL);

            connection = DriverManager.getConnection(connectionURL, this.username, this.password);

            System.out.println("New MySQL database connection.");
        }

        return connection;
    }

    public boolean ifTableExist(Table table)
    {
        boolean r = false;
        try {
            DatabaseMetaData metaData = openConnection().getMetaData();

            ResultSet tables = metaData.getTables(null, null, table.getTableName(), null);

            r = tables.next();
        } catch (CommunicationsException e) {

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public Config.IConfig init(Config config, Table... tables)
    {
        this.tables.addAll(Arrays.asList(tables));
        for (Table table : tables)
        {
            if (!ifTableExist(table))
            {
                try
                {
                    Statement statement = openConnection().createStatement();

                    statement.executeUpdate(table.getSqlStatement());

                    statement.close();
                } catch (SQLException | ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            } else
            {
                //check all columns exist
                for (ColumnType columnType : table.getColumns())
                {
                    if (!ifColumnsExist(table, columnType))
                    {
                        String sql = "ALTER TABLE " + table.getTableName() + " ADD COLUMN " + columnType.getName() + " " + columnType.getType();

                        if (execute(sql))
                            System.out.println("Successfully update " + table.getTableName() + " added new column " + columnType.getName());
                        else
                            System.out.println("An error has been detected on update " + table.getTableName() + " for add a new column " + columnType.getName());
                    }
                }
            }
        }

        return this;
    }
}
