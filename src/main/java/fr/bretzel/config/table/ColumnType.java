package fr.bretzel.config.table;

import java.math.BigInteger;

public class ColumnType
{
    private String type;

    private String name;

    private ColumnType(String name, String type)
    {
        this.name = name;
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj instanceof ColumnType && ((ColumnType) obj).getType().equals(getType()) && ((ColumnType) obj).getName().equals(getName());
    }

    /************************************************************
     *******************  STATIC MEMBERS  ***********************
     ************************************************************/


    public static ColumnType fromSQLType(String name, String type)
    {
        return new ColumnType(name, type);
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TINYINT(String name)
    {
        return new ColumnType(name, "TINYINT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType SMALLINT(String name)
    {
        return new ColumnType(name, "SMALLINT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType MEDIUMINT(String name)
    {
        return new ColumnType(name, "MEDIUMINT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType INT(String name)
    {
        return new ColumnType(name, "INT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType BIGINT(String name)
    {
        return new ColumnType(name, "BIGINT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType DOUBLE(String name)
    {
        return new ColumnType(name, "DOUBLE");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType DECIMAL(String name)
    {
        return new ColumnType(name, "DECIMAL");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType FLOAT(String name)
    {
        return new ColumnType(name, "FLOAT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TIME(String name)
    {
        return new ColumnType(name, "TIME");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TIMESTAMP(String name)
    {
        return new ColumnType(name, "TIMESTAMP");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType DATETIME(String name)
    {
        return new ColumnType(name, "DATETIME");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType DATE(String name)
    {
        return new ColumnType(name, "DATE");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType YEAR(String name)
    {
        return new ColumnType(name, "YEAR");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType CHAR(String name)
    {
        return new ColumnType(name, "CHAR");
    }

    /**
     * @param name        the of type
     * @param max_varchar must be a integer (max 65 535) or "max"
     * @return a new type
     */
    public static ColumnType VARCHAR(String name, String max_varchar)
    {
        if (max_varchar.equalsIgnoreCase("max"))
            max_varchar = String.valueOf(65535);

        if (!isInteger(max_varchar))
            throw new IllegalArgumentException("max_varchar must be a int or \"max\"");

        int value = Integer.valueOf(max_varchar);

        if (value > 65535)
            throw new IllegalArgumentException("max_varchar must be inferior of 65 535");

        if (value < 0)
            throw new IllegalArgumentException("max_varchar must be superior of 0");

        return new ColumnType(name, "VARCHAR(%type%)".replace("%type%", max_varchar));
    }

    /**
     * @param name        the of type
     * @param max_varchar must be a integer (max 65 535) or "max"
     * @return a new type
     */
    public static ColumnType VARBINARY(String name, String max_varchar)
    {
        if (max_varchar.equalsIgnoreCase("max"))
            max_varchar = String.valueOf(65535);

        if (!isInteger(max_varchar))
            throw new IllegalArgumentException("max_varchar must be a int or \"max\"");

        int value = Integer.valueOf(max_varchar);

        if (value > 65535)
            throw new IllegalArgumentException("max_varchar must be inferior of 65 535");

        if (value < 0)
            throw new IllegalArgumentException("max_varchar must be superior of 0");

        return new ColumnType(name, "VARCHAR(%type%)".replace("%type%", max_varchar));
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TINYBLOB(String name)
    {
        return new ColumnType(name, " TINYBLOB");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TINYTEXT(String name)
    {
        return new ColumnType(name, "TINYTEXT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType BLOB(String name)
    {
        return new ColumnType(name, "BLOB");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType TEXT(String name)
    {
        return new ColumnType(name, "TEXT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType MEDIUMBLOB(String name)
    {
        return new ColumnType(name, "MEDIUMBLOB");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType MEDIUMTEXT(String name)
    {
        return new ColumnType(name, "MEDIUMTEXT");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType LONGBLOB(String name)
    {
        return new ColumnType(name, "LONGBLOB");
    }

    /**
     * @param name the of type
     * @return a new type
     */
    public static ColumnType LONGTEXT(String name)
    {
        return new ColumnType(name, "LONGTEXT");
    }


    /**
     * @param s String to parse
     * @return true if s is a BigInteger
     */
    private static boolean isBigInteger(String s)
    {
        try
        {
            new BigInteger(s);
        } catch (NumberFormatException | NullPointerException e)
        {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    /**
     * @param s String to parse
     * @return true if s is a Integer
     */
    private static boolean isInteger(String s)
    {
        try
        {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e)
        {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
