package fr.bretzel.config.table;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Table
{
    private String TABLE_NAME;
    private String SQL_CREATE_STATEMENT;
    private String TABLE_CONSTRUCTOR;
    private List<ColumnType> columns = new LinkedList<>();

    public Table(String name, ColumnType... columnTypes)
    {
        TABLE_NAME = name;

        StringBuilder stringBuilder = new StringBuilder();

        int passed = 0;

        for (ColumnType value : columnTypes)
        {
            passed++;

            columns.add(value);

            if (passed == columnTypes.length)
            {
                stringBuilder.append(value.getName()).append(" ").append(value.getType());
            } else
            {
                stringBuilder.append(value.getName()).append(" ").append(value.getType()).append(", ");
            }
        }

        TABLE_CONSTRUCTOR = stringBuilder.toString();
        SQL_CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "( " + TABLE_CONSTRUCTOR + " )";
    }

    public Table(DatabaseMetaData metaData, String name)
    {
        try
        {
            ResultSet rs = metaData.getColumns(null, null, name, null);

            while (rs.next())
            {
                String SQL_name = rs.getString("COLUMN_NAME").trim();
                String SQL_type = rs.getString("TYPE_NAME").trim();

                columns.add(ColumnType.fromSQLType(SQL_name, SQL_type));
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        } finally
        {
            StringBuilder stringBuilder = new StringBuilder();

            int passed = 0;

            for (ColumnType value : columns)
            {
                passed++;

                if (passed == columns.size())
                {
                    stringBuilder.append(value.getName()).append(" ").append(value.getType());
                } else
                {
                    stringBuilder.append(value.getName()).append(" ").append(value.getType()).append(", ");
                }
            }

            TABLE_NAME = name;
            TABLE_CONSTRUCTOR = stringBuilder.toString();
            SQL_CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "( " + TABLE_CONSTRUCTOR + " )";
        }
    }

    public List<ColumnType> getColumns()
    {
        return columns;
    }

    public List<ColumnType> getColumns(String name)
    {
        List<ColumnType> list = new ArrayList<>();
        for (ColumnType value : getColumns())
            if (value.getName().equals(name))
                list.add(value);
        return list;
    }

    public List<ColumnType> getTableValueForType(String type)
    {
        List<ColumnType> values = new ArrayList<>();

        for (ColumnType value : getColumns())
        {
            if (value.getType().equals(type))
                values.add(value);
        }

        return values;
    }

    public String getTableName()
    {
        return TABLE_NAME;
    }

    public String getTableConstructor()
    {
        return TABLE_CONSTRUCTOR;
    }

    public String getSqlStatement()
    {
        return SQL_CREATE_STATEMENT;
    }
}
