package fr.bretzel.config;

import fr.bretzel.config.local.SQLiteConfig;
import fr.bretzel.config.online.MySQLConfig;
import fr.bretzel.config.table.ColumnType;
import fr.bretzel.config.table.Table;

import java.io.File;
import java.sql.*;
import java.util.List;

public class Config
{

    private SQLType configSQLType;
    private IConfig config;

    public Config(String hostname, String username, String password, String port, String database, SQLType sqlType, Table... tables)
    {
        this.configSQLType = sqlType;

        switch (sqlType)
        {
            case MY_SQL:
                config = new MySQLConfig(hostname, username, password, port, database).init(this, tables);
                break;
            default:
                config = new MySQLConfig(hostname, username, password, port, database).init(this, tables);
                break;
        }

    }

    public Config(File conf, SQLType sqlType, Table... tables)
    {
        this.configSQLType = sqlType;

        switch (sqlType)
        {
            case SQLITE:
                config = new SQLiteConfig(conf).init(this, tables);
                break;
            default:
                config = new SQLiteConfig(conf).init(this, tables);
                break;
        }

    }

    public synchronized Connection openConnection()
    {
        try
        {
            return config.openConnection();
        } catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized void closeConnection()
    {
        try
        {
            if (!config.getConnection().isClosed() && config.getConnection() != null)
            {
                config.getConnection().close();
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean ifTableIsEmpty(Table table, String columns)
    {
        return config.ifTableIsEmpty(table, columns);
    }

    public boolean ifStringExistForColon(String value, String colon, Table table)
    {
        return config.ifStringExistForColon(value, colon, table);
    }

    public SQLType getConfigSQLType()
    {
        return configSQLType;
    }

    public IConfig getConfig()
    {
        return config;
    }

    public Object getObject(String colon, String where, Table table)
    {
        return config.getObject(colon, where, table);
    }

    public String getString(String colon, String where, Table table)
    {
        return (String) getObject(colon, where, table);
    }

    public int getInt(String colon, String where, Table table)
    {
        return (int) getObject(colon, where, table);
    }

    public double getDouble(String colon, String where, Table table)
    {
        return (double) getObject(colon, where, table);
    }

    public void setObject(Object value, String colon, String where, Table table)
    {
        config.setObject(value, colon, where, table);
    }

    public void setString(String value, String colon, String where, Table table)
    {
        setObject(value, colon, where, table);
    }

    public void setInt(int value, String colon, String where, Table table)
    {
        setObject(value, colon, where, table);
    }

    public void setDouble(double value, String colon, String where, Table table)
    {
        setObject(value, colon, where, table);
    }

    public boolean execute(String SQL)
    {
        return config.execute(SQL);
    }

    public enum SQLType
    {
        SQLITE(true),
        MY_SQL(false);

        private boolean local;

        SQLType(boolean isLocal)
        {
            this.local = isLocal;
        }

        public boolean isLocal()
        {
            return local;
        }
    }

    public interface IConfig
    {
        IConfig init(Config config, Table... tables) throws SQLException, ClassNotFoundException;

        Connection openConnection() throws ClassNotFoundException, SQLException;

        Connection getConnection() throws SQLException;

        boolean ifTableIsEmpty(Table table, String columns);

        boolean ifStringExistForColon(String value, String colon, Table table);

        boolean ifColumnsExist(Table table, ColumnType column);

        boolean ifColumnsExist(Table table, String column);

        Object getObject(String colon, String where, Table table);

        void setObject(Object value, String colon, String where, Table table);

        boolean execute(String SQL);

        List<Table> getTables();
    }
}
