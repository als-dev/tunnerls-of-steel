package fr.bretzel.config.local;

import fr.bretzel.config.Config;
import fr.bretzel.config.table.ColumnType;
import fr.bretzel.config.table.Table;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLiteConfig implements Config.IConfig
{

    private Connection connection;
    private File db;

    private List<Table> tables = new ArrayList<>();

    public SQLiteConfig(File db)
    {
        this.db = db;
    }

    public Connection getConnection()
    {
        return connection;
    }

    @Override
    public boolean ifTableIsEmpty(Table table, String columns)
    {
        PreparedStatement statement = null;
        try
        {
            statement = openConnection().prepareStatement("SELECT " + columns + " FROM " + table.getTableName());

            ResultSet resultSet = statement.executeQuery();

            return !resultSet.next();
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

                openConnection().close();
            } catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean ifStringExistForColon(String value, String colon, Table table)
    {
        Statement statement = null;
        try
        {
            statement = openConnection().createStatement();
            String query = "SELECT " + colon + " FROM " + table.getTableName() + " WHERE " + colon + " = '" + value + "'";
            ResultSet set = statement.executeQuery(query);
            if (set.next())
            {
                String v = set.getString(colon);
                if (v.equalsIgnoreCase(value))
                    return true;
            }
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try {
                if (statement != null)
                    statement.close();

                getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean ifColumnsExist(Table table, ColumnType column)
    {
        return ifColumnsExist(table, column.getName());
    }

    @Override
    public boolean ifColumnsExist(Table table, String column)
    {
        boolean r = false;
        try
        {
            DatabaseMetaData md = openConnection().getMetaData();
            ResultSet rs = md.getColumns(null, null, table.getTableName(), column);

            r = rs.next();

            rs.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                openConnection().close();
            } catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        return r;
    }

    @Override
    public Object getObject(String colon, String where, Table table)
    {
        Object o = null;
        PreparedStatement statement = null;
        try
        {
            String SQL = "SELECT " + colon + " FROM " + table.getTableName() + " WHERE " + where;
            statement = openConnection().prepareStatement(SQL);
            ResultSet set = statement.executeQuery();
            o = set.next() ? set.getObject(1) : null;
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

                openConnection().close();
            } catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        return o;
    }

    @Override
    public void setObject(Object value, String colon, String where, Table table)
    {
        if (where == null || where.isEmpty())
        {
            insertInto(value, colon, table);
        } else
        {
            updateSet(value, colon, where, table);
        }
    }

    private void updateSet(Object value, String colon, String where, Table table)
    {
        Statement statement = null;
        try
        {
            String queryBaseSet = "UPDATE " + table.getTableName() + " SET " + colon + "=\"" + value + "\" WHERE " + where;
            statement = openConnection().createStatement();
            statement.executeUpdate(queryBaseSet);
            statement.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

                getConnection().close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void insertInto(Object value, String colon, Table table)
    {
        PreparedStatement statement = null;
        try
        {
            String queryBaseSet = "INSERT INTO %table% ( %colon% ) VALUES (?)";
            statement = openConnection().prepareStatement(queryBaseSet.replace("%table%", table.getTableName()).replace("%colon%", colon));
            statement.setObject(1, value);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

                getConnection().close();

            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean execute(String SQL)
    {
        Statement statement = null;
        boolean r = false;

        try
        {
            r = (statement = openConnection().createStatement()).execute(SQL);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (statement != null)
                    statement.close();

                openConnection().close();

            } catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        return r;
    }

    @Override
    public List<Table> getTables()
    {
        return tables;
    }

    public boolean checkConnection() throws SQLException
    {
        return connection != null && !connection.isClosed();
    }

    /**
     * Check connection and re open this if is closed or timeout
     *
     * @return Connection
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    public synchronized Connection openConnection() throws ClassNotFoundException, SQLException
    {
        if (checkConnection())
        {
            return getConnection();
        }

        String connectionURL = "jdbc:sqlite:" + db;

        /**
         * Verify driver is present and is loaded by current JVM
         */
        Class.forName("org.sqlite.JDBC");

        connection = DriverManager.getConnection(connectionURL);

        //System.out.println("New SQLITE database connection.");

        return connection;
    }

    /**
     * @param table table if you want check
     * @return true if table exist, false don't exist
     */
    public boolean ifTableExist(Table table)
    {
        boolean r = false;
        try
        {
            DatabaseMetaData metaData = openConnection().getMetaData();

            ResultSet tables = metaData.getTables(null, null, table.getTableName(), null);

            r = tables.next();

            tables.close();
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                openConnection().close();
            } catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        return r;
    }

    /**
     * Init new database and update table if change has been detected
     *
     * @param config Config instance
     * @param tables table if you want add/update
     * @return this current instance
     */
    @Override
    public Config.IConfig init(Config config, Table... tables)
    {
        this.tables.addAll(Arrays.asList(tables));

        for (Table table : tables)
        {
            //Check table exist
            if (!ifTableExist(table))
            {
                //Create new table
                try
                {
                    Statement statement = openConnection().createStatement();

                    statement.executeUpdate(table.getSqlStatement());

                    statement.close();
                } catch (SQLException | ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            } else
            {
                //check all columns exist

                for (ColumnType columnType : table.getColumns())
                {
                    if (!ifColumnsExist(table, columnType))
                    {
                        String sql = "ALTER TABLE " + table.getTableName() + " ADD COLUMN " + columnType.getName() + " " + columnType.getType();

                        if (execute(sql))
                            System.out.println("Successfully update " + table.getTableName() + " added new column " + columnType.getName());
                        else
                            System.out.println("An error has been detected on update " + table.getTableName() + " for add a new column " + columnType.getName());
                    }
                }
            }
        }
        return this;
    }
}
