package fr.bretzel.structure;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Directional;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Structure
{
    private static final String[] splits = Bukkit.getBukkitVersion().split("\\.");
    public static final String MC_VERSION = splits[0] + "." + splits[1] + (splits.length >= 3 ? "" : "." + splits[2]);

    //Replace block of destination
    public static final int TYPE_REPLACE = 1;
    //Place block only if no block is already present
    public static final int TYPE_KEEP = 2;
    //Destroys the block already present (and drops this block), before placing the new one.
    public static final int TYPE_DESTROY = 3;

    /**
     * @param location
     * @param location1
     * @param saveFile
     * @param executeInNewThread
     */
    public static void saveStructure(Location location, Location location1, File saveFile, boolean executeInNewThread)
    {
        if (executeInNewThread)
        {
            new Thread(() -> saveStructure(location, location1, saveFile)).start();
        } else
        {
            saveStructure(location, location1, saveFile);
        }
    }

    private static void saveStructure(Location location, Location location1, File saveFile)
    {
        if (!saveFile.exists())
        {
            try
            {
                throw new InvalidObjectException("The file: " + saveFile.getPath() + " d'ont exist.");
            } catch (InvalidObjectException e)
            {
                e.printStackTrace();
            }
        }

        if (!saveFile.getName().endsWith(".struc"))
        {
            saveFile.renameTo(new File(saveFile.getParentFile(), saveFile.getName() + ".struc"));
        }

        World world = location.getWorld();

        //find the max X and min X
        final int maxX = (location.getBlockX() < location1.getBlockX() ? location1.getBlockX() : location.getBlockX());
        final int minX = (location.getBlockX() > location1.getBlockX() ? location1.getBlockX() : location.getBlockX());

        //find the max Y and min Y
        final int maxY = (location.getBlockY() < location1.getBlockY() ? location1.getBlockY() : location.getBlockY());
        final int minY = (location.getBlockY() > location1.getBlockY() ? location1.getBlockY() : location.getBlockY());

        //find the max Z and min Z
        final int maxZ = (location.getBlockZ() < location1.getBlockZ() ? location1.getBlockZ() : location.getBlockZ());
        final int minZ = (location.getBlockZ() > location1.getBlockZ() ? location1.getBlockZ() : location.getBlockZ());

        //Create new gson and enable prety printing
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject mainObject = new JsonObject();

        mainObject.addProperty("CreatedMinecraftVersion", MC_VERSION);

        for (int y = minY; y <= maxY; y++)
        {
            for (int z = minZ; z <= maxZ; z++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    JsonObject blockObject = new JsonObject();

                    Block block = new Location(world, x, y, z).getBlock();

                    Location relativeLocation = new Location(world, x - minX, y - minY, z - minZ);

                    blockObject.addProperty("t", XMaterial.requestXMaterial(block.getType().name(), block.getData()).name());

                    try
                    {
                        if (isBefore113())
                        {
                            //For minecraft 1.12.X and before
                            Method getData = Block.class.getMethod("getData");

                            blockObject.addProperty("d", (byte) getData.invoke(block));
                        } else
                        {
                            //For minecraft 1.13.X and later
                            Class blockDataClass = Class.forName("org.bukkit.block.data.BlockData");
                            Object blockData = blockDataClass.cast(Block.class.getMethod("getBlockData").invoke(block));
                            blockObject.addProperty("d", (String) blockDataClass.getMethod("getAsString", boolean.class).invoke(blockData, true));
                        }
                    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e)
                    {
                        e.printStackTrace();
                    }


                    try
                    {
                        Directional directional = (Directional) block.getState().getData();
                        blockObject.addProperty("f", directional.getFacing().name());
                    } catch (Exception e)
                    {
                    }

                    if (hasInventory(block))
                    {
                        blockObject.add("i", getJsonInventory(getInventoryOfBlock(block)));
                    }

                    mainObject.add(toStringLocation(relativeLocation), blockObject);
                }
            }
        }

        try
        {
            FileOutputStream obj = new FileOutputStream(saveFile);
            GZIPOutputStream gzip = new GZIPOutputStream(obj);

            gzip.write(gson.toJson(mainObject).getBytes(StandardCharsets.UTF_8));

            gzip.flush();
            gzip.close();
            obj.close();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * @param minLocation
     * @param ignoreAir
     * @param convert
     * @param applyPhysics
     * @param placeType
     * @param file
     */
    public static void placeStructure(Location minLocation, boolean ignoreAir, boolean convert, boolean applyPhysics, boolean flip, int placeType, File file)
    {
        World world = minLocation.getWorld();

        if (file.exists())
        {
            if (world != null)
            {
                JsonObject mainObject = null;
                try
                {
                    GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(file));
                    JsonReader reader = new JsonReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    JsonParser parser = new JsonParser();

                    mainObject = parser.parse(reader).getAsJsonObject();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }

                if (mainObject != null)
                {
                    if (mainObject.has("CreatedMinecraftVersion"))
                    {
                        String[] mcversion = mainObject.get("CreatedMinecraftVersion").getAsString().split("\\.");

                        int structurePrimary = Integer.valueOf(mcversion[0]);
                        int structureMinor = Integer.valueOf(mcversion[1]);

                        int currentPrimary = Integer.valueOf(splits[0]);
                        int currentMinor = Integer.valueOf(splits[1]);

                        if ((currentPrimary >= 1 && currentMinor > 12) && (structurePrimary >= 1 && structureMinor <= 12) && !convert)
                        {
                            try
                            {
                                throw new InvalidMinecraftVersion("The structure has been saved in 1.12 or or before. Please update the structure.");
                            } catch (InvalidMinecraftVersion invalidMinecraftVersion)
                            {
                                invalidMinecraftVersion.printStackTrace();
                            }
                        }
                    } else
                    {
                        System.out.println("Warning cannot get 'CreatedMinecraftVersion' for structure: " + file.getName());
                    }

                    for (Map.Entry<String, JsonElement> entry : mainObject.entrySet())
                    {
                        //Ignore the key 'CreatedMinecraftVersion'
                        if (!entry.getKey().equalsIgnoreCase("CreatedMinecraftVersion"))
                        {
                            Location blockLocation = toLocationString(entry.getKey(), world).add(minLocation);

                            //Invert all coordinate of block
                            if (flip)
                            {
                                blockLocation = blockLocation.clone().multiply(-1);
                            }

                            JsonObject object = (JsonObject) entry.getValue();

                            XMaterial material = XMaterial.fromString(object.get("t").getAsString());

                            if (!(ignoreAir && material == XMaterial.AIR))
                            {

                                if (placeType == TYPE_KEEP && blockLocation.getBlock().getType() != XMaterial.AIR.parseMaterial())
                                    continue;

                                if (placeType == TYPE_DESTROY && blockLocation.getBlock().getType() != XMaterial.AIR.parseMaterial())
                                    blockLocation.getBlock().breakNaturally();

                                if (material != null)
                                {

                                    blockLocation.getBlock().setType(material.parseMaterial(), applyPhysics);

                                    try
                                    {

                                        if (isBefore113())
                                        {
                                            //For minecraft 1.12.X and before
                                            Method setData = Block.class.getMethod("setData", byte.class, boolean.class);
                                            setData.invoke(blockLocation.getBlock(), object.get("d").getAsByte(), applyPhysics);
                                        } else
                                        {
                                            //For minecraft 1.13.X and later
                                            String data = object.get("d").getAsString();

                                            if (isByte(data) && convert)
                                            {
                                                setBlockData(blockLocation, material, Byte.parseByte(data), applyPhysics);
                                            } else
                                            {
                                                setBlockData(blockLocation, material, data, applyPhysics);
                                            }
                                        }
                                    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e)
                                    {
                                        e.printStackTrace();
                                    }

                                    if (object.has("f"))
                                    {
                                        try
                                        {
                                            Directional directional = (Directional) blockLocation.getBlock().getState().getData();
                                            directional.setFacingDirection(BlockFace.valueOf(object.get("f").getAsString()));
                                            blockLocation.getBlock().getState().update();
                                        } catch (Exception e)
                                        {
                                        }
                                    }

                                    if (object.has("i"))
                                    {
                                        Inventory inventory = getInventoryOfBlock(blockLocation.getBlock());
                                        for (Map.Entry<Integer, ItemStack> slotInv : getJsonInventory(object.getAsJsonObject("i")).entrySet())
                                        {
                                            if (inventory != null)
                                                inventory.setItem(slotInv.getKey(), slotInv.getValue());
                                        }
                                    }
                                } else
                                {
                                    System.out.println("The material: " + object.get("t").getAsString() + " is not a correct material.");
                                }
                            }
                        }
                    }
                }
            } else
            {
                System.out.println("The world is null.");
            }
        }
    }

    private static HashMap<Integer, ItemStack> getJsonInventory(JsonObject object)
    {
        HashMap<Integer, ItemStack> inventoryMap = new HashMap<>();

        for (Map.Entry<String, JsonElement> entry : object.entrySet())
        {
            inventoryMap.put(Integer.valueOf(entry.getKey()), getJsonItem(entry.getValue().getAsJsonObject()));
        }

        return inventoryMap;
    }

    private static JsonObject getJsonInventory(Inventory inventory)
    {
        JsonObject object = new JsonObject();

        for (int i = 0; i < inventory.getSize(); i++)
        {
            ItemStack itemStack = inventory.getItem(i);
            if (inventory.getItem(i) != null && itemStack != null)
            {
                object.add(String.valueOf(i), getJsonItem(itemStack));
            }
        }

        return object;
    }

    private static ItemStack getJsonItem(JsonObject object)
    {
        XMaterial material = XMaterial.valueOf(object.get("t").getAsString()); //Material.getMaterial(object.get("t").getAsString());
        int amount = object.get("a").getAsInt();
        short durability = object.get("du").getAsShort();

        ItemStack stack = new ItemStack(material.parseMaterial(), amount, durability);

        if (object.has("m"))
        {
            JsonObject jsonMeta = object.get("m").getAsJsonObject();
            ItemMeta meta = stack.getItemMeta();

            if (meta != null)
            {

                if (jsonMeta.has("dn"))
                    meta.setDisplayName(jsonMeta.get("dn").getAsString());

                if (jsonMeta.has("ln"))
                    meta.setLocalizedName(jsonMeta.get("ln").getAsString());

                if (jsonMeta.has("esm") && material == XMaterial.ENCHANTED_BOOK)
                {
                    EnchantmentStorageMeta bookMeta = (EnchantmentStorageMeta) meta;

                    for (Map.Entry<String, JsonElement> entry : jsonMeta.get("esm").getAsJsonObject().entrySet())
                    {
                        bookMeta.addStoredEnchant(Enchantment.getByName(entry.getKey()), entry.getValue().getAsInt() + 1, true);
                    }
                }

                if (jsonMeta.has("e"))
                {
                    for (Map.Entry<String, JsonElement> entry : jsonMeta.get("e").getAsJsonObject().entrySet())
                    {
                        meta.addEnchant(Enchantment.getByName(entry.getKey()), entry.getValue().getAsInt() + 1, true);
                    }
                }

                if (jsonMeta.has("lr"))
                {
                    JsonArray array = jsonMeta.getAsJsonArray("lr");
                    Iterator<JsonElement> iterator = array.iterator();
                    List<String> lores = new ArrayList<>();
                    while (iterator.hasNext())
                    {
                        lores.add(iterator.next().getAsString());
                    }

                    meta.setLore(lores);
                }

                if (jsonMeta.has("fl"))
                {
                    JsonArray array = jsonMeta.getAsJsonArray("fl");

                    for (JsonElement element : array)
                    {
                        meta.getItemFlags().add(ItemFlag.valueOf(element.getAsString()));
                    }
                }

                stack.setItemMeta(meta);
            }
        }
        return stack;
    }

    private static JsonObject getJsonItem(ItemStack stack)
    {
        JsonObject object = new JsonObject();

        object.addProperty("t", stack.getType().name());
        object.addProperty("a", stack.getAmount());
        object.addProperty("du", stack.getDurability());

        if (stack.getItemMeta() != null)
        {
            JsonObject jsonMeta = new JsonObject();

            ItemMeta meta = stack.getItemMeta();

            if (meta.hasDisplayName() && !meta.getDisplayName().isEmpty())
                jsonMeta.addProperty("dn", meta.getDisplayName());


            if (meta.hasDisplayName() && !meta.getLocalizedName().isEmpty())
                jsonMeta.addProperty("ln", meta.getLocalizedName());

            if (stack.getType() == XMaterial.ENCHANTED_BOOK.parseMaterial())
            {
                EnchantmentStorageMeta storageMeta = (EnchantmentStorageMeta) meta;
                JsonObject ench = new JsonObject();

                for (Enchantment enchantment : storageMeta.getStoredEnchants().keySet())
                {
                    ench.addProperty(enchantment.getName(), meta.getEnchantLevel(enchantment));
                }

                jsonMeta.add("esm", ench);
            }

            if (meta.hasEnchants() && meta.getEnchants().size() > 0)
            {
                JsonObject ench = new JsonObject();

                for (Enchantment enchantment : meta.getEnchants().keySet())
                {
                    ench.addProperty(enchantment.getName(), meta.getEnchantLevel(enchantment));
                }

                jsonMeta.add("e", ench);
            }

            if (meta.hasLore() && meta.getLore() !=  null)
            {
                JsonArray jsonLore = new JsonArray();

                for (String s : meta.getLore())
                {
                    jsonLore.add(s);
                }

                jsonMeta.add("lr", jsonLore);
            }

            if (meta.getItemFlags().size() > 0)
            {
                JsonArray flags = new JsonArray();
                for (ItemFlag flag : meta.getItemFlags())
                {
                    flags.add(flag.name());
                }

                jsonMeta.add("fl", flags);
            }

            object.add("m", jsonMeta);
        }

        return object;
    }

    private static boolean hasInventory(Block block)
    {
        try
        {
            ((Container) block.getState()).getInventory();
        } catch (Exception e)
        {
            return false;
        }

        return true;
    }

    private static Inventory getInventoryOfBlock(Block block)
    {
        if (!hasInventory(block))
            return Bukkit.createInventory(null, 9);

        return ((Container) block.getState()).getInventory();
    }

    private static String toStringLocation(Location location)
    {
        return location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ();
    }

    private static Location toLocationString(String location, World world)
    {
        if (location != null && !location.isEmpty())
        {
            String[] strings = location.split(":");
            return new Location(world, Double.valueOf(strings[0]), Double.valueOf(strings[1]), Double.valueOf(strings[2]));
        } else
        {
            return null;
        }
    }

    private static class InvalidMinecraftVersion extends Throwable
    {
        InvalidMinecraftVersion(String msg)
        {
            super(msg);
        }
    }

    public static boolean isBefore113()
    {
        String[] splits = Bukkit.getBukkitVersion().split("\\.");

        int primary = Integer.valueOf(splits[0]);
        int minor = Integer.valueOf(splits[1]);

        return primary >= 1 && minor <= 12;
    }

    private static void setBlockData(Location location, XMaterial type, String blockDataValue, boolean appPhysics)
    {
        try
        {
            Class blockDataClass = Class.forName("org.bukkit.block.data.BlockData");

            if (blockDataValue.startsWith("minecraft:"))
            {
                Method setBlockData = Block.class.getMethod("setBlockData", blockDataClass, boolean.class);

                Object blockData = Bukkit.class.getMethod("createBlockData", String.class).invoke(null, blockDataValue);

                setBlockData.invoke(location.getBlock(), blockDataClass.cast(blockData), appPhysics);
            } else
            {

                Method setBlockData = Block.class.getMethod("setBlockData", blockDataClass, boolean.class);

                Object blockData = Bukkit.class.getMethod("createBlockData", Material.class, String.class).invoke(null, type, blockDataValue);

                setBlockData.invoke(location.getBlock(), blockDataClass.cast(blockData), appPhysics);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    private static void setBlockData(Location location, XMaterial type, byte blockDataByte, boolean appPhysics)
    {
        try
        {
            Class blockDataClass = Class.forName("org.bukkit.block.data.BlockData");

            Method setBlockData = Block.class.getMethod("setBlockData", blockDataClass, boolean.class);

            Method fromLegacy = Bukkit.getUnsafe().getClass().getMethod("fromLegacy", Material.class, byte.class);

            Object blockData = fromLegacy.invoke(Bukkit.getUnsafe(), type, blockDataByte);

            setBlockData.invoke(location.getBlock(), blockDataClass.cast(blockData), appPhysics);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    private static boolean isByte(String s)
    {
        try
        {
            Byte.parseByte(s);
        } catch (Exception e)
        {
            return false;
        }
        return true;
    }
}
